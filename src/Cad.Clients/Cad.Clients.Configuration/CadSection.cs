﻿//
// Cad.Clients.Configuration.CadSection
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Configuration;

namespace Cad.Clients.Configuration
{
    public sealed class CadSection : ConfigurationSection
    {
        #region PROPERTIES
        [ConfigurationProperty("culture", DefaultValue = "en-US")]
        public string Culture
        {
            get { return base["culture"].ToString(); }
            set { base["culture"] = value; }
        }

        [ConfigurationProperty("defaultConnectionStringName", 
            DefaultValue = "ProductMySqlServer")]
        public string DefaultConnectionStringName
        {
            get { return base["defaultConnectionStringName"].ToString(); }
            set { base["defaultConnectionStringName"] = value; }
        }

        [ConfigurationProperty("dispatchConsole", IsRequired = true)]
        public DispatchConsoleElement DispatchConsole
        {
            get { return (DispatchConsoleElement)base["dispatchConsole"]; }
        }

        [ConfigurationProperty("dispatch", IsRequired = true)]
        public DispatchElement Dispatch
        {
            get { return (DispatchElement)base["dispatch"]; }
        }

        [ConfigurationProperty("equipment", IsRequired = true)]
        public EquipmentElement Equipment
        {
            get { return (EquipmentElement)base["equipment"]; }
        }

        [ConfigurationProperty("humanResources", IsRequired = true)]
        public HumanResourcesElement HumanResources
        {
            get { return (HumanResourcesElement)base["humanResources"]; }
        }

        [ConfigurationProperty("person", IsRequired = true)]
        public PersonElement Person
        {
            get { return (PersonElement)base["person"]; }
        }
        #endregion
    }
}
