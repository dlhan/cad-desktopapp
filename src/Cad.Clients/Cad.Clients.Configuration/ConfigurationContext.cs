﻿//
// Cad.Clients.Configuration.ConfigurationContext
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Configuration;

namespace Cad.Clients.Configuration
{
    public class ConfigurationContext
    {
        public readonly static CadSection Settings =
            (CadSection)ConfigurationManager.GetSection("cad");
    }
}
