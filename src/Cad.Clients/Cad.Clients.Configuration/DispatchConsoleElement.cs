﻿//
// Cad.Clients.Configuration.DispatchConsoleElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Configuration;

namespace Cad.Clients.Configuration
{
    public sealed class DispatchConsoleElement : ConfigurationElement
    {
        [ConfigurationProperty("id", DefaultValue = 1, IsRequired = true)]
        public int Id 
        {
            get { return (int)base["id"]; }
            set { base["id"] = value; } 
        }
    }
}
