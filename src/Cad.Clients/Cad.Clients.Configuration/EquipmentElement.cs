﻿//
// Cad.Clients.Configuration.EquipmentElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Configuration;

namespace Cad.Clients.Configuration
{
    public sealed class EquipmentElement : ConfigurationElement
    {
        [ConfigurationProperty("connectionStringName")]
        public string ConnectionStringName
        {
            get { return base["connectionStringName"].ToString(); }
            set { base["connectionStringName"] = value; }
        }

        public string ConnectionString
        {
            get
            {
                // Return the base class' ConnectionString property.
                // The name of the connection string to use is retrieved from the site's 
                // custom config section and is used to read the setting from the <connectionStrings> section
                // If no connection string name is defined for the <dispatch> element, the
                // parent section's DefaultConnectionString prop is used.
                string cn = (string.IsNullOrEmpty(ConnectionStringName)
                    ? ConfigurationContext.Settings.DefaultConnectionStringName
                    : ConnectionStringName);
                return ConfigurationManager.ConnectionStrings[cn].ConnectionString;
            }
        }

        [ConfigurationProperty("providerType",
            DefaultValue = "Cad.Data.DAL.MySqlClient.MySqlEquipmentProvider")]
        public string ProviderType
        {
            get { return base["providerType"].ToString(); }
            set { base["providerType"] = value; }
        }
    }
}
