﻿//
// Cad.Clients.WinForm.AppContext
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using Cad.Clients.Configuration;
using Cad.Equipments;

namespace Cad.Clients.WinForm
{
    public static class AppContext
    {
        #region VARIABLES
        private static bool _initialized = false;
        private static Exception _initializedException = null;
        private static object _lock = new object();
        #endregion

        #region PROPERTIES
        private static string _appName = null;
        public static string AppName
        {
            get
            {
                Initialize();
                return _appName;
            }
        }

        private static string _appVersion = null;
        public static string AppVersion
        {
            get
            {
                Initialize();
                return _appVersion;
            }
        }

        private static string _appMachineName = null;
        public static string AppMachineName
        {
            get
            {
                Initialize();
                return _appMachineName;
            }
        }

        private static int? _appProcessId = null;
        public static int? AppProcessID
        {
            get
            {
                Initialize();
                return _appProcessId;
            }
        }
        private static string _culture = null;
        public static string Culture
        {
            get
            {
                Initialize();
                return _culture;
            }
        }

        //private static CadSection _settings = null;
        //public static CadSection Settings
        //{
        //    get
        //    {
        //        Initialize();
        //        return _settings;
        //    }
        //}

        private static DispatchConsole _dispatchConsole = null;
        public static DispatchConsole DispatchConsole
        {
            get
            {
                Initialize();
                return _dispatchConsole;
            }
        }
        #endregion

        #region PRIVATE METHODS
        private static void Initialize()
        {
            if (_initialized) return;
            if (_initializedException != null) throw _initializedException;

            lock (_lock)
            {
                if (_initialized) return;
                if (_initializedException != null) throw _initializedException;

                try
                {
                    Process process = Process.GetCurrentProcess();
                    Assembly assembly = Assembly.GetExecutingAssembly();

                    _appName = assembly.GetCustomAttribute<AssemblyTitleAttribute>().Title;
                    _appVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    _appMachineName = Environment.MachineName;
                    _appProcessId = Process.GetCurrentProcess().Id;

                    _culture = ConfigurationContext.Settings.Culture;
                    //_settings = (CadSection)ConfigurationManager.GetSection("cad");
                    _dispatchConsole = new DispatchConsole()
                    {
                        Id = ConfigurationContext.Settings.DispatchConsole.Id,
                    };
                }
                catch (Exception e)
                {
                    _initializedException = e;
                    throw;
                }

                _initialized = true;
            }
        }
        #endregion
    }
}
