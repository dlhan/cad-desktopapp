﻿
namespace Cad.Clients.WinForm.Controls
{
    partial class CallPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContainer = new System.Windows.Forms.TableLayoutPanel();
            this.lblCallSection = new DevExpress.XtraEditors.LabelControl();
            this.pnCall = new System.Windows.Forms.TableLayoutPanel();
            this.pnNumberStatus = new System.Windows.Forms.TableLayoutPanel();
            this.teNumOfCalls = new DevExpress.XtraEditors.TextEdit();
            this.leNumberStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.lblNumberStatus = new DevExpress.XtraEditors.LabelControl();
            this.pnDisposition = new System.Windows.Forms.TableLayoutPanel();
            this.leDisposition = new DevExpress.XtraEditors.LookUpEdit();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.teLongitude = new DevExpress.XtraEditors.TextEdit();
            this.teLatitude = new DevExpress.XtraEditors.TextEdit();
            this.lblDisposition = new DevExpress.XtraEditors.LabelControl();
            this.pnIncomingTime = new System.Windows.Forms.TableLayoutPanel();
            this.teIncomingTime = new DevExpress.XtraEditors.TextEdit();
            this.deIncomingDate = new DevExpress.XtraEditors.DateEdit();
            this.lblIncommingTime = new DevExpress.XtraEditors.LabelControl();
            this.teAddressLine1 = new DevExpress.XtraEditors.TextEdit();
            this.lblCallerLocation = new DevExpress.XtraEditors.LabelControl();
            this.teCallerName = new DevExpress.XtraEditors.TextEdit();
            this.lblCallerName = new DevExpress.XtraEditors.LabelControl();
            this.lblCall = new DevExpress.XtraEditors.LabelControl();
            this.pnCallRouteAndNo = new System.Windows.Forms.TableLayoutPanel();
            this.tePhoneNumber = new DevExpress.XtraEditors.TextEdit();
            this.leRoute = new DevExpress.XtraEditors.LookUpEdit();
            this.pnStateAndCity = new System.Windows.Forms.TableLayoutPanel();
            this.leCity = new DevExpress.XtraEditors.LookUpEdit();
            this.leProvince = new DevExpress.XtraEditors.LookUpEdit();
            this.teAddressLine2 = new DevExpress.XtraEditors.TextEdit();
            this.pnPostalCode = new System.Windows.Forms.TableLayoutPanel();
            this.tePostalCode = new DevExpress.XtraEditors.TextEdit();
            this.pnContainer.SuspendLayout();
            this.pnCall.SuspendLayout();
            this.pnNumberStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teNumOfCalls.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leNumberStatus.Properties)).BeginInit();
            this.pnDisposition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leDisposition.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLongitude.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLatitude.Properties)).BeginInit();
            this.pnIncomingTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teIncomingTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIncomingDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIncomingDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCallerName.Properties)).BeginInit();
            this.pnCallRouteAndNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tePhoneNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRoute.Properties)).BeginInit();
            this.pnStateAndCity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leProvince.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine2.Properties)).BeginInit();
            this.pnPostalCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tePostalCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            this.pnContainer.ColumnCount = 1;
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnContainer.Controls.Add(this.lblCallSection, 0, 0);
            this.pnContainer.Controls.Add(this.pnCall, 0, 1);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.RowCount = 2;
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnContainer.Size = new System.Drawing.Size(454, 700);
            this.pnContainer.TabIndex = 0;
            // 
            // lblCallSection
            // 
            this.lblCallSection.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCallSection.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(168)))), ((int)(((byte)(123)))));
            this.lblCallSection.Appearance.Options.UseFont = true;
            this.lblCallSection.Appearance.Options.UseForeColor = true;
            this.lblCallSection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCallSection.Location = new System.Drawing.Point(0, 0);
            this.lblCallSection.Margin = new System.Windows.Forms.Padding(0);
            this.lblCallSection.Name = "lblCallSection";
            this.lblCallSection.Size = new System.Drawing.Size(454, 28);
            this.lblCallSection.TabIndex = 3;
            this.lblCallSection.Text = "CALL";
            // 
            // pnCall
            // 
            this.pnCall.ColumnCount = 2;
            this.pnCall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.pnCall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnCall.Controls.Add(this.pnNumberStatus, 1, 2);
            this.pnCall.Controls.Add(this.lblNumberStatus, 0, 2);
            this.pnCall.Controls.Add(this.pnDisposition, 1, 9);
            this.pnCall.Controls.Add(this.tableLayoutPanel1, 1, 8);
            this.pnCall.Controls.Add(this.lblDisposition, 0, 9);
            this.pnCall.Controls.Add(this.pnIncomingTime, 1, 0);
            this.pnCall.Controls.Add(this.lblIncommingTime, 0, 0);
            this.pnCall.Controls.Add(this.teAddressLine1, 1, 5);
            this.pnCall.Controls.Add(this.lblCallerLocation, 0, 4);
            this.pnCall.Controls.Add(this.teCallerName, 1, 3);
            this.pnCall.Controls.Add(this.lblCallerName, 0, 3);
            this.pnCall.Controls.Add(this.lblCall, 0, 1);
            this.pnCall.Controls.Add(this.pnCallRouteAndNo, 1, 1);
            this.pnCall.Controls.Add(this.pnStateAndCity, 1, 4);
            this.pnCall.Controls.Add(this.teAddressLine2, 1, 6);
            this.pnCall.Controls.Add(this.pnPostalCode, 1, 7);
            this.pnCall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnCall.Location = new System.Drawing.Point(0, 28);
            this.pnCall.Margin = new System.Windows.Forms.Padding(0);
            this.pnCall.Name = "pnCall";
            this.pnCall.RowCount = 11;
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnCall.Size = new System.Drawing.Size(454, 672);
            this.pnCall.TabIndex = 1;
            // 
            // pnNumberStatus
            // 
            this.pnNumberStatus.ColumnCount = 2;
            this.pnNumberStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnNumberStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnNumberStatus.Controls.Add(this.teNumOfCalls, 0, 0);
            this.pnNumberStatus.Controls.Add(this.leNumberStatus, 0, 0);
            this.pnNumberStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnNumberStatus.Location = new System.Drawing.Point(130, 56);
            this.pnNumberStatus.Margin = new System.Windows.Forms.Padding(0);
            this.pnNumberStatus.Name = "pnNumberStatus";
            this.pnNumberStatus.RowCount = 1;
            this.pnNumberStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnNumberStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnNumberStatus.Size = new System.Drawing.Size(324, 28);
            this.pnNumberStatus.TabIndex = 17;
            // 
            // teNumOfCalls
            // 
            this.teNumOfCalls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teNumOfCalls.Location = new System.Drawing.Point(165, 3);
            this.teNumOfCalls.Name = "teNumOfCalls";
            this.teNumOfCalls.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teNumOfCalls.Properties.Appearance.Options.UseFont = true;
            this.teNumOfCalls.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.teNumOfCalls.Properties.NullValuePrompt = "Number of Calls";
            this.teNumOfCalls.Size = new System.Drawing.Size(156, 22);
            this.teNumOfCalls.TabIndex = 4;
            // 
            // leNumberStatus
            // 
            this.leNumberStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leNumberStatus.Location = new System.Drawing.Point(3, 3);
            this.leNumberStatus.Name = "leNumberStatus";
            this.leNumberStatus.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leNumberStatus.Properties.Appearance.Options.UseFont = true;
            this.leNumberStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leNumberStatus.Properties.NullText = "";
            this.leNumberStatus.Properties.NullValuePrompt = "Route";
            this.leNumberStatus.Size = new System.Drawing.Size(156, 22);
            this.leNumberStatus.TabIndex = 3;
            // 
            // lblNumberStatus
            // 
            this.lblNumberStatus.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberStatus.Appearance.Options.UseFont = true;
            this.lblNumberStatus.Appearance.Options.UseTextOptions = true;
            this.lblNumberStatus.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblNumberStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumberStatus.Location = new System.Drawing.Point(3, 59);
            this.lblNumberStatus.Name = "lblNumberStatus";
            this.lblNumberStatus.Size = new System.Drawing.Size(124, 22);
            this.lblNumberStatus.TabIndex = 16;
            this.lblNumberStatus.Text = "Number status:";
            // 
            // pnDisposition
            // 
            this.pnDisposition.ColumnCount = 2;
            this.pnDisposition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnDisposition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnDisposition.Controls.Add(this.leDisposition, 0, 0);
            this.pnDisposition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDisposition.Location = new System.Drawing.Point(130, 252);
            this.pnDisposition.Margin = new System.Windows.Forms.Padding(0);
            this.pnDisposition.Name = "pnDisposition";
            this.pnDisposition.RowCount = 1;
            this.pnDisposition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnDisposition.Size = new System.Drawing.Size(324, 28);
            this.pnDisposition.TabIndex = 15;
            // 
            // leDisposition
            // 
            this.leDisposition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leDisposition.Location = new System.Drawing.Point(3, 3);
            this.leDisposition.Name = "leDisposition";
            this.leDisposition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leDisposition.Properties.Appearance.Options.UseFont = true;
            this.leDisposition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leDisposition.Properties.NullText = "";
            this.leDisposition.Properties.NullValuePrompt = "Disposition";
            this.leDisposition.Size = new System.Drawing.Size(156, 22);
            this.leDisposition.TabIndex = 15;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.teLongitude, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.teLatitude, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(130, 224);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(324, 28);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // teLongitude
            // 
            this.teLongitude.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teLongitude.EditValue = "";
            this.teLongitude.Location = new System.Drawing.Point(165, 3);
            this.teLongitude.Name = "teLongitude";
            this.teLongitude.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teLongitude.Properties.Appearance.Options.UseFont = true;
            this.teLongitude.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.teLongitude.Properties.NullValuePrompt = "Longitude";
            this.teLongitude.Size = new System.Drawing.Size(156, 22);
            this.teLongitude.TabIndex = 10;
            // 
            // teLatitude
            // 
            this.teLatitude.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teLatitude.EditValue = "";
            this.teLatitude.Location = new System.Drawing.Point(3, 3);
            this.teLatitude.Name = "teLatitude";
            this.teLatitude.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teLatitude.Properties.Appearance.Options.UseFont = true;
            this.teLatitude.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.teLatitude.Properties.NullValuePrompt = "Latitude";
            this.teLatitude.Size = new System.Drawing.Size(156, 22);
            this.teLatitude.TabIndex = 9;
            // 
            // lblDisposition
            // 
            this.lblDisposition.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisposition.Appearance.Options.UseFont = true;
            this.lblDisposition.Appearance.Options.UseTextOptions = true;
            this.lblDisposition.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblDisposition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDisposition.Location = new System.Drawing.Point(3, 255);
            this.lblDisposition.Name = "lblDisposition";
            this.lblDisposition.Size = new System.Drawing.Size(124, 22);
            this.lblDisposition.TabIndex = 12;
            this.lblDisposition.Text = "Disposition:";
            // 
            // pnIncomingTime
            // 
            this.pnIncomingTime.ColumnCount = 2;
            this.pnIncomingTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnIncomingTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnIncomingTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnIncomingTime.Controls.Add(this.teIncomingTime, 1, 0);
            this.pnIncomingTime.Controls.Add(this.deIncomingDate, 0, 0);
            this.pnIncomingTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnIncomingTime.Location = new System.Drawing.Point(130, 0);
            this.pnIncomingTime.Margin = new System.Windows.Forms.Padding(0);
            this.pnIncomingTime.Name = "pnIncomingTime";
            this.pnIncomingTime.RowCount = 1;
            this.pnIncomingTime.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnIncomingTime.Size = new System.Drawing.Size(324, 28);
            this.pnIncomingTime.TabIndex = 0;
            // 
            // teIncomingTime
            // 
            this.teIncomingTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teIncomingTime.Location = new System.Drawing.Point(165, 3);
            this.teIncomingTime.Name = "teIncomingTime";
            this.teIncomingTime.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teIncomingTime.Properties.Appearance.Options.UseFont = true;
            this.teIncomingTime.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.DateTimeMaskManager));
            this.teIncomingTime.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.teIncomingTime.Properties.MaskSettings.Set("mask", "\"HH:mm\"");
            this.teIncomingTime.Properties.NullValuePrompt = "--:--";
            this.teIncomingTime.Size = new System.Drawing.Size(156, 22);
            this.teIncomingTime.TabIndex = 1;
            // 
            // deIncomingDate
            // 
            this.deIncomingDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deIncomingDate.EditValue = null;
            this.deIncomingDate.Location = new System.Drawing.Point(3, 3);
            this.deIncomingDate.Name = "deIncomingDate";
            this.deIncomingDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deIncomingDate.Properties.Appearance.Options.UseFont = true;
            this.deIncomingDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deIncomingDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deIncomingDate.Size = new System.Drawing.Size(156, 22);
            this.deIncomingDate.TabIndex = 0;
            // 
            // lblIncommingTime
            // 
            this.lblIncommingTime.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncommingTime.Appearance.Options.UseFont = true;
            this.lblIncommingTime.Appearance.Options.UseTextOptions = true;
            this.lblIncommingTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblIncommingTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIncommingTime.Location = new System.Drawing.Point(3, 3);
            this.lblIncommingTime.Name = "lblIncommingTime";
            this.lblIncommingTime.Size = new System.Drawing.Size(124, 22);
            this.lblIncommingTime.TabIndex = 10;
            this.lblIncommingTime.Text = "Incoming time:";
            // 
            // teAddressLine1
            // 
            this.teAddressLine1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teAddressLine1.EditValue = "";
            this.teAddressLine1.Location = new System.Drawing.Point(133, 143);
            this.teAddressLine1.Name = "teAddressLine1";
            this.teAddressLine1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teAddressLine1.Properties.Appearance.Options.UseFont = true;
            this.teAddressLine1.Properties.NullValuePrompt = "Address Line 1";
            this.teAddressLine1.Size = new System.Drawing.Size(318, 22);
            this.teAddressLine1.TabIndex = 7;
            // 
            // lblCallerLocation
            // 
            this.lblCallerLocation.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCallerLocation.Appearance.Options.UseFont = true;
            this.lblCallerLocation.Appearance.Options.UseTextOptions = true;
            this.lblCallerLocation.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblCallerLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCallerLocation.Location = new System.Drawing.Point(3, 115);
            this.lblCallerLocation.Name = "lblCallerLocation";
            this.lblCallerLocation.Size = new System.Drawing.Size(124, 22);
            this.lblCallerLocation.TabIndex = 5;
            this.lblCallerLocation.Text = "Location:";
            // 
            // teCallerName
            // 
            this.teCallerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teCallerName.Location = new System.Drawing.Point(133, 87);
            this.teCallerName.Name = "teCallerName";
            this.teCallerName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teCallerName.Properties.Appearance.Options.UseFont = true;
            this.teCallerName.Size = new System.Drawing.Size(318, 22);
            this.teCallerName.TabIndex = 4;
            // 
            // lblCallerName
            // 
            this.lblCallerName.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCallerName.Appearance.Options.UseFont = true;
            this.lblCallerName.Appearance.Options.UseTextOptions = true;
            this.lblCallerName.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblCallerName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCallerName.Location = new System.Drawing.Point(3, 87);
            this.lblCallerName.Name = "lblCallerName";
            this.lblCallerName.Size = new System.Drawing.Size(124, 22);
            this.lblCallerName.TabIndex = 3;
            this.lblCallerName.Text = "Caller name:";
            // 
            // lblCall
            // 
            this.lblCall.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCall.Appearance.Options.UseFont = true;
            this.lblCall.Appearance.Options.UseTextOptions = true;
            this.lblCall.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblCall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCall.Location = new System.Drawing.Point(3, 31);
            this.lblCall.Name = "lblCall";
            this.lblCall.Size = new System.Drawing.Size(124, 22);
            this.lblCall.TabIndex = 0;
            this.lblCall.Text = "Route / Number:";
            // 
            // pnCallRouteAndNo
            // 
            this.pnCallRouteAndNo.ColumnCount = 2;
            this.pnCallRouteAndNo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnCallRouteAndNo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnCallRouteAndNo.Controls.Add(this.tePhoneNumber, 1, 0);
            this.pnCallRouteAndNo.Controls.Add(this.leRoute, 0, 0);
            this.pnCallRouteAndNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnCallRouteAndNo.Location = new System.Drawing.Point(130, 28);
            this.pnCallRouteAndNo.Margin = new System.Windows.Forms.Padding(0);
            this.pnCallRouteAndNo.Name = "pnCallRouteAndNo";
            this.pnCallRouteAndNo.RowCount = 1;
            this.pnCallRouteAndNo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnCallRouteAndNo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnCallRouteAndNo.Size = new System.Drawing.Size(324, 28);
            this.pnCallRouteAndNo.TabIndex = 1;
            // 
            // tePhoneNumber
            // 
            this.tePhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tePhoneNumber.Location = new System.Drawing.Point(165, 3);
            this.tePhoneNumber.Name = "tePhoneNumber";
            this.tePhoneNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tePhoneNumber.Properties.Appearance.Options.UseFont = true;
            this.tePhoneNumber.Properties.NullValuePrompt = "Phone Number";
            this.tePhoneNumber.Size = new System.Drawing.Size(156, 22);
            this.tePhoneNumber.TabIndex = 3;
            // 
            // leRoute
            // 
            this.leRoute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leRoute.Location = new System.Drawing.Point(3, 3);
            this.leRoute.Name = "leRoute";
            this.leRoute.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leRoute.Properties.Appearance.Options.UseFont = true;
            this.leRoute.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leRoute.Properties.NullText = "";
            this.leRoute.Properties.NullValuePrompt = "Route";
            this.leRoute.Size = new System.Drawing.Size(156, 22);
            this.leRoute.TabIndex = 2;
            // 
            // pnStateAndCity
            // 
            this.pnStateAndCity.ColumnCount = 2;
            this.pnStateAndCity.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnStateAndCity.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnStateAndCity.Controls.Add(this.leCity, 0, 0);
            this.pnStateAndCity.Controls.Add(this.leProvince, 0, 0);
            this.pnStateAndCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnStateAndCity.Location = new System.Drawing.Point(130, 112);
            this.pnStateAndCity.Margin = new System.Windows.Forms.Padding(0);
            this.pnStateAndCity.Name = "pnStateAndCity";
            this.pnStateAndCity.RowCount = 1;
            this.pnStateAndCity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnStateAndCity.Size = new System.Drawing.Size(324, 28);
            this.pnStateAndCity.TabIndex = 6;
            // 
            // leCity
            // 
            this.leCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leCity.Location = new System.Drawing.Point(165, 3);
            this.leCity.Name = "leCity";
            this.leCity.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leCity.Properties.Appearance.Options.UseFont = true;
            this.leCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCity.Properties.NullText = "";
            this.leCity.Properties.NullValuePrompt = "City";
            this.leCity.Size = new System.Drawing.Size(156, 22);
            this.leCity.TabIndex = 7;
            // 
            // leProvince
            // 
            this.leProvince.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leProvince.Location = new System.Drawing.Point(3, 3);
            this.leProvince.Name = "leProvince";
            this.leProvince.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leProvince.Properties.Appearance.Options.UseFont = true;
            this.leProvince.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leProvince.Properties.NullText = "";
            this.leProvince.Properties.NullValuePrompt = "State";
            this.leProvince.Size = new System.Drawing.Size(156, 22);
            this.leProvince.TabIndex = 6;
            // 
            // teAddressLine2
            // 
            this.teAddressLine2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teAddressLine2.EditValue = "";
            this.teAddressLine2.Location = new System.Drawing.Point(133, 171);
            this.teAddressLine2.Name = "teAddressLine2";
            this.teAddressLine2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teAddressLine2.Properties.Appearance.Options.UseFont = true;
            this.teAddressLine2.Properties.NullValuePrompt = "Address Line 2";
            this.teAddressLine2.Size = new System.Drawing.Size(318, 22);
            this.teAddressLine2.TabIndex = 8;
            // 
            // pnPostalCode
            // 
            this.pnPostalCode.ColumnCount = 2;
            this.pnPostalCode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnPostalCode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnPostalCode.Controls.Add(this.tePostalCode, 0, 0);
            this.pnPostalCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnPostalCode.Location = new System.Drawing.Point(130, 196);
            this.pnPostalCode.Margin = new System.Windows.Forms.Padding(0);
            this.pnPostalCode.Name = "pnPostalCode";
            this.pnPostalCode.RowCount = 1;
            this.pnPostalCode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnPostalCode.Size = new System.Drawing.Size(324, 28);
            this.pnPostalCode.TabIndex = 9;
            // 
            // tePostalCode
            // 
            this.tePostalCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tePostalCode.EditValue = "";
            this.tePostalCode.Location = new System.Drawing.Point(3, 3);
            this.tePostalCode.Name = "tePostalCode";
            this.tePostalCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tePostalCode.Properties.Appearance.Options.UseFont = true;
            this.tePostalCode.Properties.NullValuePrompt = "Postal Code";
            this.tePostalCode.Size = new System.Drawing.Size(156, 22);
            this.tePostalCode.TabIndex = 9;
            // 
            // CallPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.pnContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CallPanel";
            this.Size = new System.Drawing.Size(454, 700);
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            this.pnCall.ResumeLayout(false);
            this.pnCall.PerformLayout();
            this.pnNumberStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teNumOfCalls.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leNumberStatus.Properties)).EndInit();
            this.pnDisposition.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leDisposition.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teLongitude.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLatitude.Properties)).EndInit();
            this.pnIncomingTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teIncomingTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIncomingDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deIncomingDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCallerName.Properties)).EndInit();
            this.pnCallRouteAndNo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tePhoneNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRoute.Properties)).EndInit();
            this.pnStateAndCity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leProvince.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine2.Properties)).EndInit();
            this.pnPostalCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tePostalCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnContainer;
        private System.Windows.Forms.TableLayoutPanel pnCall;
        private DevExpress.XtraEditors.LabelControl lblDisposition;
        private System.Windows.Forms.TableLayoutPanel pnIncomingTime;
        private DevExpress.XtraEditors.TextEdit teIncomingTime;
        private DevExpress.XtraEditors.DateEdit deIncomingDate;
        private DevExpress.XtraEditors.LabelControl lblIncommingTime;
        private DevExpress.XtraEditors.TextEdit teAddressLine1;
        private DevExpress.XtraEditors.LabelControl lblCallerLocation;
        private DevExpress.XtraEditors.TextEdit teCallerName;
        private DevExpress.XtraEditors.LabelControl lblCallerName;
        private DevExpress.XtraEditors.LabelControl lblCall;
        private System.Windows.Forms.TableLayoutPanel pnCallRouteAndNo;
        private DevExpress.XtraEditors.TextEdit tePhoneNumber;
        private System.Windows.Forms.TableLayoutPanel pnStateAndCity;
        private DevExpress.XtraEditors.LookUpEdit leProvince;
        private DevExpress.XtraEditors.TextEdit teAddressLine2;
        private System.Windows.Forms.TableLayoutPanel pnPostalCode;
        private DevExpress.XtraEditors.LabelControl lblCallSection;
        private DevExpress.XtraEditors.TextEdit tePostalCode;
        private DevExpress.XtraEditors.LookUpEdit leCity;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.TextEdit teLongitude;
        private DevExpress.XtraEditors.TextEdit teLatitude;
        private System.Windows.Forms.TableLayoutPanel pnDisposition;
        private DevExpress.XtraEditors.LookUpEdit leDisposition;
        private System.Windows.Forms.TableLayoutPanel pnNumberStatus;
        private DevExpress.XtraEditors.TextEdit teNumOfCalls;
        private DevExpress.XtraEditors.LookUpEdit leNumberStatus;
        private DevExpress.XtraEditors.LabelControl lblNumberStatus;
        private DevExpress.XtraEditors.LookUpEdit leRoute;
    }
}
