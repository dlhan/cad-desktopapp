﻿using Microsoft.Spatial;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using Cad.Data.BLL.Dispatch;
using Cad.Data.BLL.Person;

namespace Cad.Clients.WinForm.Controls
{
    public partial class CallPanel : UserControl
    {
        #region PROPERTIES
        public ReportCall Call { get; set; } = null;
        #endregion

        #region CONSTRUCTORS
        public CallPanel()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region PRIVATE METHODS
        private void InitialzieLookUpEdit(LookUpEdit edit,
            string displayMemberName,
            string valueMemberName,
            int rows = 5,
            bool showHeader = false,
            bool showFooter = false)
        {
            edit.Properties.DropDownRows = rows;
            edit.Properties.PopupSizeable = false;
            edit.Properties.ShowHeader = showHeader;
            edit.Properties.ShowFooter = showFooter;
            edit.Properties.DisplayMember = displayMemberName;
            edit.Properties.ValueMember = valueMemberName;
            edit.Properties.Columns.Add(new LookUpColumnInfo("Name"));

            switch (edit.Name)
            {
                case "leProvince":
                    leProvince.EditValueChanged += Province_EditValueChanged;
                    break;
                default: break;
            }
        }

        private void InitializeControls()
        {
            InitialzieLookUpEdit(leRoute, "Name", "Id", 3);
            InitialzieLookUpEdit(leNumberStatus, "Name", "Id");
            InitialzieLookUpEdit(leProvince, "Name", "StateProvinceCode");
            InitialzieLookUpEdit(leCity, "Name", "CityCode");
            InitialzieLookUpEdit(leDisposition, "Name", "Id");

            // Initialize mask settings
            var settings = teIncomingTime.Properties.MaskSettings
                .Configure<MaskSettings.DateTime>();
            settings.MaskExpression = "HH:mm";
            settings.SpinWithCarry = true;
            teIncomingTime.Properties.UseMaskAsDisplayFormat = true;
        }
        #endregion

        #region PUBLIC METHODS
        public void BindData()
        {
            List<CallRoute> routes = CallRoute.GetCallRoutes();
            if (routes != null && routes.Count > 0)
                leRoute.Properties.DataSource = routes;

            List<CallNumberStatus> numberStatuses = CallNumberStatus.GetCallNumberStatuses();
            if (numberStatuses != null && numberStatuses.Count > 0)
                leNumberStatus.Properties.DataSource = numberStatuses;

            List<StateProvince> provinces = StateProvince.GetStateProvinces("BD");
            if (provinces != null && provinces.Count > 0)
            {
                leProvince.Properties.DataSource = provinces;
                leProvince.EditValue = "BD-A";
            }

            List<CallDisposition> dispositions = CallDisposition.GetCallDispositions();
            if (dispositions != null && dispositions.Count > 0)
                leDisposition.Properties.DataSource = dispositions;

            if (Call == null) return;
            deIncomingDate.DateTime = Call.IncomingDate;
            teIncomingTime.Text = Call.IncomingDate
                                .ToString(Resources.TimeHrMinFormat);
            leRoute.EditValue = Call.Route.Id;
            tePhoneNumber.Text = Call.CallNumber.PhoneNumber;
            leNumberStatus.EditValue = Call.CallNumber.NumberStatus.Id;
            teNumOfCalls.EditValue = Call.CallNumber.NumOfCalls;
            teCallerName.Text = Call.CallerName;
            leProvince.EditValue = Call.Location.City.StateProvince.StateProvinceCode;
            leCity.EditValue = Call.Location.City.CityCode;
            teAddressLine1.Text = Call.Location.AddressLine1;
            teAddressLine2.Text = Call.Location.AddressLine2;
            tePostalCode.Text = Call.Location.PostalCode;
            teLatitude.Text = Call.Location.SpatialLocation.Latitude.ToString();
            teLongitude.Text = Call.Location.SpatialLocation.Longitude.ToString();
            leDisposition.EditValue = Call.Disposition.Id;
        }
        #endregion

        #region EVENTS
        private void Province_EditValueChanged(object sender, System.EventArgs e)
        {
            if (leProvince.EditValue == null) return;

            List<City> cities = City.GetCities(leProvince.EditValue.ToString());
            if (cities != null && cities.Count > 0)
                leCity.Properties.DataSource = cities;
        }
        #endregion 
    }
}
