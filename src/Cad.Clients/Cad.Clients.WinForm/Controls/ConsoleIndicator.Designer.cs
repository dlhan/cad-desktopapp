﻿
namespace Cad.Clients.WinForm.Controls
{
    partial class ConsoleIndicator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContainer = new System.Windows.Forms.TableLayoutPanel();
            this.lblConsoleId = new DevExpress.XtraEditors.LabelControl();
            this.pnContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            this.pnContainer.BackColor = System.Drawing.Color.Transparent;
            this.pnContainer.ColumnCount = 1;
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.Controls.Add(this.lblConsoleId, 0, 0);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Margin = new System.Windows.Forms.Padding(0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.RowCount = 2;
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.pnContainer.Size = new System.Drawing.Size(90, 90);
            this.pnContainer.TabIndex = 0;
            // 
            // lblConsoleId
            // 
            this.lblConsoleId.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsoleId.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblConsoleId.Appearance.Options.UseFont = true;
            this.lblConsoleId.Appearance.Options.UseForeColor = true;
            this.lblConsoleId.Appearance.Options.UseTextOptions = true;
            this.lblConsoleId.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblConsoleId.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblConsoleId.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblConsoleId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblConsoleId.Location = new System.Drawing.Point(3, 3);
            this.lblConsoleId.Name = "lblConsoleId";
            this.lblConsoleId.Size = new System.Drawing.Size(84, 66);
            this.lblConsoleId.TabIndex = 1;
            this.lblConsoleId.Text = "1";
            // 
            // ucConsoleIndicator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Cad.Clients.WinForm.Properties.Resources.console;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.pnContainer);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ucConsoleIndicator";
            this.Size = new System.Drawing.Size(90, 90);
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnContainer;
        private DevExpress.XtraEditors.LabelControl lblConsoleId;
    }
}
