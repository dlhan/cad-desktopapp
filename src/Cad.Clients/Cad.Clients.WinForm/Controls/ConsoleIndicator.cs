﻿//
// Cad.Clients.WinForm.Controls.ucConsoleIndicator
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Drawing;
using System.Windows.Forms;

namespace Cad.Clients.WinForm.Controls
{
    public partial class ConsoleIndicator : UserControl
    {
        #region PROPERTIES
        public string ConsoleId
        {
            get { return lblConsoleId.Text; }
            set 
            {
                if (int.TryParse(value.ToString(), out _))
                    lblConsoleId.Text = value; 
            }
        }

        public Color TextColor
        {
            get { return lblConsoleId.ForeColor; }
            set { lblConsoleId.ForeColor = value; }
        }

        public Image ConsoleImage
        {
            get { return BackgroundImage; }
            set { BackgroundImage = value; }
        }

        public float FontSize
        {
            get { return lblConsoleId.Font.Size; }
            set { lblConsoleId.Font = new Font("Segoe UI", value, FontStyle.Bold); }
        }
        #endregion

        #region CONSTRUCTROS
        public ConsoleIndicator()
        {
            InitializeComponent();
            BackgroundImageLayout = ImageLayout.Stretch;
        }
        #endregion
    }
}
