﻿
namespace Cad.Clients.WinForm.Controls
{
    partial class DispatchNotePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.TableLayout.TableColumnDefinition tableColumnDefinition1 = new DevExpress.XtraEditors.TableLayout.TableColumnDefinition();
            DevExpress.XtraEditors.TableLayout.TableColumnDefinition tableColumnDefinition2 = new DevExpress.XtraEditors.TableLayout.TableColumnDefinition();
            DevExpress.XtraEditors.TableLayout.TableRowDefinition tableRowDefinition1 = new DevExpress.XtraEditors.TableLayout.TableRowDefinition();
            DevExpress.XtraEditors.TableLayout.TableRowDefinition tableRowDefinition2 = new DevExpress.XtraEditors.TableLayout.TableRowDefinition();
            DevExpress.XtraEditors.TableLayout.TableRowDefinition tableRowDefinition3 = new DevExpress.XtraEditors.TableLayout.TableRowDefinition();
            DevExpress.XtraEditors.TableLayout.TableRowDefinition tableRowDefinition4 = new DevExpress.XtraEditors.TableLayout.TableRowDefinition();
            DevExpress.XtraEditors.TableLayout.TableSpan tableSpan1 = new DevExpress.XtraEditors.TableLayout.TableSpan();
            DevExpress.XtraEditors.TableLayout.TableSpan tableSpan2 = new DevExpress.XtraEditors.TableLayout.TableSpan();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement1 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement2 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement3 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement4 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement5 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            this.tvcType = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tvcDispatcher = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tvcCreatedDate = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tvcNote = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.pnContainer = new System.Windows.Forms.TableLayoutPanel();
            this.lblNoteSection = new DevExpress.XtraEditors.LabelControl();
            this.pnDispatchNote = new System.Windows.Forms.TableLayoutPanel();
            this.gcDispatchNote = new DevExpress.XtraGrid.GridControl();
            this.tvDispatchNote = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.tvcDispatchNoteId = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tvcTypeId = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.pnContainer.SuspendLayout();
            this.pnDispatchNote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDispatchNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvDispatchNote)).BeginInit();
            this.SuspendLayout();
            // 
            // tvcType
            // 
            this.tvcType.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvcType.AppearanceCell.Options.UseFont = true;
            this.tvcType.Caption = "TYPE";
            this.tvcType.FieldName = "Type.Name";
            this.tvcType.Name = "tvcType";
            this.tvcType.OptionsColumn.AllowEdit = false;
            this.tvcType.OptionsColumn.AllowFocus = false;
            this.tvcType.Visible = true;
            this.tvcType.VisibleIndex = 1;
            this.tvcType.Width = 200;
            // 
            // tvcDispatcher
            // 
            this.tvcDispatcher.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvcDispatcher.AppearanceCell.Options.UseFont = true;
            this.tvcDispatcher.Caption = "DISPATCHER";
            this.tvcDispatcher.FieldName = "Dispatcher";
            this.tvcDispatcher.Name = "tvcDispatcher";
            this.tvcDispatcher.OptionsColumn.AllowEdit = false;
            this.tvcDispatcher.OptionsColumn.AllowFocus = false;
            this.tvcDispatcher.Visible = true;
            this.tvcDispatcher.VisibleIndex = 2;
            // 
            // tvcCreatedDate
            // 
            this.tvcCreatedDate.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvcCreatedDate.AppearanceCell.Options.UseFont = true;
            this.tvcCreatedDate.Caption = "CREATED DATE";
            this.tvcCreatedDate.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:ss";
            this.tvcCreatedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tvcCreatedDate.FieldName = "CreatedDate";
            this.tvcCreatedDate.MinWidth = 23;
            this.tvcCreatedDate.Name = "tvcCreatedDate";
            this.tvcCreatedDate.OptionsColumn.AllowEdit = false;
            this.tvcCreatedDate.OptionsColumn.AllowFocus = false;
            this.tvcCreatedDate.Visible = true;
            this.tvcCreatedDate.VisibleIndex = 0;
            this.tvcCreatedDate.Width = 110;
            // 
            // tvcNote
            // 
            this.tvcNote.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvcNote.AppearanceCell.Options.UseFont = true;
            this.tvcNote.Caption = "NOTE";
            this.tvcNote.FieldName = "Note";
            this.tvcNote.Name = "tvcNote";
            this.tvcNote.Visible = true;
            this.tvcNote.VisibleIndex = 3;
            this.tvcNote.Width = 319;
            // 
            // pnContainer
            // 
            this.pnContainer.ColumnCount = 1;
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.Controls.Add(this.lblNoteSection, 0, 0);
            this.pnContainer.Controls.Add(this.pnDispatchNote, 0, 1);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.RowCount = 2;
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnContainer.Size = new System.Drawing.Size(454, 700);
            this.pnContainer.TabIndex = 0;
            // 
            // lblNoteSection
            // 
            this.lblNoteSection.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoteSection.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(168)))), ((int)(((byte)(123)))));
            this.lblNoteSection.Appearance.Options.UseFont = true;
            this.lblNoteSection.Appearance.Options.UseForeColor = true;
            this.lblNoteSection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoteSection.Location = new System.Drawing.Point(0, 0);
            this.lblNoteSection.Margin = new System.Windows.Forms.Padding(0);
            this.lblNoteSection.Name = "lblNoteSection";
            this.lblNoteSection.Size = new System.Drawing.Size(454, 28);
            this.lblNoteSection.TabIndex = 3;
            this.lblNoteSection.Text = "NOTES";
            // 
            // pnDispatchNote
            // 
            this.pnDispatchNote.ColumnCount = 1;
            this.pnDispatchNote.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnDispatchNote.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnDispatchNote.Controls.Add(this.gcDispatchNote, 0, 0);
            this.pnDispatchNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDispatchNote.Location = new System.Drawing.Point(0, 28);
            this.pnDispatchNote.Margin = new System.Windows.Forms.Padding(0);
            this.pnDispatchNote.Name = "pnDispatchNote";
            this.pnDispatchNote.RowCount = 1;
            this.pnDispatchNote.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnDispatchNote.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnDispatchNote.Size = new System.Drawing.Size(454, 672);
            this.pnDispatchNote.TabIndex = 4;
            // 
            // gcDispatchNote
            // 
            this.gcDispatchNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDispatchNote.Location = new System.Drawing.Point(3, 3);
            this.gcDispatchNote.MainView = this.tvDispatchNote;
            this.gcDispatchNote.Name = "gcDispatchNote";
            this.gcDispatchNote.Size = new System.Drawing.Size(448, 666);
            this.gcDispatchNote.TabIndex = 3;
            this.gcDispatchNote.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tvDispatchNote});
            // 
            // tvDispatchNote
            // 
            this.tvDispatchNote.Appearance.EmptySpace.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvDispatchNote.Appearance.EmptySpace.Options.UseFont = true;
            this.tvDispatchNote.Appearance.Group.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvDispatchNote.Appearance.Group.Options.UseFont = true;
            this.tvDispatchNote.Appearance.ItemFocused.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvDispatchNote.Appearance.ItemFocused.Options.UseFont = true;
            this.tvDispatchNote.Appearance.ItemHovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tvDispatchNote.Appearance.ItemHovered.Options.UseFont = true;
            this.tvDispatchNote.Appearance.ItemNormal.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvDispatchNote.Appearance.ItemNormal.Options.UseFont = true;
            this.tvDispatchNote.Appearance.ItemPressed.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvDispatchNote.Appearance.ItemPressed.Options.UseFont = true;
            this.tvDispatchNote.Appearance.ItemSelected.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvDispatchNote.Appearance.ItemSelected.Options.UseFont = true;
            this.tvDispatchNote.Appearance.ViewCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tvDispatchNote.Appearance.ViewCaption.Options.UseFont = true;
            this.tvDispatchNote.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.tvDispatchNote.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.tvcDispatchNoteId,
            this.tvcTypeId,
            this.tvcType,
            this.tvcNote,
            this.tvcDispatcher,
            this.tvcCreatedDate});
            this.tvDispatchNote.GridControl = this.gcDispatchNote;
            this.tvDispatchNote.Name = "tvDispatchNote";
            this.tvDispatchNote.OptionsBehavior.AllowSmoothScrolling = true;
            this.tvDispatchNote.OptionsTiles.HighlightFocusedTileStyle = DevExpress.XtraGrid.Views.Tile.HighlightFocusedTileStyle.Content;
            this.tvDispatchNote.OptionsTiles.IndentBetweenItems = 1;
            this.tvDispatchNote.OptionsTiles.ItemPadding = new System.Windows.Forms.Padding(20, 12, 0, 12);
            this.tvDispatchNote.OptionsTiles.ItemSize = new System.Drawing.Size(600, 130);
            this.tvDispatchNote.OptionsTiles.LayoutMode = DevExpress.XtraGrid.Views.Tile.TileViewLayoutMode.List;
            this.tvDispatchNote.OptionsTiles.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tvDispatchNote.OptionsTiles.Padding = new System.Windows.Forms.Padding(0);
            tableColumnDefinition1.Length.Value = 251D;
            tableColumnDefinition2.Length.Value = 146D;
            tableColumnDefinition2.PaddingRight = 10;
            this.tvDispatchNote.TileColumns.Add(tableColumnDefinition1);
            this.tvDispatchNote.TileColumns.Add(tableColumnDefinition2);
            tableRowDefinition1.Length.Value = 13D;
            tableRowDefinition2.Length.Value = 18D;
            tableRowDefinition3.Length.Value = 62D;
            tableRowDefinition3.PaddingBottom = 8;
            tableRowDefinition3.PaddingTop = 6;
            tableRowDefinition4.Length.Value = 15D;
            this.tvDispatchNote.TileRows.Add(tableRowDefinition1);
            this.tvDispatchNote.TileRows.Add(tableRowDefinition2);
            this.tvDispatchNote.TileRows.Add(tableRowDefinition3);
            this.tvDispatchNote.TileRows.Add(tableRowDefinition4);
            tableSpan1.RowSpan = 2;
            tableSpan2.ColumnSpan = 2;
            tableSpan2.RowIndex = 2;
            this.tvDispatchNote.TileSpans.Add(tableSpan1);
            this.tvDispatchNote.TileSpans.Add(tableSpan2);
            tileViewItemElement1.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileViewItemElement1.Appearance.Normal.ForeColor = System.Drawing.Color.Silver;
            tileViewItemElement1.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement1.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement1.ColumnIndex = 1;
            tileViewItemElement1.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement1.Text = "DISPATCHER";
            tileViewItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileViewItemElement2.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileViewItemElement2.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(168)))), ((int)(((byte)(123)))));
            tileViewItemElement2.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement2.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement2.Appearance.Normal.Options.UseTextOptions = true;
            tileViewItemElement2.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileViewItemElement2.Column = this.tvcType;
            tileViewItemElement2.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement2.Text = "tvcType";
            tileViewItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileViewItemElement3.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileViewItemElement3.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(200)))), ((int)(((byte)(170)))));
            tileViewItemElement3.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement3.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement3.Column = this.tvcDispatcher;
            tileViewItemElement3.ColumnIndex = 1;
            tileViewItemElement3.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement3.RowIndex = 1;
            tileViewItemElement3.Text = "tvcDispatcher";
            tileViewItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileViewItemElement4.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileViewItemElement4.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(200)))), ((int)(((byte)(170)))));
            tileViewItemElement4.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement4.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement4.Column = this.tvcCreatedDate;
            tileViewItemElement4.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement4.RowIndex = 3;
            tileViewItemElement4.Text = "tvcCreatedDate";
            tileViewItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            tileViewItemElement5.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileViewItemElement5.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement5.Column = this.tvcNote;
            tileViewItemElement5.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement5.RowIndex = 2;
            tileViewItemElement5.Text = "tvcNote";
            tileViewItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            this.tvDispatchNote.TileTemplate.Add(tileViewItemElement1);
            this.tvDispatchNote.TileTemplate.Add(tileViewItemElement2);
            this.tvDispatchNote.TileTemplate.Add(tileViewItemElement3);
            this.tvDispatchNote.TileTemplate.Add(tileViewItemElement4);
            this.tvDispatchNote.TileTemplate.Add(tileViewItemElement5);
            // 
            // tvcDispatchNoteId
            // 
            this.tvcDispatchNoteId.Caption = "Id";
            this.tvcDispatchNoteId.FieldName = "DispatchNoteId";
            this.tvcDispatchNoteId.Name = "tvcDispatchNoteId";
            // 
            // tvcTypeId
            // 
            this.tvcTypeId.Caption = "TYPE ID";
            this.tvcTypeId.FieldName = "Type.DispatchNoteTypeId";
            this.tvcTypeId.Name = "tvcTypeId";
            // 
            // DispatchNotePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.pnContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Name = "DispatchNotePanel";
            this.Size = new System.Drawing.Size(454, 700);
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            this.pnDispatchNote.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDispatchNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvDispatchNote)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnContainer;
        private DevExpress.XtraEditors.LabelControl lblNoteSection;
        private System.Windows.Forms.TableLayoutPanel pnDispatchNote;
        private DevExpress.XtraGrid.GridControl gcDispatchNote;
        private DevExpress.XtraGrid.Views.Tile.TileView tvDispatchNote;
        private DevExpress.XtraGrid.Columns.TileViewColumn tvcDispatchNoteId;
        private DevExpress.XtraGrid.Columns.TileViewColumn tvcTypeId;
        private DevExpress.XtraGrid.Columns.TileViewColumn tvcType;
        private DevExpress.XtraGrid.Columns.TileViewColumn tvcNote;
        private DevExpress.XtraGrid.Columns.TileViewColumn tvcDispatcher;
        private DevExpress.XtraGrid.Columns.TileViewColumn tvcCreatedDate;
    }
}
