﻿//
// Cad.Clients.WinForm.Controls.ucDispatchNotePanel
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Tile;
using Cad.Clients.WinForm.Forms;
//using Cad.Data.BLL.Dispatch;

namespace Cad.Clients.WinForm.Controls
{
    public partial class DispatchNotePanel : UserControl
    {
        #region VARIABLES
        //private BindingList<DispatchNote> notes;
        #endregion

        #region CONSTRUCTORS
        public DispatchNotePanel()
        {
            InitializeComponent();
        }
        #endregion

        #region PUBLIC METHOD
        public void AddNewNote()
        {
            //DispatchNote newNote = notes.AddNew();
            //if (newNote.Note == null || newNote.Dispatcher == null) 
            //    return;

            //newNote.DispatchNoteId = DispatchNote.InsertDispatchNote(newNote);
            //if (newNote.DispatchNoteId <= 0)
            //    notes.CancelNew(notes.IndexOf(newNote));
            //else
            //    notes.EndNew(notes.IndexOf(newNote));
        }

        public void EditSelectedNote()
        {
            //DispatchNote note = tvDispatchNote.GetFocusedRow() as DispatchNote;
            //EditDispatchNoteForm form = new EditDispatchNoteForm()
            //{
            //    DispatchNote = note
            //};

            //if (form.ShowDialog() != DialogResult.OK) return;
            //if (DispatchNote.UpdateDispatchNote(note.DispatchNoteId,
            //    form.DispatchNote.Type.DispatchNoteTypeId,
            //    form.DispatchNote.Note, note.Dispatcher, 
            //    note.CreatedDate))
            //{
            //    tvDispatchNote.SetFocusedRowCellValue("TypeId", note.Type.DispatchNoteTypeId);
            //    tvDispatchNote.SetFocusedRowCellValue("Type", note.Type.Name);
            //    tvDispatchNote.SetFocusedRowCellValue("Note", note.Note);

            //    //DispatchNote t = notes.SingleOrDefault(n => n.DispatchNoteId == note.DispatchNoteId);
            //    //MessageBox.Show(t.Note);
            //}
        }

        public void DeleteSelectedNote()
        {
            //DispatchNote note = tvDispatchNote.GetFocusedRow() as DispatchNote;
            //if (DispatchNote.DeleteDispatchNote(note.DispatchNoteId))
            //    tvDispatchNote.DeleteRow(tvDispatchNote.FocusedRowHandle);
        }

        public void BindData()
        {
            //notes = new BindingList<DispatchNote>(
            //    DispatchNote.GetDispatchNotes());
            //notes.AllowNew = true;
            //notes.AllowEdit = true;
            //notes.AddingNew += new AddingNewEventHandler(notes_AddingNew);

            //gcDispatchNote.DataSource = notes;
        }
        #endregion

        #region EVENTS
        private void notes_AddingNew(object sender, AddingNewEventArgs e)
        {
            //EditDispatchNoteForm form = new EditDispatchNoteForm();
            //if (form.ShowDialog() != DialogResult.OK) return;
            
            //e.NewObject = new DispatchNote()
            //{
            //    Type = form.DispatchNote.Type,
            //    Note = form.DispatchNote.Note,
            //    Dispatcher = "Joohyoung Kim",
            //    CreatedDate = DateTime.UtcNow
            //};
        }

        private void tvDispatchNote_ItemDoubleClick(
            object sender, 
            TileViewItemClickEventArgs e)
        {
            //EditSelectedNote();
        }
        #endregion
    }
}
