﻿
namespace Cad.Clients.WinForm.Controls
{
    partial class DispatchUnitPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContainer = new System.Windows.Forms.TableLayoutPanel();
            this.lblUnitSection = new DevExpress.XtraEditors.LabelControl();
            this.pnDispatchUnit = new System.Windows.Forms.TableLayoutPanel();
            this.gcUnits = new DevExpress.XtraGrid.GridControl();
            this.gvUnits = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSquadId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlateNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDispatch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnroute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArrived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCleared = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnContainer.SuspendLayout();
            this.pnDispatchUnit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnits)).BeginInit();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            this.pnContainer.ColumnCount = 1;
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.Controls.Add(this.lblUnitSection, 0, 0);
            this.pnContainer.Controls.Add(this.pnDispatchUnit, 0, 1);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.RowCount = 2;
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnContainer.Size = new System.Drawing.Size(850, 700);
            this.pnContainer.TabIndex = 0;
            // 
            // lblUnitSection
            // 
            this.lblUnitSection.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnitSection.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(168)))), ((int)(((byte)(123)))));
            this.lblUnitSection.Appearance.Options.UseFont = true;
            this.lblUnitSection.Appearance.Options.UseForeColor = true;
            this.lblUnitSection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnitSection.Location = new System.Drawing.Point(0, 0);
            this.lblUnitSection.Margin = new System.Windows.Forms.Padding(0);
            this.lblUnitSection.Name = "lblUnitSection";
            this.lblUnitSection.Size = new System.Drawing.Size(850, 28);
            this.lblUnitSection.TabIndex = 6;
            this.lblUnitSection.Text = "UNITS";
            this.lblUnitSection.ToolTip = "Dispatched Units";
            // 
            // pnDispatchUnit
            // 
            this.pnDispatchUnit.ColumnCount = 1;
            this.pnDispatchUnit.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnDispatchUnit.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnDispatchUnit.Controls.Add(this.gcUnits, 0, 0);
            this.pnDispatchUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnDispatchUnit.Location = new System.Drawing.Point(0, 28);
            this.pnDispatchUnit.Margin = new System.Windows.Forms.Padding(0);
            this.pnDispatchUnit.Name = "pnDispatchUnit";
            this.pnDispatchUnit.RowCount = 1;
            this.pnDispatchUnit.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnDispatchUnit.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnDispatchUnit.Size = new System.Drawing.Size(850, 672);
            this.pnDispatchUnit.TabIndex = 7;
            // 
            // gcUnits
            // 
            this.gcUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcUnits.Location = new System.Drawing.Point(3, 3);
            this.gcUnits.MainView = this.gvUnits;
            this.gcUnits.Name = "gcUnits";
            this.gcUnits.Size = new System.Drawing.Size(844, 666);
            this.gcUnits.TabIndex = 1;
            this.gcUnits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUnits});
            // 
            // gvUnits
            // 
            this.gvUnits.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvUnits.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvUnits.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSquadId,
            this.colDepartment,
            this.colVehicleType,
            this.colPlateNumber,
            this.colDispatch,
            this.colEnroute,
            this.colArrived,
            this.colCleared});
            this.gvUnits.GridControl = this.gcUnits;
            this.gvUnits.Name = "gvUnits";
            this.gvUnits.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.True;
            this.gvUnits.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gvUnits.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvUnits.OptionsDetail.EnableMasterViewMode = false;
            this.gvUnits.OptionsView.ShowGroupedColumns = true;
            this.gvUnits.OptionsView.ShowGroupPanel = false;
            this.gvUnits.OptionsView.ShowIndicator = false;
            this.gvUnits.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colSquadId
            // 
            this.colSquadId.Caption = "Squad ID";
            this.colSquadId.Name = "colSquadId";
            this.colSquadId.Width = 20;
            // 
            // colDepartment
            // 
            this.colDepartment.Caption = "Department";
            this.colDepartment.MaxWidth = 120;
            this.colDepartment.MinWidth = 120;
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 0;
            this.colDepartment.Width = 120;
            // 
            // colVehicleType
            // 
            this.colVehicleType.Caption = "Type";
            this.colVehicleType.MaxWidth = 100;
            this.colVehicleType.MinWidth = 100;
            this.colVehicleType.Name = "colVehicleType";
            this.colVehicleType.Visible = true;
            this.colVehicleType.VisibleIndex = 1;
            this.colVehicleType.Width = 100;
            // 
            // colPlateNumber
            // 
            this.colPlateNumber.Caption = "Plate Number";
            this.colPlateNumber.MaxWidth = 120;
            this.colPlateNumber.MinWidth = 120;
            this.colPlateNumber.Name = "colPlateNumber";
            this.colPlateNumber.Visible = true;
            this.colPlateNumber.VisibleIndex = 2;
            this.colPlateNumber.Width = 120;
            // 
            // colDispatch
            // 
            this.colDispatch.Caption = "Dispatch";
            this.colDispatch.MaxWidth = 120;
            this.colDispatch.MinWidth = 70;
            this.colDispatch.Name = "colDispatch";
            this.colDispatch.Visible = true;
            this.colDispatch.VisibleIndex = 3;
            this.colDispatch.Width = 120;
            // 
            // colEnroute
            // 
            this.colEnroute.Caption = "Enroute";
            this.colEnroute.MaxWidth = 120;
            this.colEnroute.MinWidth = 70;
            this.colEnroute.Name = "colEnroute";
            this.colEnroute.Visible = true;
            this.colEnroute.VisibleIndex = 4;
            this.colEnroute.Width = 120;
            // 
            // colArrived
            // 
            this.colArrived.Caption = "Arrived";
            this.colArrived.MaxWidth = 120;
            this.colArrived.MinWidth = 70;
            this.colArrived.Name = "colArrived";
            this.colArrived.Visible = true;
            this.colArrived.VisibleIndex = 5;
            this.colArrived.Width = 120;
            // 
            // colCleared
            // 
            this.colCleared.Caption = "Cleared";
            this.colCleared.MaxWidth = 120;
            this.colCleared.MinWidth = 70;
            this.colCleared.Name = "colCleared";
            this.colCleared.Visible = true;
            this.colCleared.VisibleIndex = 6;
            this.colCleared.Width = 120;
            // 
            // DispatchUnitPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.pnContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DispatchUnitPanel";
            this.Size = new System.Drawing.Size(850, 700);
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            this.pnDispatchUnit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnits)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnContainer;
        private DevExpress.XtraEditors.LabelControl lblUnitSection;
        private System.Windows.Forms.TableLayoutPanel pnDispatchUnit;
        private DevExpress.XtraGrid.GridControl gcUnits;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colSquadId;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleType;
        private DevExpress.XtraGrid.Columns.GridColumn colPlateNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDispatch;
        private DevExpress.XtraGrid.Columns.GridColumn colEnroute;
        private DevExpress.XtraGrid.Columns.GridColumn colArrived;
        private DevExpress.XtraGrid.Columns.GridColumn colCleared;
    }
}
