﻿//
// Cad.Clients.WinForm.Controls.DispatchUnitPanel
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
//using DevExpress.XtraGrid.Views.Grid;
using Cad.Clients.WinForm.Helper;
using Cad.Data.BLL.Dispatch;

namespace Cad.Clients.WinForm.Controls
{
    public partial class DispatchUnitPanel : UserControl
    {
        #region VARIABLES
        private readonly string[] fieldNames = 
        {
            "SquadId", "Vehicle.Department.Name", "Vehicle.Type.Name",
            "Vehicle.PlateNumber", "DispatchStatus.ChangedDate",
            "EnrouteStatus.ChangedDate", "ArrivedStatus.ChangedDate",
            "ClearedStatus.ChangedDate"
        };
        #endregion

        #region PROPERTIES
        private TextEdit ActiveEdit
        {
            get { return gvUnits.ActiveEditor as TextEdit; }
        }

        private bool AllowEdit
        {
            get
            {
                if (CurrentSquadUnit == null) return false;
                if (gvUnits.SelectedRowsCount == 0) return false;
                if (gvUnits.FocusedColumn.AbsoluteIndex < 4) return false;
                return true;
            }
        }

        private SquadUnit CurrentSquadUnit
        {
            get
            {
                if (gvUnits.FocusedRowHandle < 0) return null;
                return gvUnits.GetRow(gvUnits.FocusedRowHandle) as SquadUnit;
            }
        }

        public List<Squad> Squads { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DispatchUnitPanel()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region PRIVATE METHODS
        private UnitStatus CreateUnitStatus(SquadUnit squadUnit, 
            short statusTypeId, DateTime changedDate)
        {
            return new UnitStatus()
            {
                SquadId = squadUnit.SquadId,
                VehicleId = squadUnit.VehicleId,
                UnitStatusTypeId = statusTypeId,
                ChangedDate = changedDate
            };
        }

        private void CreateUnitStatus(int columnIndex, 
            SquadUnit unit, DateTime changedDate)
        {
            switch (columnIndex)
            {
                case 4:
                    CurrentSquadUnit.DispatchStatus = 
                        CreateUnitStatus(unit, 1, changedDate);
                    break;
                case 5:
                    CurrentSquadUnit.EnrouteStatus = 
                        CreateUnitStatus(unit, 2, changedDate);
                    break;
                case 6:
                    CurrentSquadUnit.ArrivedStatus =
                        CreateUnitStatus(unit, 3, changedDate);
                    break;
                case 7:
                    CurrentSquadUnit.ClearedStatus =
                        CreateUnitStatus(unit, 4, changedDate);
                    break;
                default: break;
            }
        }

        private UnitStatus GetSquadUnitStatus(int columnIndex, SquadUnit unit)
        {
            switch (columnIndex)
            {
                case 4: return unit.DispatchStatus;
                case 5: return unit.EnrouteStatus;
                case 6: return unit.ArrivedStatus;
                case 7: return unit.ClearedStatus;
                default: return null;
            }
        }

        private void EditUnitStatus(GridColumn column, DateTime changedDate)
        {
            UnitStatus unitStatus = GetSquadUnitStatus(
                column.AbsoluteIndex, CurrentSquadUnit);
            if (unitStatus == null)
            {
                CreateUnitStatus(column.AbsoluteIndex, 
                    CurrentSquadUnit, changedDate);
            }
            else
            {
                if (unitStatus.ChangedDate.Equals(changedDate)) return;
                unitStatus.ChangedDate = changedDate;
                UnitStatus.UpdateUnitStatus(unitStatus);
            }
        }

        private bool IsValidDate(TextEdit edit)
        {
            if (edit == null) return false;
            return edit.Text.Length == Resources.DateTimeFullFormat.Length;
        }

        private void InitializeGridControl()
        {
            gcUnits.RepositoryItems.Add(
                RepositoryItemHelper.CreateTimeTextEdit("riteDateTime"));

            for (int i = 0; i < gvUnits.Columns.Count; i++)
            {
                GridHelper.SetColumn(gvUnits.Columns[i],
                    new GridColumnProperty(fieldNames[i],
                        (i < 4 ? FormatType.None : FormatType.Custom),
                        (i < 4 ? string.Empty : Resources.TimeHrMinSecFormat),
                        (i < 4 ? null : gcUnits.RepositoryItems["riteDateTime"])
                    )
                );
            }

            gvUnits.KeyDown += gvUnits_KeyDown;
            gvUnits.LostFocus += gvUnits_LostFocus;
            gvUnits.ShownEditor += gvUnits_ShownEditor;
            gvUnits.ShowingEditor += gvUnits_ShowingEditor;
        }

        private void InitializeControls()
        {
            InitializeGridControl();
        }
        #endregion

        #region PUBLIC METHODS
        public void BindData()
        {
            if (Squads == null || Squads.Count == 0) return;

            List<SquadUnit> squadUnits = new List<SquadUnit>();
            foreach (Squad squad in Squads)
                squadUnits.AddRange(squad.Units);

            gcUnits.DataSource = squadUnits;
        }
        #endregion

        #region CONTRON EVENTS
        private void gvUnits_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData != Keys.Enter) return;
            if (gvUnits.FocusedRowHandle < 0) return;
            if (!IsValidDate(ActiveEdit)) return;

            EditUnitStatus(gvUnits.FocusedColumn, 
                DateTime.Parse(ActiveEdit.Text));
        }

        private void gvUnits_LostFocus(object sender, EventArgs e)
        {
            if (!IsValidDate(ActiveEdit)) return;

            EditUnitStatus(gvUnits.FocusedColumn, 
                DateTime.Parse(ActiveEdit.Text));
        }

        private void gvUnits_ShownEditor(object sender, EventArgs e)
        {
            if (ActiveEdit == null) return;
            if (ActiveEdit.Text.Length == 0)
            {
                ActiveEdit.Text = DateTime.UtcNow.ToString(
                    Resources.DateTimeFullFormat);
            }
        }

        private void gvUnits_ShowingEditor(object sender, CancelEventArgs e)
        {
            //e.Cancel = !AllowEdit;
        }
        #endregion
    }
}
