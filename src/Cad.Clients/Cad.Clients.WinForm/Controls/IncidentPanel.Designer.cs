﻿
namespace Cad.Clients.WinForm.Controls
{
    partial class IncidentPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContainer = new System.Windows.Forms.TableLayoutPanel();
            this.lblIncidentSection = new DevExpress.XtraEditors.LabelControl();
            this.pnIncident = new System.Windows.Forms.TableLayoutPanel();
            this.pnClosedTime = new System.Windows.Forms.TableLayoutPanel();
            this.teClosedTime = new DevExpress.XtraEditors.TextEdit();
            this.deClosedDate = new DevExpress.XtraEditors.DateEdit();
            this.pnOpenedTime = new System.Windows.Forms.TableLayoutPanel();
            this.teOpenedTime = new DevExpress.XtraEditors.TextEdit();
            this.deOpenedDate = new DevExpress.XtraEditors.DateEdit();
            this.lblSynopsys = new DevExpress.XtraEditors.LabelControl();
            this.lblClosedTime = new DevExpress.XtraEditors.LabelControl();
            this.lblOpenedTime = new DevExpress.XtraEditors.LabelControl();
            this.pnPostalCode = new System.Windows.Forms.TableLayoutPanel();
            this.tePostalCode = new DevExpress.XtraEditors.TextEdit();
            this.teAddressLine2 = new DevExpress.XtraEditors.TextEdit();
            this.teAddressLine1 = new DevExpress.XtraEditors.TextEdit();
            this.pnStateAndCity = new System.Windows.Forms.TableLayoutPanel();
            this.leCity = new DevExpress.XtraEditors.LookUpEdit();
            this.leProvince = new DevExpress.XtraEditors.LookUpEdit();
            this.lblLocation = new DevExpress.XtraEditors.LabelControl();
            this.pnScale = new System.Windows.Forms.TableLayoutPanel();
            this.leScale = new DevExpress.XtraEditors.LookUpEdit();
            this.lblScale = new DevExpress.XtraEditors.LabelControl();
            this.pnCategory = new System.Windows.Forms.TableLayoutPanel();
            this.leSubcategory = new DevExpress.XtraEditors.LookUpEdit();
            this.leCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.lblCategory = new DevExpress.XtraEditors.LabelControl();
            this.meSynopsis = new DevExpress.XtraEditors.MemoEdit();
            this.pnSpatialLocatino = new System.Windows.Forms.TableLayoutPanel();
            this.teLongitude = new DevExpress.XtraEditors.TextEdit();
            this.teLatitude = new DevExpress.XtraEditors.TextEdit();
            this.pnContainer.SuspendLayout();
            this.pnIncident.SuspendLayout();
            this.pnClosedTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teClosedTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClosedDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClosedDate.Properties)).BeginInit();
            this.pnOpenedTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teOpenedTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deOpenedDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deOpenedDate.Properties)).BeginInit();
            this.pnPostalCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tePostalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine1.Properties)).BeginInit();
            this.pnStateAndCity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leProvince.Properties)).BeginInit();
            this.pnScale.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leScale.Properties)).BeginInit();
            this.pnCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leSubcategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meSynopsis.Properties)).BeginInit();
            this.pnSpatialLocatino.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLongitude.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLatitude.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            this.pnContainer.ColumnCount = 1;
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnContainer.Controls.Add(this.lblIncidentSection, 0, 0);
            this.pnContainer.Controls.Add(this.pnIncident, 0, 1);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.RowCount = 2;
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnContainer.Size = new System.Drawing.Size(450, 700);
            this.pnContainer.TabIndex = 0;
            // 
            // lblIncidentSection
            // 
            this.lblIncidentSection.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncidentSection.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(168)))), ((int)(((byte)(123)))));
            this.lblIncidentSection.Appearance.Options.UseFont = true;
            this.lblIncidentSection.Appearance.Options.UseForeColor = true;
            this.lblIncidentSection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIncidentSection.Location = new System.Drawing.Point(0, 0);
            this.lblIncidentSection.Margin = new System.Windows.Forms.Padding(0);
            this.lblIncidentSection.Name = "lblIncidentSection";
            this.lblIncidentSection.Size = new System.Drawing.Size(450, 28);
            this.lblIncidentSection.TabIndex = 4;
            this.lblIncidentSection.Text = "INCIDENT";
            // 
            // pnIncident
            // 
            this.pnIncident.ColumnCount = 2;
            this.pnIncident.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.pnIncident.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnIncident.Controls.Add(this.pnClosedTime, 1, 8);
            this.pnIncident.Controls.Add(this.pnOpenedTime, 1, 7);
            this.pnIncident.Controls.Add(this.lblSynopsys, 0, 9);
            this.pnIncident.Controls.Add(this.lblClosedTime, 0, 8);
            this.pnIncident.Controls.Add(this.lblOpenedTime, 0, 7);
            this.pnIncident.Controls.Add(this.pnPostalCode, 1, 5);
            this.pnIncident.Controls.Add(this.teAddressLine2, 1, 4);
            this.pnIncident.Controls.Add(this.teAddressLine1, 1, 3);
            this.pnIncident.Controls.Add(this.pnStateAndCity, 1, 2);
            this.pnIncident.Controls.Add(this.lblLocation, 0, 2);
            this.pnIncident.Controls.Add(this.pnScale, 1, 1);
            this.pnIncident.Controls.Add(this.lblScale, 0, 1);
            this.pnIncident.Controls.Add(this.pnCategory, 1, 0);
            this.pnIncident.Controls.Add(this.lblCategory, 0, 0);
            this.pnIncident.Controls.Add(this.meSynopsis, 1, 9);
            this.pnIncident.Controls.Add(this.pnSpatialLocatino, 1, 6);
            this.pnIncident.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnIncident.Location = new System.Drawing.Point(0, 28);
            this.pnIncident.Margin = new System.Windows.Forms.Padding(0);
            this.pnIncident.Name = "pnIncident";
            this.pnIncident.RowCount = 10;
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnIncident.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnIncident.Size = new System.Drawing.Size(450, 672);
            this.pnIncident.TabIndex = 2;
            // 
            // pnClosedTime
            // 
            this.pnClosedTime.ColumnCount = 2;
            this.pnClosedTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnClosedTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnClosedTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnClosedTime.Controls.Add(this.teClosedTime, 1, 0);
            this.pnClosedTime.Controls.Add(this.deClosedDate, 0, 0);
            this.pnClosedTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnClosedTime.Location = new System.Drawing.Point(130, 224);
            this.pnClosedTime.Margin = new System.Windows.Forms.Padding(0);
            this.pnClosedTime.Name = "pnClosedTime";
            this.pnClosedTime.RowCount = 1;
            this.pnClosedTime.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnClosedTime.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnClosedTime.Size = new System.Drawing.Size(320, 28);
            this.pnClosedTime.TabIndex = 15;
            // 
            // teClosedTime
            // 
            this.teClosedTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teClosedTime.Location = new System.Drawing.Point(163, 3);
            this.teClosedTime.Name = "teClosedTime";
            this.teClosedTime.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teClosedTime.Properties.Appearance.Options.UseFont = true;
            this.teClosedTime.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.DateTimeMaskManager));
            this.teClosedTime.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.teClosedTime.Properties.MaskSettings.Set("mask", "\"HH:mm\"");
            this.teClosedTime.Properties.NullValuePrompt = "--:--";
            this.teClosedTime.Size = new System.Drawing.Size(154, 22);
            this.teClosedTime.TabIndex = 1;
            // 
            // deClosedDate
            // 
            this.deClosedDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deClosedDate.EditValue = null;
            this.deClosedDate.Location = new System.Drawing.Point(3, 3);
            this.deClosedDate.Name = "deClosedDate";
            this.deClosedDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deClosedDate.Properties.Appearance.Options.UseFont = true;
            this.deClosedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deClosedDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deClosedDate.Size = new System.Drawing.Size(154, 22);
            this.deClosedDate.TabIndex = 0;
            // 
            // pnOpenedTime
            // 
            this.pnOpenedTime.ColumnCount = 2;
            this.pnOpenedTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnOpenedTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnOpenedTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnOpenedTime.Controls.Add(this.teOpenedTime, 1, 0);
            this.pnOpenedTime.Controls.Add(this.deOpenedDate, 0, 0);
            this.pnOpenedTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnOpenedTime.Location = new System.Drawing.Point(130, 196);
            this.pnOpenedTime.Margin = new System.Windows.Forms.Padding(0);
            this.pnOpenedTime.Name = "pnOpenedTime";
            this.pnOpenedTime.RowCount = 1;
            this.pnOpenedTime.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnOpenedTime.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnOpenedTime.Size = new System.Drawing.Size(320, 28);
            this.pnOpenedTime.TabIndex = 14;
            // 
            // teOpenedTime
            // 
            this.teOpenedTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teOpenedTime.Location = new System.Drawing.Point(163, 3);
            this.teOpenedTime.Name = "teOpenedTime";
            this.teOpenedTime.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teOpenedTime.Properties.Appearance.Options.UseFont = true;
            this.teOpenedTime.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.DateTimeMaskManager));
            this.teOpenedTime.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.teOpenedTime.Properties.MaskSettings.Set("mask", "\"HH:mm\"");
            this.teOpenedTime.Properties.NullValuePrompt = "--:--";
            this.teOpenedTime.Size = new System.Drawing.Size(154, 22);
            this.teOpenedTime.TabIndex = 1;
            // 
            // deOpenedDate
            // 
            this.deOpenedDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deOpenedDate.EditValue = null;
            this.deOpenedDate.Location = new System.Drawing.Point(3, 3);
            this.deOpenedDate.Name = "deOpenedDate";
            this.deOpenedDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deOpenedDate.Properties.Appearance.Options.UseFont = true;
            this.deOpenedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deOpenedDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deOpenedDate.Size = new System.Drawing.Size(154, 22);
            this.deOpenedDate.TabIndex = 0;
            // 
            // lblSynopsys
            // 
            this.lblSynopsys.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSynopsys.Appearance.Options.UseFont = true;
            this.lblSynopsys.Appearance.Options.UseTextOptions = true;
            this.lblSynopsys.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblSynopsys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSynopsys.Location = new System.Drawing.Point(3, 255);
            this.lblSynopsys.Name = "lblSynopsys";
            this.lblSynopsys.Size = new System.Drawing.Size(124, 414);
            this.lblSynopsys.TabIndex = 13;
            this.lblSynopsys.Text = "Synopsys:";
            // 
            // lblClosedTime
            // 
            this.lblClosedTime.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClosedTime.Appearance.Options.UseFont = true;
            this.lblClosedTime.Appearance.Options.UseTextOptions = true;
            this.lblClosedTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblClosedTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblClosedTime.Location = new System.Drawing.Point(3, 227);
            this.lblClosedTime.Name = "lblClosedTime";
            this.lblClosedTime.Size = new System.Drawing.Size(124, 22);
            this.lblClosedTime.TabIndex = 12;
            this.lblClosedTime.Text = "Closed time:";
            // 
            // lblOpenedTime
            // 
            this.lblOpenedTime.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpenedTime.Appearance.Options.UseFont = true;
            this.lblOpenedTime.Appearance.Options.UseTextOptions = true;
            this.lblOpenedTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblOpenedTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOpenedTime.Location = new System.Drawing.Point(3, 199);
            this.lblOpenedTime.Name = "lblOpenedTime";
            this.lblOpenedTime.Size = new System.Drawing.Size(124, 22);
            this.lblOpenedTime.TabIndex = 11;
            this.lblOpenedTime.Text = "Opened time:";
            // 
            // pnPostalCode
            // 
            this.pnPostalCode.ColumnCount = 2;
            this.pnPostalCode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnPostalCode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnPostalCode.Controls.Add(this.tePostalCode, 0, 0);
            this.pnPostalCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnPostalCode.Location = new System.Drawing.Point(130, 140);
            this.pnPostalCode.Margin = new System.Windows.Forms.Padding(0);
            this.pnPostalCode.Name = "pnPostalCode";
            this.pnPostalCode.RowCount = 1;
            this.pnPostalCode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnPostalCode.Size = new System.Drawing.Size(320, 28);
            this.pnPostalCode.TabIndex = 6;
            // 
            // tePostalCode
            // 
            this.tePostalCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tePostalCode.EditValue = "";
            this.tePostalCode.Location = new System.Drawing.Point(3, 3);
            this.tePostalCode.Name = "tePostalCode";
            this.tePostalCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tePostalCode.Properties.Appearance.Options.UseFont = true;
            this.tePostalCode.Properties.NullValuePrompt = "Postal Code";
            this.tePostalCode.Size = new System.Drawing.Size(154, 22);
            this.tePostalCode.TabIndex = 6;
            // 
            // teAddressLine2
            // 
            this.teAddressLine2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teAddressLine2.EditValue = "";
            this.teAddressLine2.Location = new System.Drawing.Point(133, 115);
            this.teAddressLine2.Name = "teAddressLine2";
            this.teAddressLine2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teAddressLine2.Properties.Appearance.Options.UseFont = true;
            this.teAddressLine2.Properties.NullValuePrompt = "Address Line 2";
            this.teAddressLine2.Size = new System.Drawing.Size(314, 22);
            this.teAddressLine2.TabIndex = 5;
            // 
            // teAddressLine1
            // 
            this.teAddressLine1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teAddressLine1.EditValue = "";
            this.teAddressLine1.Location = new System.Drawing.Point(133, 87);
            this.teAddressLine1.Name = "teAddressLine1";
            this.teAddressLine1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teAddressLine1.Properties.Appearance.Options.UseFont = true;
            this.teAddressLine1.Properties.NullValuePrompt = "Address Line 1";
            this.teAddressLine1.Size = new System.Drawing.Size(314, 22);
            this.teAddressLine1.TabIndex = 4;
            // 
            // pnStateAndCity
            // 
            this.pnStateAndCity.ColumnCount = 2;
            this.pnStateAndCity.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnStateAndCity.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnStateAndCity.Controls.Add(this.leCity, 0, 0);
            this.pnStateAndCity.Controls.Add(this.leProvince, 0, 0);
            this.pnStateAndCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnStateAndCity.Location = new System.Drawing.Point(130, 56);
            this.pnStateAndCity.Margin = new System.Windows.Forms.Padding(0);
            this.pnStateAndCity.Name = "pnStateAndCity";
            this.pnStateAndCity.RowCount = 1;
            this.pnStateAndCity.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnStateAndCity.Size = new System.Drawing.Size(320, 28);
            this.pnStateAndCity.TabIndex = 3;
            // 
            // leCity
            // 
            this.leCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leCity.Location = new System.Drawing.Point(163, 3);
            this.leCity.Name = "leCity";
            this.leCity.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leCity.Properties.Appearance.Options.UseFont = true;
            this.leCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCity.Properties.NullText = "";
            this.leCity.Properties.NullValuePrompt = "City";
            this.leCity.Size = new System.Drawing.Size(154, 22);
            this.leCity.TabIndex = 2;
            // 
            // leProvince
            // 
            this.leProvince.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leProvince.Location = new System.Drawing.Point(3, 3);
            this.leProvince.Name = "leProvince";
            this.leProvince.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leProvince.Properties.Appearance.Options.UseFont = true;
            this.leProvince.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leProvince.Properties.NullText = "";
            this.leProvince.Properties.NullValuePrompt = "State";
            this.leProvince.Size = new System.Drawing.Size(154, 22);
            this.leProvince.TabIndex = 1;
            // 
            // lblLocation
            // 
            this.lblLocation.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocation.Appearance.Options.UseFont = true;
            this.lblLocation.Appearance.Options.UseTextOptions = true;
            this.lblLocation.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLocation.Location = new System.Drawing.Point(3, 59);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(124, 22);
            this.lblLocation.TabIndex = 6;
            this.lblLocation.Text = "Location:";
            // 
            // pnScale
            // 
            this.pnScale.ColumnCount = 2;
            this.pnScale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnScale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnScale.Controls.Add(this.leScale, 0, 0);
            this.pnScale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnScale.Location = new System.Drawing.Point(130, 28);
            this.pnScale.Margin = new System.Windows.Forms.Padding(0);
            this.pnScale.Name = "pnScale";
            this.pnScale.RowCount = 1;
            this.pnScale.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnScale.Size = new System.Drawing.Size(320, 28);
            this.pnScale.TabIndex = 2;
            // 
            // leScale
            // 
            this.leScale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leScale.Location = new System.Drawing.Point(3, 3);
            this.leScale.Name = "leScale";
            this.leScale.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leScale.Properties.Appearance.Options.UseFont = true;
            this.leScale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leScale.Properties.NullText = "";
            this.leScale.Properties.NullValuePrompt = "Scale";
            this.leScale.Size = new System.Drawing.Size(154, 22);
            this.leScale.TabIndex = 0;
            // 
            // lblScale
            // 
            this.lblScale.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScale.Appearance.Options.UseFont = true;
            this.lblScale.Appearance.Options.UseTextOptions = true;
            this.lblScale.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblScale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblScale.Location = new System.Drawing.Point(3, 31);
            this.lblScale.Name = "lblScale";
            this.lblScale.Size = new System.Drawing.Size(124, 22);
            this.lblScale.TabIndex = 4;
            this.lblScale.Text = "Scale:";
            // 
            // pnCategory
            // 
            this.pnCategory.ColumnCount = 2;
            this.pnCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnCategory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnCategory.Controls.Add(this.leSubcategory, 0, 0);
            this.pnCategory.Controls.Add(this.leCategory, 0, 0);
            this.pnCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnCategory.Location = new System.Drawing.Point(130, 0);
            this.pnCategory.Margin = new System.Windows.Forms.Padding(0);
            this.pnCategory.Name = "pnCategory";
            this.pnCategory.RowCount = 1;
            this.pnCategory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnCategory.Size = new System.Drawing.Size(320, 28);
            this.pnCategory.TabIndex = 1;
            // 
            // leSubcategory
            // 
            this.leSubcategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leSubcategory.Location = new System.Drawing.Point(163, 3);
            this.leSubcategory.Name = "leSubcategory";
            this.leSubcategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leSubcategory.Properties.Appearance.Options.UseFont = true;
            this.leSubcategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leSubcategory.Properties.NullText = "";
            this.leSubcategory.Properties.NullValuePrompt = "Subcategory";
            this.leSubcategory.Size = new System.Drawing.Size(154, 22);
            this.leSubcategory.TabIndex = 1;
            // 
            // leCategory
            // 
            this.leCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leCategory.Location = new System.Drawing.Point(3, 3);
            this.leCategory.Name = "leCategory";
            this.leCategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leCategory.Properties.Appearance.Options.UseFont = true;
            this.leCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCategory.Properties.NullText = "";
            this.leCategory.Properties.NullValuePrompt = "Category";
            this.leCategory.Size = new System.Drawing.Size(154, 22);
            this.leCategory.TabIndex = 0;
            // 
            // lblCategory
            // 
            this.lblCategory.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Appearance.Options.UseFont = true;
            this.lblCategory.Appearance.Options.UseTextOptions = true;
            this.lblCategory.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCategory.Location = new System.Drawing.Point(3, 3);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(124, 22);
            this.lblCategory.TabIndex = 0;
            this.lblCategory.Text = "Category:";
            // 
            // meSynopsis
            // 
            this.meSynopsis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meSynopsis.Location = new System.Drawing.Point(133, 255);
            this.meSynopsis.Name = "meSynopsis";
            this.meSynopsis.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meSynopsis.Properties.Appearance.Options.UseFont = true;
            this.meSynopsis.Size = new System.Drawing.Size(314, 414);
            this.meSynopsis.TabIndex = 8;
            // 
            // pnSpatialLocatino
            // 
            this.pnSpatialLocatino.ColumnCount = 2;
            this.pnSpatialLocatino.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnSpatialLocatino.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnSpatialLocatino.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnSpatialLocatino.Controls.Add(this.teLongitude, 0, 0);
            this.pnSpatialLocatino.Controls.Add(this.teLatitude, 0, 0);
            this.pnSpatialLocatino.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnSpatialLocatino.Location = new System.Drawing.Point(130, 168);
            this.pnSpatialLocatino.Margin = new System.Windows.Forms.Padding(0);
            this.pnSpatialLocatino.Name = "pnSpatialLocatino";
            this.pnSpatialLocatino.RowCount = 1;
            this.pnSpatialLocatino.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnSpatialLocatino.Size = new System.Drawing.Size(320, 28);
            this.pnSpatialLocatino.TabIndex = 7;
            // 
            // teLongitude
            // 
            this.teLongitude.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teLongitude.EditValue = "";
            this.teLongitude.Location = new System.Drawing.Point(163, 3);
            this.teLongitude.Name = "teLongitude";
            this.teLongitude.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teLongitude.Properties.Appearance.Options.UseFont = true;
            this.teLongitude.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.teLongitude.Properties.NullValuePrompt = "Longitude";
            this.teLongitude.Size = new System.Drawing.Size(154, 22);
            this.teLongitude.TabIndex = 1;
            // 
            // teLatitude
            // 
            this.teLatitude.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.teLatitude.EditValue = "";
            this.teLatitude.Location = new System.Drawing.Point(3, 3);
            this.teLatitude.Name = "teLatitude";
            this.teLatitude.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teLatitude.Properties.Appearance.Options.UseFont = true;
            this.teLatitude.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.teLatitude.Properties.NullValuePrompt = "Latitude";
            this.teLatitude.Size = new System.Drawing.Size(154, 22);
            this.teLatitude.TabIndex = 0;
            // 
            // IncidentPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.pnContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "IncidentPanel";
            this.Size = new System.Drawing.Size(450, 700);
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            this.pnIncident.ResumeLayout(false);
            this.pnIncident.PerformLayout();
            this.pnClosedTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teClosedTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClosedDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deClosedDate.Properties)).EndInit();
            this.pnOpenedTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teOpenedTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deOpenedDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deOpenedDate.Properties)).EndInit();
            this.pnPostalCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tePostalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressLine1.Properties)).EndInit();
            this.pnStateAndCity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leProvince.Properties)).EndInit();
            this.pnScale.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leScale.Properties)).EndInit();
            this.pnCategory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leSubcategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meSynopsis.Properties)).EndInit();
            this.pnSpatialLocatino.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teLongitude.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLatitude.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnContainer;
        private System.Windows.Forms.TableLayoutPanel pnIncident;
        private DevExpress.XtraEditors.LabelControl lblOpenedTime;
        private System.Windows.Forms.TableLayoutPanel pnPostalCode;
        private DevExpress.XtraEditors.TextEdit teAddressLine2;
        private DevExpress.XtraEditors.TextEdit teAddressLine1;
        private System.Windows.Forms.TableLayoutPanel pnStateAndCity;
        private DevExpress.XtraEditors.LookUpEdit leProvince;
        private DevExpress.XtraEditors.LabelControl lblLocation;
        private System.Windows.Forms.TableLayoutPanel pnScale;
        private DevExpress.XtraEditors.LookUpEdit leScale;
        private DevExpress.XtraEditors.LabelControl lblScale;
        private System.Windows.Forms.TableLayoutPanel pnCategory;
        private DevExpress.XtraEditors.LookUpEdit leSubcategory;
        private DevExpress.XtraEditors.LookUpEdit leCategory;
        private DevExpress.XtraEditors.LabelControl lblCategory;
        private DevExpress.XtraEditors.MemoEdit meSynopsis;
        private System.Windows.Forms.TableLayoutPanel pnSpatialLocatino;
        private DevExpress.XtraEditors.TextEdit teLongitude;
        private DevExpress.XtraEditors.TextEdit teLatitude;
        private DevExpress.XtraEditors.LabelControl lblIncidentSection;
        private DevExpress.XtraEditors.TextEdit tePostalCode;
        private DevExpress.XtraEditors.LookUpEdit leCity;
        private DevExpress.XtraEditors.LabelControl lblClosedTime;
        private System.Windows.Forms.TableLayoutPanel pnOpenedTime;
        private DevExpress.XtraEditors.TextEdit teOpenedTime;
        private DevExpress.XtraEditors.DateEdit deOpenedDate;
        private System.Windows.Forms.TableLayoutPanel pnClosedTime;
        private DevExpress.XtraEditors.TextEdit teClosedTime;
        private DevExpress.XtraEditors.DateEdit deClosedDate;
        private DevExpress.XtraEditors.LabelControl lblSynopsys;
    }
}
