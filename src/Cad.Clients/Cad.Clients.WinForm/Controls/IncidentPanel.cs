﻿//
// Cad.Clients.WinForm.Controls.IncidentPanel
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Cad.Data.BLL.Dispatch;
using Cad.Data.BLL.Person;

namespace Cad.Clients.WinForm.Controls
{
    public partial class IncidentPanel : UserControl
    {
        #region PROPERTIES
        public Incident CurrentIncident { get; set; } = null;
        #endregion

        #region CONSTRUCTORS
        public IncidentPanel()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region PRIVATE METHODS
        private void InitialzieLookUpEdit(LookUpEdit edit,
            string displayMemberName,
            string valueMemberName,
            int rows = 5,
            bool showHeader = false, 
            bool showFooter = false)
        {
            edit.Properties.DropDownRows = rows;
            edit.Properties.PopupSizeable = false;
            edit.Properties.ShowHeader = showHeader;
            edit.Properties.ShowFooter = showFooter;
            edit.Properties.DisplayMember = displayMemberName;
            edit.Properties.ValueMember = valueMemberName;
            edit.Properties.Columns.Add(new LookUpColumnInfo("Name"));
            
            switch (edit.Name)
            {
                case "leCategory":
                    leCategory.EditValueChanged += Category_EditValueChanged;
                    break;
                case "leSubcategory":
                    edit.Properties.Columns.Add(new LookUpColumnInfo("Description"));
                    edit.Properties.PopupWidth = 400;
                    edit.Properties.Columns[0].Width = 100;
                    edit.Properties.Columns[1].Width = 300;
                    break;
                case "leScale":
                    edit.Properties.Columns.Add(new LookUpColumnInfo("Description"));
                    edit.Properties.PopupWidth = 620;
                    edit.Properties.Columns[0].Width = 80;
                    edit.Properties.Columns[1].Width = 540;
                    break;
                case "leProvince":
                    leProvince.EditValueChanged += Province_EditValueChanged;
                    break;
                default: break;
            }
        }

        private void InitializeControls()
        {
            InitialzieLookUpEdit(leCategory, "Name", "Id", 4);
            InitialzieLookUpEdit(leSubcategory, "Name", "Id", 5, true);
            InitialzieLookUpEdit(leScale, "Name", "Id", 3, true);

            InitialzieLookUpEdit(leProvince, "Name", "StateProvinceCode");
            InitialzieLookUpEdit(leCity, "Name", "CityCode");
        }
        #endregion

        #region PUBLIC METHOD
        public void BindData()
        {
            List<IncidentCategory> categories = 
                IncidentCategory.GetIncidentCategories();

            if (categories != null && categories.Count > 0)
                leCategory.Properties.DataSource = categories;

            List<IncidentScale> scales = IncidentScale.GetIncidentScales();
            if (scales != null && scales.Count > 0)
                leScale.Properties.DataSource = scales;

            List<StateProvince> provinces = StateProvince.GetStateProvinces("BD");
            if (provinces != null && provinces.Count > 0)
                leProvince.Properties.DataSource = provinces;

            if (CurrentIncident == null) return;
            leCategory.EditValue = CurrentIncident.Category.Id;
            // leSubcategory => binded at Category_EditValueChanged event.
            leScale.EditValue = CurrentIncident.Scale.Id;
            leProvince.EditValue = CurrentIncident.Location.City.StateProvince.StateProvinceCode;
            leCity.EditValue = CurrentIncident.Location.City.CityCode;
            teAddressLine1.Text = CurrentIncident.Location.AddressLine1;
            teAddressLine2.Text = CurrentIncident.Location.AddressLine2;
            tePostalCode.Text = CurrentIncident.Location.PostalCode;
            teLatitude.Text = CurrentIncident.Location.SpatialLocation.Latitude.ToString();
            teLongitude.Text = CurrentIncident.Location.SpatialLocation.Longitude.ToString();
            deOpenedDate.DateTime = CurrentIncident.OpenedDate;
            teOpenedTime.Text = CurrentIncident.OpenedDate.ToString(Resources.TimeHrMinFormat);
            if (CurrentIncident.ClosedDate != DateTime.MinValue)
            {
                deClosedDate.DateTime = CurrentIncident.ClosedDate;
                teClosedTime.Text = CurrentIncident.ClosedDate.ToString(Resources.TimeHrMinFormat);
            }
            meSynopsis.Text = CurrentIncident.Synopsys;
        }
        #endregion

        #region CONTROL EVENTS
        private void Province_EditValueChanged(object sender, EventArgs e)
        {
            if (leProvince.EditValue == null) return;

            List<City> cities = City.GetCities(leProvince.EditValue.ToString());
            if (cities != null && cities.Count > 0)
                leCity.Properties.DataSource = cities;
        }

        private void Category_EditValueChanged(object sender, System.EventArgs e)
        {
            if (leCategory.EditValue == null) return;

            List<IncidentSubcategory> categories = IncidentSubcategory
                .GetIncidentSubcategories((short)leCategory.EditValue);
            if (categories != null && categories.Count > 0)
            {
                leSubcategory.Properties.DataSource = categories;
                if (CurrentIncident != null)
                    leSubcategory.EditValue = CurrentIncident.Subcategory.Id;
            }
        }
        #endregion
    }
}
