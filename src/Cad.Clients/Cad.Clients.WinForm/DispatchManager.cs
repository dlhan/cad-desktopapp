﻿//
// Cad.Clients.WinForm.DispatchManager
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Equipments.Cti;
//using Cad.Data.BLL.Dispatch;

namespace Cad.Clients.WinForm
{
    internal static class DispatchManager
    {
        #region VARIABLES
        private static bool _initialized = false;
        private static Exception _initializedException = null;
        private static object _lock = new object();
        #endregion

        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        #endregion

        #region PRIVATE METHODS
        private static void Initialize()
        {
            if (_initialized) return;
            if (_initializedException != null) throw _initializedException;

            lock (_lock)
            {
                if (_initialized) return;
                if (_initializedException != null) throw _initializedException;

                try
                {
                }
                catch (Exception e)
                {
                    _initializedException = e;
                    throw;
                }

                _initialized = true;
            }
        }
        #endregion

        #region PUBLIC METHODS
        public static void EnableCti(bool enable)
        {
            Telephony.ExecuteAgentFeature(
                enable ? AgentFeatures.Enable : AgentFeatures.Disable);
        }

        public static AgentState GetCtiState()
        {
            return Telephony.AgentState;
        }
        #endregion
    }
}
