﻿
namespace Cad.Clients.WinForm.Forms
{
    partial class EditDispatchNoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContainer = new System.Windows.Forms.TableLayoutPanel();
            this.lblDispatcherVal = new DevExpress.XtraEditors.LabelControl();
            this.lblCreatedDateVal = new DevExpress.XtraEditors.LabelControl();
            this.lblCreatedDate = new DevExpress.XtraEditors.LabelControl();
            this.lblDispatcher = new DevExpress.XtraEditors.LabelControl();
            this.lblNote = new DevExpress.XtraEditors.LabelControl();
            this.pnCommand = new System.Windows.Forms.TableLayoutPanel();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.lblNoteType = new DevExpress.XtraEditors.LabelControl();
            this.lblSeperator = new DevExpress.XtraEditors.LabelControl();
            this.meNote = new DevExpress.XtraEditors.MemoEdit();
            this.leNoteType = new DevExpress.XtraEditors.LookUpEdit();
            this.pnContainer.SuspendLayout();
            this.pnCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leNoteType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            this.pnContainer.ColumnCount = 2;
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.pnContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.Controls.Add(this.lblDispatcherVal, 1, 1);
            this.pnContainer.Controls.Add(this.lblCreatedDateVal, 1, 3);
            this.pnContainer.Controls.Add(this.lblCreatedDate, 0, 3);
            this.pnContainer.Controls.Add(this.lblDispatcher, 0, 1);
            this.pnContainer.Controls.Add(this.lblNote, 0, 2);
            this.pnContainer.Controls.Add(this.pnCommand, 1, 5);
            this.pnContainer.Controls.Add(this.lblNoteType, 0, 0);
            this.pnContainer.Controls.Add(this.lblSeperator, 0, 4);
            this.pnContainer.Controls.Add(this.meNote, 1, 2);
            this.pnContainer.Controls.Add(this.leNoteType, 1, 0);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(10, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.RowCount = 6;
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.pnContainer.Size = new System.Drawing.Size(564, 256);
            this.pnContainer.TabIndex = 0;
            // 
            // lblDispatcherVal
            // 
            this.lblDispatcherVal.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispatcherVal.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(200)))), ((int)(((byte)(170)))));
            this.lblDispatcherVal.Appearance.Options.UseFont = true;
            this.lblDispatcherVal.Appearance.Options.UseForeColor = true;
            this.lblDispatcherVal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDispatcherVal.Location = new System.Drawing.Point(133, 31);
            this.lblDispatcherVal.Name = "lblDispatcherVal";
            this.lblDispatcherVal.Size = new System.Drawing.Size(428, 22);
            this.lblDispatcherVal.TabIndex = 8;
            // 
            // lblCreatedDateVal
            // 
            this.lblCreatedDateVal.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreatedDateVal.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(200)))), ((int)(((byte)(170)))));
            this.lblCreatedDateVal.Appearance.Options.UseFont = true;
            this.lblCreatedDateVal.Appearance.Options.UseForeColor = true;
            this.lblCreatedDateVal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDateVal.Location = new System.Drawing.Point(133, 173);
            this.lblCreatedDateVal.Name = "lblCreatedDateVal";
            this.lblCreatedDateVal.Size = new System.Drawing.Size(428, 22);
            this.lblCreatedDateVal.TabIndex = 7;
            this.lblCreatedDateVal.Text = "--:--";
            // 
            // lblCreatedDate
            // 
            this.lblCreatedDate.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreatedDate.Appearance.Options.UseFont = true;
            this.lblCreatedDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreatedDate.Location = new System.Drawing.Point(3, 173);
            this.lblCreatedDate.Name = "lblCreatedDate";
            this.lblCreatedDate.Size = new System.Drawing.Size(124, 22);
            this.lblCreatedDate.TabIndex = 5;
            this.lblCreatedDate.Text = "Created Date:";
            // 
            // lblDispatcher
            // 
            this.lblDispatcher.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispatcher.Appearance.Options.UseFont = true;
            this.lblDispatcher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDispatcher.Location = new System.Drawing.Point(3, 31);
            this.lblDispatcher.Name = "lblDispatcher";
            this.lblDispatcher.Size = new System.Drawing.Size(124, 22);
            this.lblDispatcher.TabIndex = 4;
            this.lblDispatcher.Text = "Dispatcher:";
            // 
            // lblNote
            // 
            this.lblNote.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNote.Appearance.Options.UseFont = true;
            this.lblNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNote.Location = new System.Drawing.Point(3, 59);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(124, 108);
            this.lblNote.TabIndex = 2;
            this.lblNote.Text = "Note:";
            // 
            // pnCommand
            // 
            this.pnCommand.ColumnCount = 3;
            this.pnCommand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnCommand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.pnCommand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.pnCommand.Controls.Add(this.btnOk, 1, 0);
            this.pnCommand.Controls.Add(this.btnCancel, 2, 0);
            this.pnCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnCommand.Location = new System.Drawing.Point(130, 221);
            this.pnCommand.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pnCommand.Name = "pnCommand";
            this.pnCommand.RowCount = 1;
            this.pnCommand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnCommand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.pnCommand.Size = new System.Drawing.Size(434, 32);
            this.pnCommand.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Appearance.Options.UseFont = true;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOk.Location = new System.Drawing.Point(237, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(94, 26);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Location = new System.Drawing.Point(337, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 26);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "&Cancel";
            // 
            // lblNoteType
            // 
            this.lblNoteType.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoteType.Appearance.Options.UseFont = true;
            this.lblNoteType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoteType.Location = new System.Drawing.Point(3, 3);
            this.lblNoteType.Name = "lblNoteType";
            this.lblNoteType.Size = new System.Drawing.Size(124, 22);
            this.lblNoteType.TabIndex = 1;
            this.lblNoteType.Text = "Type:";
            // 
            // lblSeperator
            // 
            this.pnContainer.SetColumnSpan(this.lblSeperator, 2);
            this.lblSeperator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSeperator.LineVisible = true;
            this.lblSeperator.Location = new System.Drawing.Point(3, 201);
            this.lblSeperator.Name = "lblSeperator";
            this.lblSeperator.Size = new System.Drawing.Size(558, 14);
            this.lblSeperator.TabIndex = 3;
            // 
            // meNote
            // 
            this.meNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meNote.Location = new System.Drawing.Point(133, 59);
            this.meNote.Name = "meNote";
            this.meNote.Size = new System.Drawing.Size(428, 108);
            this.meNote.TabIndex = 6;
            // 
            // leNoteType
            // 
            this.leNoteType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leNoteType.Location = new System.Drawing.Point(133, 3);
            this.leNoteType.Name = "leNoteType";
            this.leNoteType.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leNoteType.Properties.Appearance.Options.UseFont = true;
            this.leNoteType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leNoteType.Properties.NullValuePrompt = "Select Note Type";
            this.leNoteType.Size = new System.Drawing.Size(428, 22);
            this.leNoteType.TabIndex = 9;
            this.leNoteType.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.leNoteType_QueryPopUp);
            // 
            // DispatchNoteForm
            // 
            this.AcceptButton = this.btnOk;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(584, 261);
            this.Controls.Add(this.pnContainer);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.ShowIcon = false;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DispatchNoteForm";
            this.Padding = new System.Windows.Forms.Padding(10, 0, 10, 5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dispatch Note";
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            this.pnCommand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leNoteType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnContainer;
        private System.Windows.Forms.TableLayoutPanel pnCommand;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.LabelControl lblNoteType;
        private DevExpress.XtraEditors.LabelControl lblNote;
        private DevExpress.XtraEditors.LabelControl lblSeperator;
        private DevExpress.XtraEditors.LabelControl lblCreatedDate;
        private DevExpress.XtraEditors.LabelControl lblDispatcher;
        private DevExpress.XtraEditors.MemoEdit meNote;
        private DevExpress.XtraEditors.LabelControl lblDispatcherVal;
        private DevExpress.XtraEditors.LabelControl lblCreatedDateVal;
        private DevExpress.XtraEditors.LookUpEdit leNoteType;
    }
}