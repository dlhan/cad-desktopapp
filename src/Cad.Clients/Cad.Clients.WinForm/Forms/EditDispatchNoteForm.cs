﻿//
// Cad.Clients.WinForm.Forms.DispatchNoteForm
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using DevExpress.XtraEditors;
//using Cad.Data.BLL.Dispatch;

namespace Cad.Clients.WinForm.Forms
{
    public partial class EditDispatchNoteForm : XtraForm
    {
        #region PROPERTIES
        //private DispatchNote dispatchNote = null;
        //public DispatchNote DispatchNote
        //{
        //    get { return dispatchNote; }
        //    set
        //    {
        //        if (value != null)
        //        {
        //            dispatchNote = value;
        //            leNoteType.EditValue = dispatchNote.Type.DispatchNoteTypeId;
        //            lblDispatcherVal.Text = dispatchNote.Dispatcher;
        //            meNote.Text = dispatchNote.Note;
        //            lblCreatedDateVal.Text = dispatchNote.CreatedDate
        //                .ToString(Resources.DateTimeTemplate);
        //        }
        //    }
        //}
        #endregion

        #region CONSTRUCTORS
        public EditDispatchNoteForm()
        {
            InitializeComponent();

            //this.Text = $"{AppContext.AppName} - Dispatch Note";
            //leNoteType.Properties.DisplayMember = "Name";
            //leNoteType.Properties.ValueMember = "DispatchNoteTypeId";
            //leNoteType.Properties.DataSource = DispatchNoteType.GetDispatchNoteTypes();
            //leNoteType.Properties.ShowHeader = false;
            //leNoteType.Properties.ShowLines = false;
        }
        #endregion

        #region PRIVATE METHODS
        #endregion

        #region EVENTS
        private void leNoteType_QueryPopUp(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //leNoteType.Properties.PopulateColumns();
            //leNoteType.Properties.Columns["DispatchNoteTypeId"].Visible = false;
            //leNoteType.Properties.Columns["Name"].Visible = true;
            leNoteType.Properties.Columns["CreatedDate"].Visible = false;
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            //if (dispatchNote == null) dispatchNote = new DispatchNote();
            //dispatchNote.Type = DispatchNoteType
            //    .GetDispatchNoteType((int)leNoteType.EditValue);
            //dispatchNote.Note = meNote.Text;
        }
        #endregion
    }
}
