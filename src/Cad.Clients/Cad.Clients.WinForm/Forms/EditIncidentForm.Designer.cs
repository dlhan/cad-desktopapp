﻿
namespace Cad.Clients.WinForm.Forms
{
    partial class EditIncidentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditIncidentForm));
            this.rcCase = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.rpHome = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgSave = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.ponContainer = new System.Windows.Forms.TableLayoutPanel();
            this.ucCallPanel = new Cad.Clients.WinForm.Controls.CallPanel();
            this.ucIncidentPanel = new Cad.Clients.WinForm.Controls.IncidentPanel();
            this.ucUnitPanel = new Cad.Clients.WinForm.Controls.DispatchUnitPanel();
            ((System.ComponentModel.ISupportInitialize)(this.rcCase)).BeginInit();
            this.ponContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // rcCase
            // 
            this.rcCase.EmptyAreaImageOptions.ImagePadding = new System.Windows.Forms.Padding(30, 32, 30, 32);
            this.rcCase.ExpandCollapseItem.Id = 0;
            this.rcCase.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.rcCase.ExpandCollapseItem,
            this.rcCase.SearchEditItem,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiCancel});
            this.rcCase.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.rcCase.Location = new System.Drawing.Point(0, 0);
            this.rcCase.MaxItemId = 5;
            this.rcCase.Name = "rcCase";
            this.rcCase.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpHome});
            this.rcCase.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.rcCase.Size = new System.Drawing.Size(1598, 160);
            this.rcCase.StatusBar = this.ribbonStatusBar;
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Save && Close";
            this.bbiSaveAndClose.Id = 1;
            this.bbiSaveAndClose.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.floppy_disk_delete_16x16;
            this.bbiSaveAndClose.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.floppy_disk_delete_32x32;
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            this.bbiSaveAndClose.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Save && New";
            this.bbiSaveAndNew.Id = 2;
            this.bbiSaveAndNew.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.floppy_disk_add_16x16;
            this.bbiSaveAndNew.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.floppy_disk_add_32x32;
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            this.bbiSaveAndNew.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText)));
            // 
            // bbiCancel
            // 
            this.bbiCancel.Caption = "Cancel";
            this.bbiCancel.Id = 3;
            this.bbiCancel.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.exit_16x16;
            this.bbiCancel.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.exit_32x32;
            this.bbiCancel.Name = "bbiCancel";
            // 
            // rpHome
            // 
            this.rpHome.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgSave});
            this.rpHome.Name = "rpHome";
            this.rpHome.Text = "HOME";
            // 
            // rpgSave
            // 
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndClose, true);
            this.rpgSave.ItemLinks.Add(this.bbiSaveAndNew);
            this.rpgSave.ItemLinks.Add(this.bbiCancel, true);
            this.rpgSave.Name = "rpgSave";
            this.rpgSave.Text = "Save";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 575);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.rcCase;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1598, 24);
            // 
            // ponContainer
            // 
            this.ponContainer.ColumnCount = 6;
            this.ponContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 7F));
            this.ponContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 460F));
            this.ponContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 7F));
            this.ponContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 460F));
            this.ponContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 7F));
            this.ponContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ponContainer.Controls.Add(this.ucCallPanel, 1, 0);
            this.ponContainer.Controls.Add(this.ucIncidentPanel, 3, 0);
            this.ponContainer.Controls.Add(this.ucUnitPanel, 5, 0);
            this.ponContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ponContainer.Location = new System.Drawing.Point(0, 160);
            this.ponContainer.Name = "ponContainer";
            this.ponContainer.RowCount = 1;
            this.ponContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ponContainer.Size = new System.Drawing.Size(1598, 415);
            this.ponContainer.TabIndex = 2;
            // 
            // ucCallPanel
            // 
            this.ucCallPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ucCallPanel.Call = null;
            this.ucCallPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucCallPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucCallPanel.Location = new System.Drawing.Point(10, 3);
            this.ucCallPanel.Name = "ucCallPanel";
            this.ucCallPanel.Size = new System.Drawing.Size(454, 409);
            this.ucCallPanel.TabIndex = 0;
            // 
            // ucIncidentPanel
            // 
            this.ucIncidentPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ucIncidentPanel.CurrentIncident = null;
            this.ucIncidentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucIncidentPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucIncidentPanel.Location = new System.Drawing.Point(477, 3);
            this.ucIncidentPanel.Name = "ucIncidentPanel";
            this.ucIncidentPanel.Size = new System.Drawing.Size(454, 409);
            this.ucIncidentPanel.TabIndex = 1;
            // 
            // ucUnitPanel
            // 
            this.ucUnitPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ucUnitPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucUnitPanel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucUnitPanel.Location = new System.Drawing.Point(944, 3);
            this.ucUnitPanel.Name = "ucUnitPanel";
            this.ucUnitPanel.Size = new System.Drawing.Size(651, 409);
            this.ucUnitPanel.Squads = null;
            this.ucUnitPanel.TabIndex = 2;
            // 
            // EditIncidentForm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1598, 599);
            this.Controls.Add(this.ponContainer);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.rcCase);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("EditIncidentForm.IconOptions.Icon")));
            this.Name = "EditIncidentForm";
            this.Ribbon = this.rcCase;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "IncidentForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditIncidentForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.rcCase)).EndInit();
            this.ponContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl rcCase;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpHome;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSave;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private System.Windows.Forms.TableLayoutPanel ponContainer;
        private Controls.CallPanel ucCallPanel;
        private Controls.IncidentPanel ucIncidentPanel;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private Controls.DispatchUnitPanel ucUnitPanel;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
    }
}