﻿//
// Cad.Clients.WinForm.Forms.EditCaseForm
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using Cad.Data.BLL.Dispatch;
using Cad.Clients.WinForm.Modules;

namespace Cad.Clients.WinForm.Forms
{
    public partial class EditIncidentForm : RibbonForm
    {
        #region VARIABLES
        private IncidentModule parentModule;
        #endregion

        #region PROPERTIES
        private Incident incident = null;
        public Incident Incident
        {
            get { return incident; }
            set
            {
                incident = value;
                SetCaption();
            }
        }
        #endregion

        #region CONSTRUCTORS
        public EditIncidentForm(IncidentModule parentModule)
        {
            this.parentModule = parentModule;
            InitializeComponent();
            Initialize();
        }
        #endregion

        #region PRIVATE METHODS
        private void InitializeRibbonButton(BarButtonItem item, object tag, string hint)
        {
            item.ItemClick += new ItemClickEventHandler(BarButton_ItemClick);
            item.Hint = hint;
            item.Tag = tag;
        }

        private void InitializeRibbonControls()
        {
            InitializeRibbonButton(bbiSaveAndClose,
                Resources.SaveAndCloseTag,
                string.Empty);
            InitializeRibbonButton(bbiSaveAndNew,
                Resources.SaveAndNewTag,
                string.Empty);
            InitializeRibbonButton(bbiCancel,
                Resources.CancelTag,
                string.Empty);
        }

        private void Initialize()
        {
            InitializeRibbonControls();
        }

        private void SetCaption()
        {
            this.Text = incident is null
                ? "Add New Incident" 
                : string.Format("{0} / {1} / {2}",
                    incident.Category.Name,
                    incident.Subcategory.Name,
                    incident.Scale.Name);
        }
        #endregion

        #region PUBLIC METHODS
        public void BindData()
        {
            if (incident != null)
            {
                ucCallPanel.Call = incident.Call;
                ucIncidentPanel.CurrentIncident = incident;
                ucUnitPanel.Squads = incident.Squads;
            }
            ucCallPanel.BindData();
            ucIncidentPanel.BindData();
            ucUnitPanel.BindData();
        }
        #endregion

        #region FORM EVENTS
        private void EditIncidentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            parentModule.BindData();
        }
        #endregion

        #region CONTROL EVENTS
        private void BarButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (((BarButtonItem)e.Item).Name)
            {
                case "bbiCancel":
                    this.Close();
                    break;
                case "bbiSaveAndClose":
                    System.Console.WriteLine("Save & Close clicked.");
                    break;
                case "bbiSaveAndNew":
                    System.Console.WriteLine("Save & New clicked.");
                    break;
                default: break;
            }
        }
        #endregion
    }
}