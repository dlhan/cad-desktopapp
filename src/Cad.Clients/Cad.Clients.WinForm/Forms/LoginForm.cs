﻿//
// Cad.Clients.WinForm.Forms.LoginForm
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using DevExpress.XtraEditors;

namespace Cad.Clients.WinForm.Forms
{
    public partial class LoginForm : XtraForm
    {
        #region CONSTRUCTORS
        public LoginForm()
        {
            InitializeComponent();
            Initialize();
        }
        #endregion

        #region METHODS
        private void Initialize()
        {
            this.Text = "Login";
        }
        #endregion
    }
}
