﻿
namespace Cad.Clients.WinForm.Forms
{
    partial class RecordingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecordingForm));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiPlay = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPause = new DevExpress.XtraBars.BarButtonItem();
            this.bbiStop = new DevExpress.XtraBars.BarButtonItem();
            this.rpRecord = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgPlayback = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.pnRoot = new System.Windows.Forms.TableLayoutPanel();
            this.lblRecordedDate = new DevExpress.XtraEditors.LabelControl();
            this.lblCurrentPosition = new DevExpress.XtraEditors.LabelControl();
            this.lblRecordId = new DevExpress.XtraEditors.LabelControl();
            this.picSoundWave = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            this.pnRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSoundWave.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.bbiPlay,
            this.bbiPause,
            this.bbiStop});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 4;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpRecord});
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(598, 160);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // bbiPlay
            // 
            this.bbiPlay.Caption = "Play";
            this.bbiPlay.Id = 1;
            this.bbiPlay.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.cd_run_16x16;
            this.bbiPlay.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.cd_run_32x32;
            this.bbiPlay.Name = "bbiPlay";
            // 
            // bbiPause
            // 
            this.bbiPause.Caption = "Pause";
            this.bbiPause.Id = 2;
            this.bbiPause.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.cd_pause_16x16;
            this.bbiPause.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.cd_pause_32x32;
            this.bbiPause.Name = "bbiPause";
            // 
            // bbiStop
            // 
            this.bbiStop.Caption = "Stop";
            this.bbiStop.Id = 3;
            this.bbiStop.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.cd_stop_16x16;
            this.bbiStop.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.cd_stop_32x32;
            this.bbiStop.Name = "bbiStop";
            // 
            // rpRecord
            // 
            this.rpRecord.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgPlayback});
            this.rpRecord.Name = "rpRecord";
            this.rpRecord.Text = "Record";
            // 
            // rpgPlayback
            // 
            this.rpgPlayback.ItemLinks.Add(this.bbiPlay);
            this.rpgPlayback.ItemLinks.Add(this.bbiPause);
            this.rpgPlayback.ItemLinks.Add(this.bbiStop);
            this.rpgPlayback.Name = "rpgPlayback";
            this.rpgPlayback.Text = "Playback";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 475);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(598, 24);
            // 
            // pnRoot
            // 
            this.pnRoot.ColumnCount = 3;
            this.pnRoot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnRoot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnRoot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnRoot.Controls.Add(this.lblRecordedDate, 1, 4);
            this.pnRoot.Controls.Add(this.lblCurrentPosition, 1, 3);
            this.pnRoot.Controls.Add(this.lblRecordId, 1, 1);
            this.pnRoot.Controls.Add(this.picSoundWave, 1, 2);
            this.pnRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRoot.Location = new System.Drawing.Point(0, 160);
            this.pnRoot.Name = "pnRoot";
            this.pnRoot.RowCount = 6;
            this.pnRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.pnRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.pnRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.pnRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.pnRoot.Size = new System.Drawing.Size(598, 315);
            this.pnRoot.TabIndex = 2;
            // 
            // lblRecordedDate
            // 
            this.lblRecordedDate.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecordedDate.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(187)))), ((int)(((byte)(151)))));
            this.lblRecordedDate.Appearance.Options.UseFont = true;
            this.lblRecordedDate.Appearance.Options.UseForeColor = true;
            this.lblRecordedDate.Appearance.Options.UseTextOptions = true;
            this.lblRecordedDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblRecordedDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRecordedDate.Location = new System.Drawing.Point(23, 260);
            this.lblRecordedDate.Name = "lblRecordedDate";
            this.lblRecordedDate.Size = new System.Drawing.Size(552, 22);
            this.lblRecordedDate.TabIndex = 3;
            this.lblRecordedDate.Text = "Recorded at 01/07/2021";
            // 
            // lblCurrentPosition
            // 
            this.lblCurrentPosition.Appearance.Font = new System.Drawing.Font("Segoe UI", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentPosition.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(168)))), ((int)(((byte)(123)))));
            this.lblCurrentPosition.Appearance.Options.UseFont = true;
            this.lblCurrentPosition.Appearance.Options.UseForeColor = true;
            this.lblCurrentPosition.Appearance.Options.UseTextOptions = true;
            this.lblCurrentPosition.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblCurrentPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCurrentPosition.Location = new System.Drawing.Point(23, 210);
            this.lblCurrentPosition.Name = "lblCurrentPosition";
            this.lblCurrentPosition.Size = new System.Drawing.Size(552, 44);
            this.lblCurrentPosition.TabIndex = 2;
            this.lblCurrentPosition.Text = "13:12";
            // 
            // lblRecordId
            // 
            this.lblRecordId.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecordId.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(187)))), ((int)(((byte)(151)))));
            this.lblRecordId.Appearance.Options.UseFont = true;
            this.lblRecordId.Appearance.Options.UseForeColor = true;
            this.lblRecordId.Appearance.Options.UseTextOptions = true;
            this.lblRecordId.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblRecordId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRecordId.Location = new System.Drawing.Point(23, 33);
            this.lblRecordId.Name = "lblRecordId";
            this.lblRecordId.Size = new System.Drawing.Size(552, 22);
            this.lblRecordId.TabIndex = 0;
            this.lblRecordId.Text = "REC-2K210701-001";
            // 
            // picSoundWave
            // 
            this.picSoundWave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picSoundWave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSoundWave.Location = new System.Drawing.Point(220, 88);
            this.picSoundWave.Margin = new System.Windows.Forms.Padding(200, 30, 200, 30);
            this.picSoundWave.Name = "picSoundWave";
            this.picSoundWave.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picSoundWave.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.picSoundWave.Properties.Appearance.Options.UseBackColor = true;
            this.picSoundWave.Properties.Appearance.Options.UseFont = true;
            this.picSoundWave.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picSoundWave.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picSoundWave.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picSoundWave.Size = new System.Drawing.Size(158, 89);
            this.picSoundWave.TabIndex = 1;
            // 
            // RecordingForm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 499);
            this.Controls.Add(this.pnRoot);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("RecordingForm.IconOptions.Icon")));
            this.Name = "RecordingForm";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Recording";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            this.pnRoot.ResumeLayout(false);
            this.pnRoot.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSoundWave.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpRecord;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgPlayback;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem bbiPlay;
        private DevExpress.XtraBars.BarButtonItem bbiPause;
        private DevExpress.XtraBars.BarButtonItem bbiStop;
        private System.Windows.Forms.TableLayoutPanel pnRoot;
        private DevExpress.XtraEditors.LabelControl lblRecordId;
        private DevExpress.XtraEditors.PictureEdit picSoundWave;
        private DevExpress.XtraEditors.LabelControl lblCurrentPosition;
        private DevExpress.XtraEditors.LabelControl lblRecordedDate;
    }
}