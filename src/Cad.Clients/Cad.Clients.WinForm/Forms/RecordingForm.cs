﻿//
// Cad.Clients.WinForm.Forms.RecordingForm
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using DevExpress.Utils.Svg;
using DevExpress.XtraBars.Ribbon;

namespace Cad.Clients.WinForm.Forms
{
    public partial class RecordingForm : RibbonForm
    {
        #region CONSTRUCTORS
        public RecordingForm()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region PRIVATE METHODS
        private void InitializeControls()
        {
            var svg = SvgBitmap.FromFile(@"Resources\sound_wave.svg");
            picSoundWave.Image = svg.Render(null, 0.8);
        }
        #endregion
    }
}