﻿//
// Cad.Clients.WinForm.Forms.SplashScreenForm
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace Cad.Clients.WinForm.Forms
{
    public partial class SplashScreenForm : SplashScreen
    {
        #region VARIABLES
        #endregion

        #region CONSTRUCTORS
        public SplashScreenForm()
        {
            InitializeComponent();
        }
        #endregion

        #region METHODS

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        #region EVENTS
        #endregion

        public enum SplashScreenCommand
        {
        }
    }
}