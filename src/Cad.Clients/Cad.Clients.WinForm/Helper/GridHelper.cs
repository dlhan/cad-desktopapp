﻿//
// Cad.Clients.WinForm.Helper.GridHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;

namespace Cad.Clients.WinForm.Helper
{
    public class GridColumnProperty
    {
        public string FieldName { get; set; }
        public FormatType DisplayFormatType { get; set; }
        public string DisplayFormatString { get; set; }
        public RepositoryItem ColumnEdit { get; set; }

        #region CONSTRUCTORS
        public GridColumnProperty() { }

        public GridColumnProperty(string fieldName, 
            FormatType dispalyFormatType, string displayFormatString,
            RepositoryItem columnEdit = null)
        {
            FieldName = fieldName;
            DisplayFormatType = dispalyFormatType;
            DisplayFormatString = displayFormatString;
            ColumnEdit = columnEdit;
        }
        #endregion
    }

    public class GridHelper
    {
        public static void SetColumn(GridColumn column, 
            GridColumnProperty prop)
        {
            column.FieldName = prop.FieldName;
            column.DisplayFormat.FormatType = prop.DisplayFormatType;
            column.DisplayFormat.FormatString = prop.DisplayFormatString;
            column.OptionsColumn.AllowEdit = (prop.ColumnEdit != null);
            column.ColumnEdit = prop.ColumnEdit;
        }
    }
}
