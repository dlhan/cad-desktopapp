﻿// Cad.Clients.WinForm.Helper.ImageHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using DevExpress.Utils;

namespace Cad.Clients.WinForm.Helper
{
    public class ImageHelper
    {
        public static ImageCollection CreateIncidentCategoryImages()
        {
            ImageCollection images = new ImageCollection();
            images.AddImage(Properties.Resources.flag_red_16x16);
            images.AddImage(Properties.Resources.flag_blue_16x16);
            images.AddImage(Properties.Resources.flag_green_16x16);
            images.AddImage(Properties.Resources.flag_yellow_16x16);
            return images;
        }
    }
}
