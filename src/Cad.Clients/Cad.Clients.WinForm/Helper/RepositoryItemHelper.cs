﻿using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using Cad.Data.BLL.Dispatch;

namespace Cad.Clients.WinForm.Helper
{
    public class RepositoryItemHelper
    {
        public static RepositoryItemTextEdit CreateTimeTextEdit(string name)
        {
            RepositoryItemTextEdit result = new RepositoryItemTextEdit();
            result.Name = name;
            result.EditFormat.FormatType = FormatType.Custom;
            result.EditFormat.FormatString = Resources.DateTimeFullFormat;

            return result;
        }

        public static RepositoryItemImageComboBox 
            CreateIncidentCategoryImageComboBox(string name)
        {
            RepositoryItemImageComboBox result = new RepositoryItemImageComboBox();
            foreach (IncidentCategory record in IncidentCategory.GetIncidentCategories())
            {
                result.Items.Add(new ImageComboBoxItem()
                {
                    Description = record.Name,
                    Value = (short)record.Id,
                    ImageIndex = record.ImageIndex
                });
            }
            result.SmallImages = ImageHelper.CreateIncidentCategoryImages();
            result.Name = name;

            return result;
        }
    }
}
