﻿
namespace Cad.Clients.WinForm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem3 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.rcMain = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bsiNavigation = new DevExpress.XtraBars.BarSubItem();
            this.bbiAnswer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiHangup = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTransfer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiConference = new DevExpress.XtraBars.BarButtonItem();
            this.bbiHold = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDialup = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEnable = new DevExpress.XtraBars.BarButtonItem();
            this.bsiConsoleInfo = new DevExpress.XtraBars.BarStaticItem();
            this.bbiRequestAli = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCallDisposition = new DevExpress.XtraBars.BarButtonItem();
            this.mnuCallDisposition = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAnnounce = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNotResponse = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPrank = new DevExpress.XtraBars.BarButtonItem();
            this.bbiWrongNumber = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTestCall = new DevExpress.XtraBars.BarButtonItem();
            this.bbiIncidentCall = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBuildSquad = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNewNote = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditNote = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteNote = new DevExpress.XtraBars.BarButtonItem();
            this.bbiListeningRecord = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditIncident = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFireFlag = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRescueFlag = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEmergencyFlag = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMiscFlag = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiCurrentViewIncidents = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.bbiFieldSupport = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDispatchUnit = new DevExpress.XtraBars.BarButtonItem();
            this.bsiInfo = new DevExpress.XtraBars.BarStaticItem();
            this.bsiTemp = new DevExpress.XtraBars.BarStaticItem();
            this.rpDispatch = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgCall = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgDispatch = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rptDispatchNote = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgRecording = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpIncident = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgIncidents = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCategory = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCurrentView = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgAction = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpView = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rsbMain = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.nbcMain = new DevExpress.XtraNavBar.NavBarControl();
            this.nbgModules = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiDispatch = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiIncident = new DevExpress.XtraNavBar.NavBarItem();
            this.pcMain = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.rcMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuCallDisposition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbcMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcMain)).BeginInit();
            this.SuspendLayout();
            // 
            // rcMain
            // 
            this.rcMain.ExpandCollapseItem.Id = 0;
            this.rcMain.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.rcMain.ExpandCollapseItem,
            this.rcMain.SearchEditItem,
            this.bsiNavigation,
            this.bbiAnswer,
            this.bbiHangup,
            this.bbiTransfer,
            this.bbiConference,
            this.bbiHold,
            this.bbiDialup,
            this.bbiEnable,
            this.bsiConsoleInfo,
            this.bbiRequestAli,
            this.bbiCallDisposition,
            this.bbiAnnounce,
            this.bbiNotResponse,
            this.bbiPrank,
            this.bbiWrongNumber,
            this.bbiTestCall,
            this.bbiIncidentCall,
            this.bbiBuildSquad,
            this.bbiNewNote,
            this.bbiEditNote,
            this.bbiDeleteNote,
            this.bbiListeningRecord,
            this.bbiNewIncident,
            this.bbiEditIncident,
            this.bbiFireFlag,
            this.bbiRescueFlag,
            this.bbiEmergencyFlag,
            this.bbiMiscFlag,
            this.rgbiCurrentViewIncidents,
            this.bbiFieldSupport,
            this.bbiDispatchUnit,
            this.bsiInfo,
            this.bsiTemp});
            this.rcMain.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.rcMain.Location = new System.Drawing.Point(0, 0);
            this.rcMain.MaxItemId = 37;
            this.rcMain.Name = "rcMain";
            this.rcMain.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpDispatch,
            this.rpIncident,
            this.rpView});
            this.rcMain.Size = new System.Drawing.Size(1748, 160);
            this.rcMain.StatusBar = this.rsbMain;
            // 
            // bsiNavigation
            // 
            this.bsiNavigation.Caption = "Navigation";
            this.bsiNavigation.Id = 1;
            this.bsiNavigation.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.compass_16x16;
            this.bsiNavigation.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.compass_32x32;
            this.bsiNavigation.Name = "bsiNavigation";
            // 
            // bbiAnswer
            // 
            this.bbiAnswer.Caption = "Answer";
            this.bbiAnswer.Id = 2;
            this.bbiAnswer.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.phone_pickup_32x32;
            this.bbiAnswer.Name = "bbiAnswer";
            this.bbiAnswer.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiHangup
            // 
            this.bbiHangup.Caption = "Hangup";
            this.bbiHangup.Id = 3;
            this.bbiHangup.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.phone_hangup_16x16;
            this.bbiHangup.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.phone_hangup_32x32;
            this.bbiHangup.Name = "bbiHangup";
            this.bbiHangup.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiTransfer
            // 
            this.bbiTransfer.Caption = "Transfer";
            this.bbiTransfer.Id = 4;
            this.bbiTransfer.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.phone_redirect_16x16;
            this.bbiTransfer.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.phone_redirect_32x32;
            this.bbiTransfer.Name = "bbiTransfer";
            this.bbiTransfer.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiConference
            // 
            this.bbiConference.Caption = "Conference";
            this.bbiConference.Id = 5;
            this.bbiConference.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.phone_conference_16x16;
            this.bbiConference.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.phone_conference_32x32;
            this.bbiConference.Name = "bbiConference";
            this.bbiConference.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiHold
            // 
            this.bbiHold.Caption = "Hold";
            this.bbiHold.Id = 6;
            this.bbiHold.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.phone_hold_16x16;
            this.bbiHold.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.phone_hold_32x32;
            this.bbiHold.Name = "bbiHold";
            this.bbiHold.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiDialup
            // 
            this.bbiDialup.Caption = "Dialup";
            this.bbiDialup.Id = 7;
            this.bbiDialup.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.phone_call_16x16;
            this.bbiDialup.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.phone_call_32x32;
            this.bbiDialup.Name = "bbiDialup";
            this.bbiDialup.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiEnable
            // 
            this.bbiEnable.Caption = "Enable";
            this.bbiEnable.Id = 8;
            this.bbiEnable.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.phone_receiver_16x16;
            this.bbiEnable.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.phone_receiver_32x32;
            this.bbiEnable.Name = "bbiEnable";
            this.bbiEnable.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bsiConsoleInfo
            // 
            this.bsiConsoleInfo.Id = 9;
            this.bsiConsoleInfo.Name = "bsiConsoleInfo";
            this.bsiConsoleInfo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiRequestAli
            // 
            this.bbiRequestAli.Caption = "Request ALI";
            this.bbiRequestAli.Id = 10;
            this.bbiRequestAli.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.location_pin_16x16;
            this.bbiRequestAli.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.location_pin_32x32;
            this.bbiRequestAli.Name = "bbiRequestAli";
            this.bbiRequestAli.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiCallDisposition
            // 
            this.bbiCallDisposition.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCallDisposition.Caption = "Disposition";
            this.bbiCallDisposition.DropDownControl = this.mnuCallDisposition;
            this.bbiCallDisposition.Id = 11;
            this.bbiCallDisposition.Name = "bbiCallDisposition";
            this.bbiCallDisposition.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // mnuCallDisposition
            // 
            this.mnuCallDisposition.ItemLinks.Add(this.bbiAnnounce);
            this.mnuCallDisposition.ItemLinks.Add(this.bbiNotResponse);
            this.mnuCallDisposition.ItemLinks.Add(this.bbiPrank);
            this.mnuCallDisposition.ItemLinks.Add(this.bbiWrongNumber);
            this.mnuCallDisposition.ItemLinks.Add(this.bbiTestCall);
            this.mnuCallDisposition.ItemLinks.Add(this.bbiIncidentCall);
            this.mnuCallDisposition.Name = "mnuCallDisposition";
            this.mnuCallDisposition.Ribbon = this.rcMain;
            // 
            // bbiAnnounce
            // 
            this.bbiAnnounce.Caption = "Announce";
            this.bbiAnnounce.Id = 12;
            this.bbiAnnounce.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.user_message_16x16;
            this.bbiAnnounce.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.user_message_32x32;
            this.bbiAnnounce.Name = "bbiAnnounce";
            // 
            // bbiNotResponse
            // 
            this.bbiNotResponse.Caption = "Not Response";
            this.bbiNotResponse.Id = 13;
            this.bbiNotResponse.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.user_time_16x16;
            this.bbiNotResponse.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.user_time_32x32;
            this.bbiNotResponse.Name = "bbiNotResponse";
            // 
            // bbiPrank
            // 
            this.bbiPrank.Caption = "Prank";
            this.bbiPrank.Id = 14;
            this.bbiPrank.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.masks_16x16;
            this.bbiPrank.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.masks_32x32;
            this.bbiPrank.Name = "bbiPrank";
            // 
            // bbiWrongNumber
            // 
            this.bbiWrongNumber.Caption = "Wrong Number";
            this.bbiWrongNumber.Id = 15;
            this.bbiWrongNumber.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.symbol_questionmark_16x16;
            this.bbiWrongNumber.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.symbol_questionmark_32x32;
            this.bbiWrongNumber.Name = "bbiWrongNumber";
            // 
            // bbiTestCall
            // 
            this.bbiTestCall.Caption = "Test";
            this.bbiTestCall.Id = 16;
            this.bbiTestCall.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.crash_testdummy_16x16;
            this.bbiTestCall.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.crash_testdummy_32x32;
            this.bbiTestCall.Name = "bbiTestCall";
            // 
            // bbiIncidentCall
            // 
            this.bbiIncidentCall.Caption = "Incident";
            this.bbiIncidentCall.Id = 17;
            this.bbiIncidentCall.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.alarm_16x16;
            this.bbiIncidentCall.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.alarm_32x32;
            this.bbiIncidentCall.Name = "bbiIncidentCall";
            // 
            // bbiBuildSquad
            // 
            this.bbiBuildSquad.Caption = "Build Squad";
            this.bbiBuildSquad.Id = 18;
            this.bbiBuildSquad.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.industrial_robot_16x16;
            this.bbiBuildSquad.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.industrial_robot_32x32;
            this.bbiBuildSquad.Name = "bbiBuildSquad";
            this.bbiBuildSquad.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiNewNote
            // 
            this.bbiNewNote.Caption = "New Note";
            this.bbiNewNote.Id = 20;
            this.bbiNewNote.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.document_new_16x16;
            this.bbiNewNote.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.document_new_32x32;
            this.bbiNewNote.Name = "bbiNewNote";
            // 
            // bbiEditNote
            // 
            this.bbiEditNote.Caption = "Edit Note";
            this.bbiEditNote.Id = 21;
            this.bbiEditNote.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.note_edit_16x16;
            this.bbiEditNote.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.note_edit_32x32;
            this.bbiEditNote.Name = "bbiEditNote";
            this.bbiEditNote.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiDeleteNote
            // 
            this.bbiDeleteNote.Caption = "Delete";
            this.bbiDeleteNote.Id = 22;
            this.bbiDeleteNote.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.note_delete_16x16;
            this.bbiDeleteNote.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.note_delete_32x32;
            this.bbiDeleteNote.Name = "bbiDeleteNote";
            this.bbiDeleteNote.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // bbiListeningRecord
            // 
            this.bbiListeningRecord.Caption = "Listening Record";
            this.bbiListeningRecord.Id = 23;
            this.bbiListeningRecord.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.cd_16x16;
            this.bbiListeningRecord.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.cd_32x32;
            this.bbiListeningRecord.Name = "bbiListeningRecord";
            // 
            // bbiNewIncident
            // 
            this.bbiNewIncident.Caption = "New Incident";
            this.bbiNewIncident.Id = 24;
            this.bbiNewIncident.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.briefcase_add_16x16;
            this.bbiNewIncident.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.briefcase_add_32x32;
            this.bbiNewIncident.Name = "bbiNewIncident";
            // 
            // bbiEditIncident
            // 
            this.bbiEditIncident.Caption = "Edit Incident";
            this.bbiEditIncident.Id = 25;
            this.bbiEditIncident.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.briefcase_edit_16x16;
            this.bbiEditIncident.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.briefcase_edit_32x32;
            this.bbiEditIncident.Name = "bbiEditIncident";
            // 
            // bbiFireFlag
            // 
            this.bbiFireFlag.Caption = "Fire";
            this.bbiFireFlag.Id = 26;
            this.bbiFireFlag.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.flag_red_16x16;
            this.bbiFireFlag.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.flag_red_32x32;
            this.bbiFireFlag.Name = "bbiFireFlag";
            // 
            // bbiRescueFlag
            // 
            this.bbiRescueFlag.Caption = "Rescue";
            this.bbiRescueFlag.Id = 27;
            this.bbiRescueFlag.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.flag_blue_16x16;
            this.bbiRescueFlag.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.flag_blue_32x32;
            this.bbiRescueFlag.Name = "bbiRescueFlag";
            this.bbiRescueFlag.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // bbiEmergencyFlag
            // 
            this.bbiEmergencyFlag.Caption = "Emergency";
            this.bbiEmergencyFlag.Id = 28;
            this.bbiEmergencyFlag.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.flag_green_16x16;
            this.bbiEmergencyFlag.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.flag_green_32x32;
            this.bbiEmergencyFlag.Name = "bbiEmergencyFlag";
            this.bbiEmergencyFlag.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // bbiMiscFlag
            // 
            this.bbiMiscFlag.Caption = "Misc";
            this.bbiMiscFlag.Id = 29;
            this.bbiMiscFlag.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.flag_yellow_16x16;
            this.bbiMiscFlag.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.flag_yellow_32x32;
            this.bbiMiscFlag.Name = "bbiMiscFlag";
            this.bbiMiscFlag.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // rgbiCurrentViewIncidents
            // 
            this.rgbiCurrentViewIncidents.Caption = "Current View";
            // 
            // 
            // 
            galleryItemGroup1.Caption = "Group";
            galleryItem1.Caption = "List";
            galleryItem1.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.index_32x32;
            galleryItem1.Tag = "IncidentListTag";
            galleryItem2.Caption = "Opened";
            galleryItem2.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.signboard_open_32x32;
            galleryItem2.Tag = "IncidentOpenedTag";
            galleryItem3.Caption = "Today";
            galleryItem3.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.calendar_32x32;
            galleryItem3.Tag = "IncidentTodayTag";
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2,
            galleryItem3});
            this.rgbiCurrentViewIncidents.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.rgbiCurrentViewIncidents.Gallery.ImageSize = new System.Drawing.Size(37, 34);
            this.rgbiCurrentViewIncidents.Gallery.ShowItemText = true;
            this.rgbiCurrentViewIncidents.Id = 30;
            this.rgbiCurrentViewIncidents.Name = "rgbiCurrentViewIncidents";
            // 
            // bbiFieldSupport
            // 
            this.bbiFieldSupport.Caption = "Field Support";
            this.bbiFieldSupport.Id = 31;
            this.bbiFieldSupport.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.user_headset_16x16;
            this.bbiFieldSupport.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.user_headset_32x32;
            this.bbiFieldSupport.Name = "bbiFieldSupport";
            // 
            // bbiDispatchUnit
            // 
            this.bbiDispatchUnit.Caption = "Dispatch Unit";
            this.bbiDispatchUnit.Id = 32;
            this.bbiDispatchUnit.ImageOptions.Image = global::Cad.Clients.WinForm.Properties.Resources.loudspeaker_16x16;
            this.bbiDispatchUnit.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.loudspeaker_32x32;
            this.bbiDispatchUnit.Name = "bbiDispatchUnit";
            // 
            // bsiInfo
            // 
            this.bsiInfo.Id = 35;
            this.bsiInfo.Name = "bsiInfo";
            // 
            // bsiTemp
            // 
            this.bsiTemp.Id = 36;
            this.bsiTemp.Name = "bsiTemp";
            // 
            // rpDispatch
            // 
            this.rpDispatch.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgCall,
            this.rpgDispatch,
            this.rptDispatchNote,
            this.rpgRecording});
            this.rpDispatch.Name = "rpDispatch";
            this.rpDispatch.Tag = "Dispatch";
            this.rpDispatch.Text = "DISPATCH";
            // 
            // rpgCall
            // 
            this.rpgCall.ItemLinks.Add(this.bbiEnable, true);
            this.rpgCall.ItemLinks.Add(this.bbiAnswer);
            this.rpgCall.ItemLinks.Add(this.bbiHangup);
            this.rpgCall.ItemLinks.Add(this.bbiHold);
            this.rpgCall.ItemLinks.Add(this.bbiTransfer);
            this.rpgCall.ItemLinks.Add(this.bbiConference);
            this.rpgCall.ItemLinks.Add(this.bbiDialup);
            this.rpgCall.ItemLinks.Add(this.bbiRequestAli);
            this.rpgCall.ItemLinks.Add(this.bbiCallDisposition, true);
            this.rpgCall.Name = "rpgCall";
            this.rpgCall.Text = "Call";
            // 
            // rpgDispatch
            // 
            this.rpgDispatch.AllowTextClipping = false;
            this.rpgDispatch.ItemLinks.Add(this.bbiBuildSquad);
            this.rpgDispatch.ItemLinks.Add(this.bbiDispatchUnit);
            this.rpgDispatch.Name = "rpgDispatch";
            this.rpgDispatch.Text = "Dispatch";
            // 
            // rptDispatchNote
            // 
            this.rptDispatchNote.ItemLinks.Add(this.bbiNewNote);
            this.rptDispatchNote.ItemLinks.Add(this.bbiEditNote);
            this.rptDispatchNote.ItemLinks.Add(this.bbiDeleteNote);
            this.rptDispatchNote.Name = "rptDispatchNote";
            this.rptDispatchNote.Text = "Note";
            // 
            // rpgRecording
            // 
            this.rpgRecording.ItemLinks.Add(this.bbiListeningRecord);
            this.rpgRecording.Name = "rpgRecording";
            this.rpgRecording.Text = "Recording";
            // 
            // rpIncident
            // 
            this.rpIncident.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgIncidents,
            this.rpgCategory,
            this.rpgCurrentView,
            this.rpgAction});
            this.rpIncident.Name = "rpIncident";
            this.rpIncident.Tag = "Incident";
            this.rpIncident.Text = "INCIDENT";
            // 
            // rpgIncidents
            // 
            this.rpgIncidents.ItemLinks.Add(this.bbiNewIncident);
            this.rpgIncidents.ItemLinks.Add(this.bbiEditIncident);
            this.rpgIncidents.Name = "rpgIncidents";
            this.rpgIncidents.Text = "New / Edit";
            // 
            // rpgCategory
            // 
            this.rpgCategory.ItemLinks.Add(this.bbiFireFlag);
            this.rpgCategory.ItemLinks.Add(this.bbiRescueFlag);
            this.rpgCategory.ItemLinks.Add(this.bbiEmergencyFlag);
            this.rpgCategory.ItemLinks.Add(this.bbiMiscFlag);
            this.rpgCategory.Name = "rpgCategory";
            this.rpgCategory.Text = "Category";
            // 
            // rpgCurrentView
            // 
            this.rpgCurrentView.ItemLinks.Add(this.rgbiCurrentViewIncidents);
            this.rpgCurrentView.Name = "rpgCurrentView";
            this.rpgCurrentView.Text = "Current View";
            // 
            // rpgAction
            // 
            this.rpgAction.ItemLinks.Add(this.bbiFieldSupport);
            this.rpgAction.Name = "rpgAction";
            this.rpgAction.Text = "Action";
            // 
            // rpView
            // 
            this.rpView.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.rpView.Name = "rpView";
            this.rpView.Text = "VIEW";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.bsiNavigation);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Navigation";
            // 
            // rsbMain
            // 
            this.rsbMain.ItemLinks.Add(this.bsiConsoleInfo);
            this.rsbMain.ItemLinks.Add(this.bsiInfo, true);
            this.rsbMain.ItemLinks.Add(this.bsiTemp, true);
            this.rsbMain.Location = new System.Drawing.Point(0, 925);
            this.rsbMain.Name = "rsbMain";
            this.rsbMain.Ribbon = this.rcMain;
            this.rsbMain.Size = new System.Drawing.Size(1748, 24);
            // 
            // nbcMain
            // 
            this.nbcMain.ActiveGroup = this.nbgModules;
            this.nbcMain.Dock = System.Windows.Forms.DockStyle.Left;
            this.nbcMain.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nbgModules});
            this.nbcMain.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.nbiDispatch,
            this.nbiIncident});
            this.nbcMain.LinkSelectionMode = DevExpress.XtraNavBar.LinkSelectionModeType.OneInControl;
            this.nbcMain.Location = new System.Drawing.Point(0, 160);
            this.nbcMain.Margin = new System.Windows.Forms.Padding(5);
            this.nbcMain.MenuManager = this.rcMain;
            this.nbcMain.Name = "nbcMain";
            this.nbcMain.NavigationPaneGroupClientHeight = 310;
            this.nbcMain.OptionsNavPane.ExpandedWidth = 150;
            this.nbcMain.OptionsNavPane.ShowOverflowButton = false;
            this.nbcMain.OptionsNavPane.ShowOverflowPanel = false;
            this.nbcMain.OptionsNavPane.ShowSplitter = false;
            this.nbcMain.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.nbcMain.Size = new System.Drawing.Size(150, 765);
            this.nbcMain.TabIndex = 4;
            this.nbcMain.Text = "navMain";
            this.nbcMain.SelectedLinkChanged += new DevExpress.XtraNavBar.ViewInfo.NavBarSelectedLinkChangedEventHandler(this.nbcMain_SelectedLinkChanged);
            this.nbcMain.NavPaneStateChanged += new System.EventHandler(this.nbcMain_NavPaneStateChanged);
            // 
            // nbgModules
            // 
            this.nbgModules.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbgModules.Appearance.Options.UseFont = true;
            this.nbgModules.Caption = "Jobs";
            this.nbgModules.Expanded = true;
            this.nbgModules.GroupCaptionUseImage = DevExpress.XtraNavBar.NavBarImage.Large;
            this.nbgModules.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.nbgModules.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiDispatch),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiIncident)});
            this.nbgModules.Name = "nbgModules";
            this.nbgModules.NavigationPaneVisible = false;
            this.nbgModules.SelectedLinkIndex = 1;
            // 
            // nbiDispatch
            // 
            this.nbiDispatch.Caption = "Dispatch";
            this.nbiDispatch.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.headset_32x32;
            this.nbiDispatch.ImageOptions.SmallImage = global::Cad.Clients.WinForm.Properties.Resources.headset_16x16;
            this.nbiDispatch.Name = "nbiDispatch";
            // 
            // nbiIncident
            // 
            this.nbiIncident.Caption = "Incident";
            this.nbiIncident.ImageOptions.LargeImage = global::Cad.Clients.WinForm.Properties.Resources.briefcase_document_32x32;
            this.nbiIncident.ImageOptions.SmallImage = global::Cad.Clients.WinForm.Properties.Resources.briefcase_document_16x16;
            this.nbiIncident.Name = "nbiIncident";
            // 
            // pcMain
            // 
            this.pcMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcMain.Location = new System.Drawing.Point(150, 160);
            this.pcMain.Margin = new System.Windows.Forms.Padding(5);
            this.pcMain.Name = "pcMain";
            this.pcMain.Size = new System.Drawing.Size(1598, 765);
            this.pcMain.TabIndex = 8;
            // 
            // MainForm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1748, 949);
            this.Controls.Add(this.pcMain);
            this.Controls.Add(this.nbcMain);
            this.Controls.Add(this.rsbMain);
            this.Controls.Add(this.rcMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("MainForm.IconOptions.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(1750, 950);
            this.Name = "MainForm";
            this.Ribbon = this.rcMain;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.rsbMain;
            this.Text = "CAD";
            ((System.ComponentModel.ISupportInitialize)(this.rcMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuCallDisposition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbcMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl rcMain;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpDispatch;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCall;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpIncident;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCategory;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpView;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar rsbMain;
        private DevExpress.XtraNavBar.NavBarControl nbcMain;
        private DevExpress.XtraNavBar.NavBarGroup nbgModules;
        private DevExpress.XtraNavBar.NavBarItem nbiDispatch;
        private DevExpress.XtraNavBar.NavBarItem nbiIncident;
        private DevExpress.XtraBars.BarSubItem bsiNavigation;
        private DevExpress.XtraEditors.PanelControl pcMain;
        private DevExpress.XtraBars.BarButtonItem bbiAnswer;
        private DevExpress.XtraBars.BarButtonItem bbiHangup;
        private DevExpress.XtraBars.BarButtonItem bbiTransfer;
        private DevExpress.XtraBars.BarButtonItem bbiConference;
        private DevExpress.XtraBars.BarButtonItem bbiHold;
        private DevExpress.XtraBars.BarButtonItem bbiDialup;
        private DevExpress.XtraBars.BarButtonItem bbiEnable;
        private DevExpress.XtraBars.BarStaticItem bsiConsoleInfo;
        private DevExpress.XtraBars.BarButtonItem bbiRequestAli;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgDispatch;
        private DevExpress.XtraBars.BarButtonItem bbiCallDisposition;
        private DevExpress.XtraBars.BarButtonItem bbiAnnounce;
        private DevExpress.XtraBars.PopupMenu mnuCallDisposition;
        private DevExpress.XtraBars.BarButtonItem bbiNotResponse;
        private DevExpress.XtraBars.BarButtonItem bbiPrank;
        private DevExpress.XtraBars.BarButtonItem bbiWrongNumber;
        private DevExpress.XtraBars.BarButtonItem bbiTestCall;
        private DevExpress.XtraBars.BarButtonItem bbiIncidentCall;
        private DevExpress.XtraBars.BarButtonItem bbiBuildSquad;
        private DevExpress.XtraBars.BarButtonItem bbiNewNote;
        private DevExpress.XtraBars.BarButtonItem bbiEditNote;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rptDispatchNote;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteNote;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgRecording;
        private DevExpress.XtraBars.BarButtonItem bbiListeningRecord;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgIncidents;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCurrentView;
        private DevExpress.XtraBars.BarButtonItem bbiNewIncident;
        private DevExpress.XtraBars.BarButtonItem bbiEditIncident;
        private DevExpress.XtraBars.BarButtonItem bbiFireFlag;
        private DevExpress.XtraBars.BarButtonItem bbiRescueFlag;
        private DevExpress.XtraBars.BarButtonItem bbiEmergencyFlag;
        private DevExpress.XtraBars.BarButtonItem bbiMiscFlag;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiCurrentViewIncidents;
        private DevExpress.XtraBars.BarButtonItem bbiFieldSupport;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAction;
        private DevExpress.XtraBars.BarButtonItem bbiDispatchUnit;
        private DevExpress.XtraBars.BarStaticItem bsiInfo;
        private DevExpress.XtraBars.BarStaticItem bsiTemp;
    }
}

