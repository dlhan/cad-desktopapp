﻿//
// Cad.Clients.WinForm.MainForm
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraNavBar;
using DevExpress.XtraNavBar.ViewInfo;
using DevExpress.XtraSplashScreen;
using Cad.Clients.WinForm.Modules;

namespace Cad.Clients.WinForm
{
    public partial class MainForm : RibbonForm
    {
        #region VARIABLES
        private ModuleNavigator _moduleNavigator;
        internal List<BarItem> _allowCustomizationMenuList = new List<BarItem>();
        #endregion

        #region PROPERTIES
        public RibbonStatusBar RibbonStatusBar { get { return rsbMain; } }
        #endregion

        #region CONSTRUCTORS
        public MainForm()
        {
            InitializeComponent();
            Initialize();
        }
        #endregion

        #region PRIVATE METHODS
        private void Initialize()
        {
            this.Text = $"{AppContext.AppName} - v{AppContext.AppVersion}";

            _moduleNavigator = new ModuleNavigator(rcMain, pcMain);
            InitializeRibbonControls();
            InitializeNavControls();
        }

        /// <summary>
        /// Initialize navigation bar control and ribbon 
        /// submenu item(bsiNavigation) of view page(rpView).
        /// </summary>
        private void InitializeNavControls()
        {
            // initialize navbarcontrol.
            nbiDispatch.Tag = new NavBarGroupTagObject(
                Properties.Resources.DispatchModuleName, 
                typeof(DispatchModule));
            nbiIncident.Tag = new NavBarGroupTagObject(
                Properties.Resources.IncidentModuleName,
                typeof(IncidentModule));
            nbgModules.SelectedLinkIndex = 0;

            // initialize submenu of ribbon control.
            foreach (NavBarItemLink link in nbgModules.ItemLinks)
            {
                BarButtonItem item = new BarButtonItem(rcMain.Manager, link.Item.Caption)
                {
                    Tag = link,
                    Glyph = link.Item.SmallImage
                };
                item.ItemClick += new ItemClickEventHandler(bsiNavigation_ItemClick);
                bsiNavigation.ItemLinks.Add(item);
            }
        }

        private void InitializeRibbonControls()
        {
            rpDispatch.Tag = Properties.Resources.DispatchModuleName;
            rpIncident.Tag = Properties.Resources.IncidentModuleName;

            // dispatch tab
            InitializeRibbonButton(bbiEnable, 
                Resources.CallEnableTag, 
                Properties.Resources.EnableCallDescription);
            InitializeRibbonButton(bbiAnswer, 
                Resources.CallAnswerTag,
                Properties.Resources.AnswerCallDescription);
            InitializeRibbonButton(bbiHangup, 
                Resources.CallHanupTag,
                Properties.Resources.HangupCallDescription);
            InitializeRibbonButton(bbiHold, 
                Resources.CallHoldTag,
                Properties.Resources.HoldCallDescription);
            InitializeRibbonButton(bbiTransfer, 
                Resources.CallTransferTag,
                Properties.Resources.TransferCallDescription);
            InitializeRibbonButton(bbiConference, 
                Resources.CallConferenceTag,
                Properties.Resources.ConferenceCallDescription);
            InitializeRibbonButton(bbiDialup, 
                Resources.CallDialupTag,
                Properties.Resources.DialupCallDescription);
            InitializeRibbonButton(bbiRequestAli, 
                Resources.RequestAliTag,
                Properties.Resources.RequestAliDescription);
            InitializeRibbonButton(bbiCallDisposition, 
                Resources.CallDispositionTag,
                string.Empty);
            InitializeRibbonButton(bbiAnnounce, 
                Resources.DispositionAnnounceTag,
                Properties.Resources.AnnounceDescription);
            InitializeRibbonButton(bbiNotResponse, 
                Resources.DispositionNotResponseTag,
                Properties.Resources.NotResponseDescription);
            InitializeRibbonButton(bbiPrank, 
                Resources.DispositionPrankTag,
                Properties.Resources.PrankDescription);
            InitializeRibbonButton(bbiWrongNumber, 
                Resources.DispositionWrongNumberTag,
                Properties.Resources.WrongNumberDescription);
            InitializeRibbonButton(bbiTestCall,
                Resources.DispositionTestCallTag,
                Properties.Resources.TestCallDescription);
            InitializeRibbonButton(bbiIncidentCall,
                Resources.DispositionIncidentCallTag,
                Properties.Resources.IncidentCallDescription);

            InitializeRibbonButton(bbiBuildSquad,
                Resources.SquadBuildTag,
                Properties.Resources.BuildSquadDescription);
            InitializeRibbonButton(bbiDispatchUnit,
                Resources.UnitDispatchTag,
                Properties.Resources.DispatchUnitDescription);

            InitializeRibbonButton(bbiNewNote,
                Resources.NoteNewTag,
                Properties.Resources.NewNoteDescription);
            InitializeRibbonButton(bbiEditNote,
                Resources.NoteEditTag,
                Properties.Resources.EditNoteDescription);
            InitializeRibbonButton(bbiDeleteNote,
                Resources.NoteDeleteTag,
                Properties.Resources.DeleteNoteDescription);
            
            InitializeRibbonButton(bbiListeningRecord,
                Resources.RecordListeningTag,
                Properties.Resources.RecordListeningDescription);

            InitailzePopupMenuItem(bbiCallDisposition, bbiAnnounce,
                Resources.DispositionAnnounceTag);

            // incident tab
            InitializeRibbonButton(bbiNewIncident,
                Resources.IncidentNewTag,
                Properties.Resources.NewIncidentDescription);
            InitializeRibbonButton(bbiEditIncident,
                Resources.IncidentEditTag,
                Properties.Resources.EditIncidentDescription);

            // view tab
            _allowCustomizationMenuList.Add(bsiNavigation);
        }

        private void InitializeRibbonButton(BarButtonItem item, object tag, string hint)
        {
            item.ItemClick += new ItemClickEventHandler(BarButton_ItemClick);
            item.Hint = hint;
            item.Tag = tag;
        }

        private void InitailzePopupMenuItem(BarItem parent, BarItem child, object tag)
        {
            parent.Glyph = child.Glyph;
            parent.LargeGlyph = child.LargeGlyph;
            parent.SuperTip = child.SuperTip;
            parent.Hint = child.Hint;
            parent.Tag = tag;
        }
        #endregion

        #region INTERNAL METHODS
        internal void ShowInfo(int? count)
        {
            bsiInfo.Caption = count is null 
                ? string.Empty
                : string.Format(Properties.Resources.InfoText, count);
        }

        internal void ShowConsoleInfo()
        {
            Equipments.Cti.AgentState state = DispatchManager.GetCtiState();

            if (state.HasFlag(Equipments.Cti.AgentState.Enabled))
            {
                bbiEnable.Caption = "Disable";
                bbiEnable.Hint = Properties.Resources.DisableCallDescription;
                bbiEnable.ImageOptions.Image = Properties.Resources.phone_reject_16x16;
                bbiEnable.ImageOptions.LargeImage = Properties.Resources.phone_reject_32x32;
                bsiConsoleInfo.Caption = $"Console #{AppContext.DispatchConsole.Id} - Enabled";
            }
            else
            {
                bbiEnable.Caption = "Enable";
                bbiEnable.Hint = Properties.Resources.EnableCallDescription;
                bbiEnable.ImageOptions.Image = Properties.Resources.phone_receiver_16x16;
                bbiEnable.ImageOptions.LargeImage = Properties.Resources.phone_receiver_32x32;
                bsiConsoleInfo.Caption = $"Console #{AppContext.DispatchConsole.Id} - Disabled";
            }
        }
        #endregion

        #region PUBLIC METHODS
        public void ShowInfo(bool visible)
        {
            bsiInfo.Visibility = bsiTemp.Visibility = visible
                ? BarItemVisibility.Always
                : BarItemVisibility.Never;
        }
        #endregion

        #region CONTROL EVENTS
        /// <summary>
        /// Ribbon button item click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (((BarButtonItem)e.Item).Name)
            {
                case "bbiAnnounce":
                    InitailzePopupMenuItem(bbiCallDisposition, 
                        bbiAnnounce, Resources.DispositionAnnounceTag);
                    break;
                case "bbiNotResponse":
                    InitailzePopupMenuItem(bbiCallDisposition,
                        bbiNotResponse, Resources.DispositionNotResponseTag);
                    break;
                case "bbiPrank":
                    InitailzePopupMenuItem(bbiCallDisposition,
                        bbiPrank, Resources.DispositionPrankTag);
                    break;
                case "bbiWrongNumber":
                    InitailzePopupMenuItem(bbiCallDisposition,
                        bbiWrongNumber, Resources.DispositionWrongNumberTag);
                    break;
                case "bbiTestCall":
                    InitailzePopupMenuItem(bbiCallDisposition,
                        bbiTestCall, Resources.DispositionTestCallTag);
                    break;
                case "bbiIncidentCall":
                    InitailzePopupMenuItem(bbiCallDisposition,
                        bbiIncidentCall, Resources.DispositionIncidentCallTag);
                    break;
                default: break;
            }
            _moduleNavigator.CurrentModule.ButtonClick(e.Item.Tag.ToString());
        }

        private void nbcMain_NavPaneStateChanged(object sender, EventArgs e)
        {
        }

        private void nbcMain_SelectedLinkChanged(object sender, 
            NavBarSelectedLinkChangedEventArgs e)
        {
            if (e.Link != null)
                _moduleNavigator.ChangeSelectedItem(e.Link, null);
        }

        private void bsiNavigation_ItemClick(object sender, ItemClickEventArgs e)
        {
            nbgModules.SelectedLink = (NavBarItemLink)e.Item.Tag;
        }

        internal void OnModuleShown(BaseModule baseModel)
        {
            // rpgPrint.Visible = CurrentPrintableComponent != null;
        }
        #endregion

        #region FORM EVENTS
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            SplashScreenManager.CloseForm(false);
        }
        #endregion
    }
}
