﻿//
// Cad.Clients.WinForm.Modules.BaseControl
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using DevExpress.Utils.Design;
using DevExpress.XtraEditors;

namespace Cad.Clients.WinForm.Modules
{
    /// <summary>
    /// Parent control class for BaseModule.
    /// </summary>
    public class BaseControl : XtraUserControl
    {
        #region CONSTRUCTORS
        public BaseControl()
        {
            if (!DesignTimeTools.IsDesignMode)
            {
                LookAndFeel.ActiveLookAndFeel.StyleChanged +=
                    new EventHandler(ActiveLookAndFeel_StyleChanged);
            }
            this.VisibleChanged += new EventHandler(BaseControl_VisibleChanged);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !DesignTimeTools.IsDesignMode)
                LookAndFeel.ActiveLookAndFeel.StyleChanged -=
                    new EventHandler(ActiveLookAndFeel_StyleChanged);
            base.Dispose(disposing);
        }
        #endregion

        #region METHODS
        internal virtual void ShowControlFirstTime() { }
        protected virtual void LookAndFeelStyleChanged() { }
        #endregion

        #region EVENTS
        internal void ActiveLookAndFeel_StyleChanged(object sender, EventArgs e)
        {
            LookAndFeelStyleChanged();
        }

        internal void BaseControl_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                ShowControlFirstTime();
                this.VisibleChanged -= new EventHandler(BaseControl_VisibleChanged);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!DesignTimeTools.IsDesignMode) LookAndFeelStyleChanged();
        }
        #endregion
    }
}
