﻿//
// Cad.Clients.WinForm.Modules.BaseModule
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;

namespace Cad.Clients.WinForm.Modules
{
    /// <summary>
    /// Base class of Job modules.
    /// </summary>
    public class BaseModule : BaseControl
    {
        #region PROPERTIES
        [DefaultValue(false)]
        protected virtual bool AutoMergeRibbon { get; private set; }

        protected RibbonControl MainRibbon { get { return OwnerForm.Ribbon; } }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
            Browsable(false)]
        protected virtual RibbonControl ChildRibbon
        {
            get
            {
                if (!AutoMergeRibbon) return null;
                return FindRibbon(Controls);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
            Browsable(false)]
        protected virtual RibbonStatusBar ChildRibbonStatusBar
        {
            get
            {
                if (ChildRibbon != null) return ChildRibbon.StatusBar;
                return null;
            }
        }

        public string PartName { get; private set; } = string.Empty;

        public virtual string ModuleName
        {
            get { return this.GetType().Name; }
        }

        internal MainForm OwnerForm 
        {
            get { return this.FindForm() as MainForm; } 
        }

        protected virtual GridControl Grid { get { return null; } }
        #endregion

        #region CONSTRUCTORS
        public BaseModule()
        {
        }
        #endregion

        #region METHODS
        private void CapitalizeChildRibbonPages()
        {
            if (ChildRibbon == null) return;

            foreach (RibbonPage page in ChildRibbon.Pages) 
                page.Text = page.Text.ToUpper();

            foreach (RibbonPageCategory category in ChildRibbon.PageCategories)
                foreach (RibbonPage page in category.Pages)
                    page.Text = page.Text.ToUpper();
        }

        private void SetMenuManager(ControlCollection controlCollection, 
            IDXMenuManager manager)
        {
            foreach (Control control in controlCollection)
            {
                GridControl gridControl = control as GridControl;
                if (gridControl != null)
                {
                    gridControl.MenuManager = manager;
                    break;
                }
                //PivotGridControl pivot = control as PivotGridControl;
                //if (pivot != null)
                //{
                //    pivot.MenuManager = manager;
                //    break;
                //}
                BaseEdit edit = control as BaseEdit;
                if (edit != null)
                {
                    edit.MenuManager = manager;
                    break;
                }
                SetMenuManager(control.Controls, manager);
            }
        }

        internal RibbonControl FindRibbon(ControlCollection controls)
        {
            RibbonControl ribbon = controls.OfType<Control>()
                .FirstOrDefault(x => x is RibbonControl) as RibbonControl;
            if (ribbon != null) return ribbon;

            foreach (Control control in controls)
            {
                if (!control.HasChildren) continue;
                if ((ribbon = FindRibbon(control.Controls)) != null)
                    return ribbon;
            }
            return null;
        }

        internal virtual void FocusObject(object o) 
        { 
        }

        internal virtual void HideModule()
        {
            if (!AutoMergeRibbon || OwnerForm == null) return;
            if (OwnerForm.Ribbon.MergedRibbon == ChildRibbon)
            {
                RibbonPage page = OwnerForm.Ribbon.MergedPages
                    .GetPageByText("VIEW");
                if (page != null) OwnerForm.Ribbon.Pages.Add(page);
                OwnerForm.Ribbon.UnMergeRibbon();

            }
            OwnerForm.RibbonStatusBar.UnMergeStatusBar();
            OwnerForm.ShowInfo(true);
        }

        internal virtual void ShowModule(bool firstShow)
        {
            if (OwnerForm == null) return;
            if (AutoMergeRibbon && ChildRibbon != null)
            {
                OwnerForm.Ribbon.MergeRibbon(ChildRibbon);
                RibbonPage page = OwnerForm.Ribbon.Pages.GetPageByText("VIEW");
                if (page != null)
                {
                    OwnerForm.Ribbon.MergedPages.Remove(page);
                    OwnerForm.Ribbon.MergedPages.Insert(
                        OwnerForm.Ribbon.MergedPages.Count, page);
                }
                if (ChildRibbonStatusBar != null)
                {
                    OwnerForm.RibbonStatusBar.MergeStatusBar(ChildRibbonStatusBar);
                    OwnerForm.ShowInfo(false);
                }
            }
            //OwnerForm.SaveAsMenuItem.Enabled = SaveAsEnable;
            //OwnerForm.SaveAttachmentMenuItem.Enabled = SaveAttachmentEnable;
            //ShowReminder();
            ShowInfo();
            //OwnerForm.ZoomManager.ZoomFactor = (int)(ZoomFactor * 100);
            //SetZoomCaption();
            //OwnerForm.EnableZoomControl(AllowZoomControl);
            OwnerForm.OnModuleShown(this);
        }

        internal void ShowConsoleInfo()
        {
            if (OwnerForm == null) return;
            OwnerForm.ShowConsoleInfo();
        }

        internal void ShowInfo()
        {
            if (OwnerForm == null) return;
            if (Grid == null) 
            { 
                OwnerForm.ShowInfo(null); 
                return; 
            }
            ICollection list = Grid.DataSource as ICollection;
            OwnerForm.ShowInfo(list?.Count);
        }

        internal void ShowInfo(ColumnView view)
        {
            if (OwnerForm == null) return;
            //ShowReminder();
            OwnerForm.ShowInfo(view.DataRowCount);
        }

        internal virtual void InitializeModule(IDXMenuManager manager, object data)
        {
            SetMenuManager(this.Controls, manager);
            if (Grid != null && Grid.MainView is ColumnView)
            {
                ((ColumnView)Grid.MainView).ColumnFilterChanged 
                    += new EventHandler(BaseModule_ColumnFilterChanged);
            }
            CapitalizeChildRibbonPages();
        }

        protected internal virtual void ButtonClick(string tag) { }
        protected internal virtual void SendKeyDown(KeyEventArgs e) { }
        #endregion

        #region EVENTS
        private void BaseModule_ColumnFilterChanged(object sender, EventArgs e)
        {
            ShowInfo(sender as ColumnView);
        }
        #endregion
    }
}
