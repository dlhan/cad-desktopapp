﻿
namespace Cad.Clients.WinForm.Modules
{
    partial class DispatchModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DispatchModule));
            this.pnContainer = new System.Windows.Forms.TableLayoutPanel();
            this.pnHeader = new System.Windows.Forms.TableLayoutPanel();
            this.pnClock = new System.Windows.Forms.TableLayoutPanel();
            this.lblCurrentDate = new DevExpress.XtraEditors.LabelControl();
            this.lblCurrentTime = new DevExpress.XtraEditors.LabelControl();
            this.ucConsole = new Cad.Clients.WinForm.Controls.ConsoleIndicator();
            this.pnContent = new System.Windows.Forms.TableLayoutPanel();
            this.pnDispatch = new System.Windows.Forms.TableLayoutPanel();
            this.ucCallPanel = new Cad.Clients.WinForm.Controls.CallPanel();
            this.ucIncidentPanel = new Cad.Clients.WinForm.Controls.IncidentPanel();
            this.ucDispatchNotes = new Cad.Clients.WinForm.Controls.DispatchNotePanel();
            this.ucDispatchUnits = new Cad.Clients.WinForm.Controls.DispatchUnitPanel();
            this.pnContainer.SuspendLayout();
            this.pnHeader.SuspendLayout();
            this.pnClock.SuspendLayout();
            this.pnContent.SuspendLayout();
            this.pnDispatch.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            resources.ApplyResources(this.pnContainer, "pnContainer");
            this.pnContainer.Controls.Add(this.pnHeader, 1, 0);
            this.pnContainer.Controls.Add(this.pnContent, 1, 1);
            this.pnContainer.Name = "pnContainer";
            // 
            // pnHeader
            // 
            resources.ApplyResources(this.pnHeader, "pnHeader");
            this.pnHeader.Controls.Add(this.pnClock, 2, 0);
            this.pnHeader.Controls.Add(this.ucConsole, 0, 0);
            this.pnHeader.Name = "pnHeader";
            // 
            // pnClock
            // 
            resources.ApplyResources(this.pnClock, "pnClock");
            this.pnClock.Controls.Add(this.lblCurrentDate, 0, 0);
            this.pnClock.Controls.Add(this.lblCurrentTime, 0, 1);
            this.pnClock.Name = "pnClock";
            // 
            // lblCurrentDate
            // 
            this.lblCurrentDate.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblCurrentDate.Appearance.Font")));
            this.lblCurrentDate.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(200)))), ((int)(((byte)(170)))));
            this.lblCurrentDate.Appearance.Options.UseFont = true;
            this.lblCurrentDate.Appearance.Options.UseForeColor = true;
            this.lblCurrentDate.Appearance.Options.UseTextOptions = true;
            this.lblCurrentDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            resources.ApplyResources(this.lblCurrentDate, "lblCurrentDate");
            this.lblCurrentDate.Name = "lblCurrentDate";
            // 
            // lblCurrentTime
            // 
            this.lblCurrentTime.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblCurrentTime.Appearance.Font")));
            this.lblCurrentTime.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(187)))), ((int)(((byte)(151)))));
            this.lblCurrentTime.Appearance.Options.UseFont = true;
            this.lblCurrentTime.Appearance.Options.UseForeColor = true;
            this.lblCurrentTime.Appearance.Options.UseTextOptions = true;
            this.lblCurrentTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblCurrentTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            resources.ApplyResources(this.lblCurrentTime, "lblCurrentTime");
            this.lblCurrentTime.Name = "lblCurrentTime";
            // 
            // ucConsole
            // 
            resources.ApplyResources(this.ucConsole, "ucConsole");
            this.ucConsole.ConsoleId = "1";
            this.ucConsole.ConsoleImage = ((System.Drawing.Image)(resources.GetObject("ucConsole.ConsoleImage")));
            this.ucConsole.FontSize = 18F;
            this.ucConsole.Name = "ucConsole";
            this.ucConsole.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(200)))), ((int)(((byte)(170)))));
            // 
            // pnContent
            // 
            resources.ApplyResources(this.pnContent, "pnContent");
            this.pnContent.Controls.Add(this.pnDispatch, 0, 0);
            this.pnContent.Controls.Add(this.ucDispatchNotes, 2, 0);
            this.pnContent.Controls.Add(this.ucDispatchUnits, 4, 0);
            this.pnContent.Name = "pnContent";
            // 
            // pnDispatch
            // 
            resources.ApplyResources(this.pnDispatch, "pnDispatch");
            this.pnDispatch.Controls.Add(this.ucCallPanel, 0, 0);
            this.pnDispatch.Controls.Add(this.ucIncidentPanel, 0, 2);
            this.pnDispatch.Name = "pnDispatch";
            // 
            // ucCallPanel
            // 
            this.ucCallPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ucCallPanel.Call = null;
            resources.ApplyResources(this.ucCallPanel, "ucCallPanel");
            this.ucCallPanel.Name = "ucCallPanel";
            // 
            // ucIncidentPanel
            // 
            this.ucIncidentPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ucIncidentPanel.CurrentIncident = null;
            resources.ApplyResources(this.ucIncidentPanel, "ucIncidentPanel");
            this.ucIncidentPanel.Name = "ucIncidentPanel";
            // 
            // ucDispatchNotes
            // 
            this.ucDispatchNotes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            resources.ApplyResources(this.ucDispatchNotes, "ucDispatchNotes");
            this.ucDispatchNotes.Name = "ucDispatchNotes";
            // 
            // ucDispatchUnits
            // 
            this.ucDispatchUnits.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            resources.ApplyResources(this.ucDispatchUnits, "ucDispatchUnits");
            this.ucDispatchUnits.Name = "ucDispatchUnits";
            this.ucDispatchUnits.Squads = null;
            // 
            // DispatchModule
            // 
            this.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("DispatchModule.Appearance.Font")));
            this.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Appearance.Options.UseFont = true;
            this.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnContainer);
            this.Name = "DispatchModule";
            this.pnContainer.ResumeLayout(false);
            this.pnHeader.ResumeLayout(false);
            this.pnClock.ResumeLayout(false);
            this.pnClock.PerformLayout();
            this.pnContent.ResumeLayout(false);
            this.pnDispatch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnContainer;
        private System.Windows.Forms.TableLayoutPanel pnHeader;
        private System.Windows.Forms.TableLayoutPanel pnClock;
        private DevExpress.XtraEditors.LabelControl lblCurrentDate;
        private DevExpress.XtraEditors.LabelControl lblCurrentTime;
        private Controls.ConsoleIndicator ucConsole;
        private System.Windows.Forms.TableLayoutPanel pnContent;
        private System.Windows.Forms.TableLayoutPanel pnDispatch;
        private Controls.CallPanel ucCallPanel;
        private Controls.IncidentPanel ucIncidentPanel;
        private Controls.DispatchNotePanel ucDispatchNotes;
        private Controls.DispatchUnitPanel ucDispatchUnits;
    }
}
