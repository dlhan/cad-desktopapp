﻿//
// Cad.Clients.WinForm.Modules.DispatchModule
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.Utils.Menu;
using Cad.Equipments.Cti;
using Cad.Clients.WinForm.Forms;

namespace Cad.Clients.WinForm.Modules
{
    public partial class DispatchModule : BaseModule
    {
        #region VARIABLES
        private const int CLOCK_TIMER_INTERVAL = 1000;
        private const string TIME_FORMAT = "{0:HH:mm:ss}";
        private Timer tmClock;
        #endregion

        #region PROPERTIES
        public override string ModuleName
        {
            get 
            {
                return Properties.Resources.DispatchModuleName;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public DispatchModule()
        {
            InitializeComponent();
        }
        #endregion

        #region METHODS
        private void EnableCtiState()
        {
            DispatchManager.EnableCti(
                DispatchManager.GetCtiState().HasFlag(AgentState.Disabled));

            ShowConsoleInfo();
        }

        private void ListeningRecord()
        {
            RecordingForm form = new RecordingForm();
            form.ShowDialog();
        }

        internal override void InitializeModule(IDXMenuManager manager, object data)
        {
            base.InitializeModule(manager, data);

            tmClock = new Timer();
            tmClock.Tick += new EventHandler(OnClockTimerEvent);
            tmClock.Interval = CLOCK_TIMER_INTERVAL;
            tmClock.Start();

            ucCallPanel.BindData();
            ucIncidentPanel.BindData();
            //ucDispatchNotes.BindData();
            //ucDispatchUnits.BindData();
        }

        internal override void ShowModule(bool firstShow)
        {
            base.ShowModule(firstShow);

            if (firstShow)
            {
                ShowConsoleInfo();
            }
        }
        #endregion

        #region TIMER EVENTS
        private void OnClockTimerEvent(object sender, EventArgs args)
        {
            string dtNow = DateTime.UtcNow.ToString("D", 
                CultureInfo.CreateSpecificCulture(AppContext.Culture));
            if (!dtNow.Equals(lblCurrentDate.Text))
                lblCurrentDate.Text = dtNow;
            lblCurrentTime.Text = string.Format(TIME_FORMAT, DateTime.Now);
        }
        #endregion

        #region CONTROL EVENTS
        protected internal override void ButtonClick(string tag)
        {
            base.ButtonClick(tag);

            switch (tag)
            {
                case Resources.CallEnableTag: EnableCtiState(); break;
                case Resources.NoteNewTag: ucDispatchNotes.AddNewNote(); break;
                case Resources.NoteEditTag: ucDispatchNotes.EditSelectedNote(); break;
                case Resources.NoteDeleteTag: ucDispatchNotes.DeleteSelectedNote(); break;
                case Resources.RecordListeningTag: ListeningRecord();  break;
                default: break;
            }
        }
        #endregion

        #region FORM EVENTS
        #endregion
    }
}
