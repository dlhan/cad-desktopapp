﻿
namespace Cad.Clients.WinForm.Modules
{
    partial class IncidentModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IncidentModule));
            this.pnRoot = new System.Windows.Forms.TableLayoutPanel();
            this.gcIncidents = new DevExpress.XtraGrid.GridControl();
            this.gvIncidents = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubcategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCall = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOpenedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIncidents)).BeginInit();
            this.SuspendLayout();
            // 
            // pnRoot
            // 
            resources.ApplyResources(this.pnRoot, "pnRoot");
            this.pnRoot.Controls.Add(this.gcIncidents, 0, 0);
            this.pnRoot.Name = "pnRoot";
            // 
            // gcIncidents
            // 
            resources.ApplyResources(this.gcIncidents, "gcIncidents");
            this.gcIncidents.MainView = this.gvIncidents;
            this.gcIncidents.Name = "gcIncidents";
            this.gcIncidents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvIncidents});
            // 
            // gvIncidents
            // 
            this.gvIncidents.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colCategory,
            this.colSubcategory,
            this.colScale,
            this.colCall,
            this.colLocation,
            this.colOpenedDate,
            this.colClosedDate,
            this.colCreatedDate,
            this.colModifiedDate});
            this.gvIncidents.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gvIncidents.GridControl = this.gcIncidents;
            this.gvIncidents.Name = "gvIncidents";
            this.gvIncidents.OptionsBehavior.Editable = false;
            this.gvIncidents.OptionsCustomization.AllowGroup = false;
            this.gvIncidents.OptionsDetail.EnableMasterViewMode = false;
            this.gvIncidents.OptionsView.ShowGroupPanel = false;
            this.gvIncidents.OptionsView.ShowIndicator = false;
            // 
            // colId
            // 
            resources.ApplyResources(this.colId, "colId");
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colCategory
            // 
            resources.ApplyResources(this.colCategory, "colCategory");
            this.colCategory.FieldName = "Category.Id";
            this.colCategory.MaxWidth = 100;
            this.colCategory.MinWidth = 50;
            this.colCategory.Name = "colCategory";
            // 
            // colSubcategory
            // 
            resources.ApplyResources(this.colSubcategory, "colSubcategory");
            this.colSubcategory.FieldName = "Subcategory.Name";
            this.colSubcategory.MaxWidth = 120;
            this.colSubcategory.MinWidth = 50;
            this.colSubcategory.Name = "colSubcategory";
            // 
            // colScale
            // 
            resources.ApplyResources(this.colScale, "colScale");
            this.colScale.FieldName = "Scale.Name";
            this.colScale.MaxWidth = 100;
            this.colScale.MinWidth = 50;
            this.colScale.Name = "colScale";
            // 
            // colCall
            // 
            resources.ApplyResources(this.colCall, "colCall");
            this.colCall.FieldName = "Call.CallNumber.PhoneNumber";
            this.colCall.MaxWidth = 100;
            this.colCall.MinWidth = 50;
            this.colCall.Name = "colCall";
            // 
            // colLocation
            // 
            resources.ApplyResources(this.colLocation, "colLocation");
            this.colLocation.FieldName = "Location.AddressLine1";
            this.colLocation.MinWidth = 50;
            this.colLocation.Name = "colLocation";
            // 
            // colOpenedDate
            // 
            resources.ApplyResources(this.colOpenedDate, "colOpenedDate");
            this.colOpenedDate.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.colOpenedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colOpenedDate.FieldName = "OpenedDate";
            this.colOpenedDate.MaxWidth = 120;
            this.colOpenedDate.MinWidth = 50;
            this.colOpenedDate.Name = "colOpenedDate";
            // 
            // colClosedDate
            // 
            resources.ApplyResources(this.colClosedDate, "colClosedDate");
            this.colClosedDate.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.colClosedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colClosedDate.FieldName = "ClosedDate";
            this.colClosedDate.MaxWidth = 120;
            this.colClosedDate.MinWidth = 50;
            this.colClosedDate.Name = "colClosedDate";
            // 
            // colCreatedDate
            // 
            resources.ApplyResources(this.colCreatedDate, "colCreatedDate");
            this.colCreatedDate.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.colCreatedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCreatedDate.FieldName = "CreatedDate";
            this.colCreatedDate.MaxWidth = 120;
            this.colCreatedDate.MinWidth = 50;
            this.colCreatedDate.Name = "colCreatedDate";
            // 
            // colModifiedDate
            // 
            resources.ApplyResources(this.colModifiedDate, "colModifiedDate");
            this.colModifiedDate.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.colModifiedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colModifiedDate.FieldName = "ModifiedDate";
            this.colModifiedDate.MaxWidth = 120;
            this.colModifiedDate.MinWidth = 50;
            this.colModifiedDate.Name = "colModifiedDate";
            // 
            // IncidentModule
            // 
            this.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("IncidentModule.Appearance.Font")));
            this.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Appearance.Options.UseFont = true;
            this.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnRoot);
            this.Name = "IncidentModule";
            this.pnRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIncidents)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnRoot;
        private DevExpress.XtraGrid.GridControl gcIncidents;
        private DevExpress.XtraGrid.Views.Grid.GridView gvIncidents;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colSubcategory;
        private DevExpress.XtraGrid.Columns.GridColumn colScale;
        private DevExpress.XtraGrid.Columns.GridColumn colOpenedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colCall;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
    }
}
