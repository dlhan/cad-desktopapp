﻿//
// Cad.Clients.WinForm.Modules.DispatchModule
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using Cad.Clients.WinForm.Forms;
using Cad.Clients.WinForm.Helper;
using Cad.Data.BLL.Dispatch;

namespace Cad.Clients.WinForm.Modules
{
    public partial class IncidentModule : BaseModule
    {
        #region VARIABLES
        #endregion

        #region PROPERTIES
        private Incident CurrentIncident
        {
            get
            {
                if (gvIncidents.FocusedRowHandle < 0) return null;
                return gvIncidents.GetRow(gvIncidents.FocusedRowHandle) as Incident;
            }
        }
        
        protected override GridControl Grid { get { return gcIncidents; } }

        public override string ModuleName
        {
            get
            {
                return Properties.Resources.IncidentModuleName;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public IncidentModule()
        {
            InitializeComponent();
        }
        #endregion

        #region PRIVATE METHODS
        public void BindData()
        {
            gcIncidents.DataSource = Incident.GetOpenedIncidents();
            Console.WriteLine("Incident grid data binding completed.");
            ShowInfo(gvIncidents);
        }

        private void EditIncident(Incident incident)
        {
            EditIncidentForm form = new EditIncidentForm(this);
            form.Incident = incident;
            form.BindData();
            form.Show();
        }
        #endregion

        #region OVERRIDE METHODS
        internal override void InitializeModule(IDXMenuManager manager, object data)
        {
            base.InitializeModule(manager, data);

            gcIncidents.RepositoryItems.Add(
                RepositoryItemHelper.CreateIncidentCategoryImageComboBox(
                    "riicbIncidentCategory"));
            colCategory.ColumnEdit = gcIncidents.RepositoryItems["riicbIncidentCategory"];
            gvIncidents.KeyDown += gvIncidents_KeyDown;
            gvIncidents.RowCellClick += gvIncidents_RowCellClick;

            BindData();
        }

        internal override void ShowModule(bool firstShow)
        {
            base.ShowModule(firstShow);
        }
        #endregion

        #region CONTROL EVENTS
        protected internal override void ButtonClick(string tag)
        {
            base.ButtonClick(tag);

            switch (tag)
            {
                case Resources.IncidentNewTag: 
                    EditIncident(null); 
                    break;
                case Resources.IncidentEditTag: 
                    EditIncident(CurrentIncident);
                    break;
                default: break;
            }
        }

        private void gvIncidents_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && gvIncidents.FocusedRowHandle >= 0)
                EditIncident(CurrentIncident);
        }
        private void gvIncidents_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowHandle >= 0 && e.Clicks == 2)
                EditIncident(CurrentIncident);
        }
        #endregion
    }
}
