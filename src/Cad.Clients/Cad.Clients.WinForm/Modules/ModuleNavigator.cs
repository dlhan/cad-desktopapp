﻿//
// Cad.Clients.WinForm.Modules.ModuleNavigator
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using DevExpress.XtraSplashScreen;

namespace Cad.Clients.WinForm.Modules
{
    /// <summary>
    /// Navigation job modules.
    /// </summary>
    public class ModuleNavigator
    {
        #region VARIABLES
        internal RibbonControl _ribbon;
        internal PanelControl _panel;
        internal string _currentModuleName = string.Empty;
        #endregion

        #region PROPERTIES
        public BaseModule CurrentModule
        {
            get
            {
                if (_panel.Controls.Count == 0)
                    return null;
                return _panel.Controls[0] as BaseModule;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public ModuleNavigator(RibbonControl ribbon, PanelControl panel)
        {
            _ribbon = ribbon;
            _panel = panel;
        }
        #endregion

        #region METHODS
        private void ChangeRibbon(
            NavBarGroupTagObject groupObject, 
            RibbonControl ribbon)
        {
            ribbon.ColorScheme = groupObject.RibbonColorSchema;
            foreach (RibbonPage page in GetDeferredPageToShow(groupObject, ribbon))
                page.Visible = true;

            foreach (RibbonPage page in ribbon.Pages)
            {
                if (page.Visible)
                {
                    ribbon.SelectedPage = page;
                    break;
                }
            }
        }

        private void ChangeModule(
            NavBarGroupTagObject groupObject,
            PanelControl panel)
        {
            if (groupObject.Module == null) return;

            if (panel.Controls.Count > 0)
            {
                if (panel.Controls[0] is BaseModule currentModule)
                    currentModule.HideModule();
            }
            panel.Controls.Clear();
            panel.Controls.Add(groupObject.Module);
            groupObject.Module.Dock = DockStyle.Fill;
            groupObject.Module.ShowModule(IsFirstShow(groupObject));
        }

        private List<RibbonPage> GetDeferredPageToShow(
            NavBarGroupTagObject groupObject, 
            RibbonControl ribbon)
        {
            bool allowSetVisiblePage = true;
            List<RibbonPage> results = new List<RibbonPage>();
            foreach (RibbonPage page in ribbon.Pages)
            {
                if (!string.IsNullOrEmpty($"{page.Tag}"))
                {
                    bool isPageVisible = groupObject.Name.Equals(page.Tag);
                    if (isPageVisible != page.Visible && isPageVisible)
                        results.Add(page);
                    else
                        page.Visible = isPageVisible;
                }
                if (page.Visible && allowSetVisiblePage)
                {
                    // page.Text = "Home";
                    ribbon.SelectedPage = page;
                    allowSetVisiblePage = false;
                }
            }
            return results;
        }

        private void InitializeModule(
            NavBarGroupTagObject groupObject,
            RibbonControl ribbon,
            object moduleData, 
            string caption)
        {
            if (!IsFirstShow(groupObject)) return;

            OpenSplashScreen();

            ConstructorInfo ci = groupObject
                .ModuleType
                .GetConstructor(Type.EmptyTypes);
            if (ci != null)
            {
                try
                {
                    groupObject.Module = ci.Invoke(null) as BaseModule;
                    groupObject.Module.InitializeModule(ribbon, moduleData);
                    _currentModuleName = caption;
                }
                catch (Exception e)
                {
                    var entryAsm = Assembly.GetEntryAssembly();
                    throw new ApplicationException(
                        $"Error on Showing Module: {caption}\r\n"
                        + $"PrevModule: {_currentModuleName}\r\n"
                        + $"StartUp: {(entryAsm != null ? entryAsm.Location : string.Empty)}",
                        e);
                }
            }

            CloseSplashScreen(moduleData);
        }

        private bool IsFirstShow(NavBarGroupTagObject groupObject)
        {
            return groupObject.Module == null;
        }

        private void OpenSplashScreen()
        {
            if (SplashScreenManager.Default == null)
            {
                SplashScreenManager.ShowForm(
                    _ribbon.FindForm(),
                    typeof(Forms.SplashScreenForm),
                    false, true);
            }
        }

        private void CloseSplashScreen(object moduleData)
        {
            if (SplashScreenManager.Default != null)
            {
                if (moduleData is Form form)
                {
                    if (SplashScreenManager.FormInPendingState)
                        SplashScreenManager.CloseForm();
                    else
                        SplashScreenManager.CloseForm(false, 500, form);
                }
                else
                    SplashScreenManager.CloseForm();
            }
        }

        public void ChangeSelectedItem(NavBarItemLink link, object moduleData)
        {
            if (!(link.Item.Tag is NavBarGroupTagObject groupObject)) return;
            
            InitializeModule(groupObject, _ribbon, moduleData, link.Caption);
            ChangeRibbon(groupObject, _ribbon);
            ChangeModule(groupObject, _panel);
        }
        #endregion
    }
}
