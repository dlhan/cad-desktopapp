﻿//
// Cad.Clients.WinForm.Modules.NavBarGroupTagObject
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using DevExpress.XtraBars.Ribbon;

namespace Cad.Clients.WinForm.Modules
{
    public class NavBarGroupTagObject
    {
        #region PROPERTIES
        internal string name;
        public string Name { get { return name; } }

        internal Type moduleType;
        public Type ModuleType { get { return moduleType; } }

        internal BaseModule module;
        public BaseModule Module
        {
            get { return module; }
            set { module = value; }
        }

        internal RibbonControlColorScheme ribbonColorSchema =
            RibbonControlColorScheme.Default;
        public RibbonControlColorScheme RibbonColorSchema
        {
            get { return ribbonColorSchema; }
        }
        #endregion

        #region CONSTRUCTORS
        public NavBarGroupTagObject(string name, Type moduletype)
            : this(name, moduletype, RibbonControlColorScheme.Default)
        {
        }

        public NavBarGroupTagObject(string name, Type moduleType,
            RibbonControlColorScheme ribbonScheme)
        {
            this.name = name;
            this.moduleType = moduleType;
            this.ribbonColorSchema = ribbonScheme;
            module = null;
        }
        #endregion
    }
}
