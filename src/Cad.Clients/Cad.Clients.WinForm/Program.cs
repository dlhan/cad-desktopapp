﻿//
// Cad.Clients.WinForm.Program
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.XtraSplashScreen;

namespace Cad.Clients.WinForm
{
    static class Program
    {
        #region MAIN
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] arguments)
        {
            // For single instance.
            var mutex = new Mutex(true, AppContext.AppName, out bool result);
            if (!result)
            {
                MessageBox.Show("Another instance is already runngin.");
                return;
            }

            InitializeCulture(AppContext.Culture);
            InitializeDevExpress(arguments);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!ValidateUser()) return;
            InitializeOthers();
            Application.Run(new MainForm());

            // For single instance
            GC.KeepAlive(mutex);
        }
        #endregion

        #region METHODS
        static private void InitializeCulture(string culture)
        {
            CultureInfo ci = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        static private void InitializeDevExpress(string[] arguments)
        {
            DevExpress.Utils.LocalizationHelper.SetCurrentCulture(arguments);
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.Utils.AppearanceObject.DefaultFont =
                new Font("Segoe UI", 9);
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(
                //"Office 2019 Colorful", "Amber");
                //"Office 2019 Colorful", "Forest");
                "Office 2016 Colorful");
            SkinManager.EnableFormSkins();
        }

        static private bool ValidateUser()
        {
            Forms.LoginForm form = new Forms.LoginForm();
            return form.ShowDialog() == DialogResult.OK;
        }

        static private void InitializeOthers()
        {
            // Show splash screen
            SplashScreenManager.ShowSkinSplashScreen(
                logoImage: Properties.Resources.Logo,
                title: AppContext.AppName,
                subtitle: AppContext.AppVersion,
                footer: "Copyright @ 2021 Incon." + Environment.NewLine + "All rights reserved.",
                loading: "Download system codes...");

            // TODO: replace sleep() to application initializing code.
            Thread.Sleep(2000);
            SplashScreenManager.Default.SendCommand(
                SkinSplashScreenCommand.UpdateLoadingText, "Download success.");
            Thread.Sleep(1000);
        }
        #endregion
    }
}
