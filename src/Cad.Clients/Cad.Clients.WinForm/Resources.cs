﻿//
// Cad.Clients.WinForm.Resources
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Clients.WinForm
{
    public static class Resources
    {
        #region STRING TEMPLATES
        internal const string DateTimeFullFormat = @"yyyy-MM-dd HH:mm:ss";
        internal const string TimeHrMinFormat = "HH:mm";
        internal const string TimeHrMinSecFormat = "HH:mm:ss";
        #endregion

        #region TAGS OF MAIN RIBBON
        internal const string CallEnableTag = "EnableCall";
        internal const string CallAnswerTag = "AnswerCall";
        internal const string CallHanupTag = "HangupCall";
        internal const string CallHoldTag = "HoldCall";
        internal const string CallTransferTag = "TransferCall";
        internal const string CallConferenceTag = "ConferenceCall";
        internal const string CallDialupTag = "DialupCall";
        internal const string RequestAliTag = "RequestAli";
        internal const string CallDispositionTag = "DispositionCall";
        internal const string DispositionAnnounceTag = "AnnounceDisposition";
        internal const string DispositionNotResponseTag = "NotResponseDisposition";
        internal const string DispositionPrankTag = "PrankDisposition";
        internal const string DispositionWrongNumberTag = "WrongNumberDisposition";
        internal const string DispositionTestCallTag = "TestCallDisposition";
        internal const string DispositionIncidentCallTag = "IncidentCallDisposition";
        
        internal const string SquadBuildTag = "BuildSquad";
        internal const string UnitDispatchTag = "DispatchUnit";

        internal const string NoteNewTag = "NewNote";
        internal const string NoteEditTag = "EditNote";
        internal const string NoteDeleteTag = "DeleteNote";
        internal const string RecordListeningTag = "ListeningRecord";

        internal const string IncidentNewTag = "NewIncident";
        internal const string IncidentEditTag = "EditIncident";
        #endregion

        #region TAGS OF INCIDENT(FORM) RIBBON
        internal const string CancelTag = "Cancel";
        internal const string SaveTag = "Save";
        internal const string SaveAndCloseTag = "SaveAndClose";
        internal const string SaveAndNewTag = "SaveAndNew";
        #endregion
    }

    public static class Colors
    {
        /// <summary>
        /// HEX: #000000
        /// RGB: 0, 0, 0
        /// CMYK: NaN, Nan, Nan, 100
        /// </summary>
        public static System.Drawing.Color Black { get; } = System.Drawing.Color.FromArgb(255, 0, 0, 0);
        /// <summary>
        /// HEX: #8bc8aa
        /// RGB: 139, 200, 170
        /// CMYK: 31, 0, 15, 22
        /// </summary>
        public static System.Drawing.Color VistaBlue { get; } = System.Drawing.Color.FromArgb(255, 139, 200, 170);
        /// <summary>
        /// HEX: #6abb97
        /// RGB: 106, 187, 151
        /// CMYK: 43, 0, 19, 27
        /// </summary>
        public static System.Drawing.Color SilverTree { get; } = System.Drawing.Color.FromArgb(255, 106, 187, 151);
        /// <summary>
        /// HEX: #39a87b
        /// RGB: 57, 168, 123
        /// CMKY: 66, 0, 27, 34
        /// </summary>
        public static System.Drawing.Color OceanGreen { get; } = System.Drawing.Color.FromArgb(255, 57, 168, 123);
        /// <summary>
        /// HEX: #008755
        /// RGB: 0, 135, 85
        /// CMKY: 100, 0, 37, 47
        /// </summary>
        public static System.Drawing.Color DeepSea { get; } = System.Drawing.Color.FromArgb(255, 0, 135, 85);
        /// <summary>
        /// HEX: #007348
        /// RGB: 0, 115, 72
        /// CMKY: 100, 0, 37, 55
        /// </summary>
        public static System.Drawing.Color FunGreen { get; } = System.Drawing.Color.FromArgb(255, 0, 115, 72);
    }
}
