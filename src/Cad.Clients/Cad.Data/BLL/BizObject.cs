﻿//
// Cad.Data.BLL.BizObject
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.BLL
{
    public abstract class BizObject
    {
        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        public BizObject() {}
        #endregion

        #region PROTECTED METHODS
        #endregion
    }
}
