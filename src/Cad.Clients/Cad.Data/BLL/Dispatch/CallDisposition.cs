﻿//
// Cad.Data.BLL.Dispatch.CallDisposition
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class CallDisposition : BizObject
    {
        #region PROPERTIES
        public short Id { get; set; }
        public string Name { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallDisposition() {}
        #endregion

        #region PRIVATE METHODS
        private static CallDisposition GetCallDispositionFromEntity(
            CallDispositionEntity record)
        {
            if (record == null) return null;

            return new CallDisposition()
            {
                Id = record.Id,
                Name = record.Name,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<CallDisposition> GetCallDispositionsFromEntities(
            List<CallDispositionEntity> recordSet)
        {
            List<CallDisposition> result = new List<CallDisposition>();
            foreach (CallDispositionEntity record in recordSet)
                result.Add(GetCallDispositionFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC  METHODS
        public static CallDisposition GetCallDisposition(short id)
        {
            return GetCallDispositionFromEntity(
                SiteProvider.Dispatch.GetCallDisposition(id));
        }

        public static List<CallDisposition> GetCallDispositions()
        {
            return GetCallDispositionsFromEntities(
                SiteProvider.Dispatch.GetCallDispositions());
        }
        #endregion
    }
}
