﻿//
// Cad.Data.BLL.Dispatch.CallLocation
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using Microsoft.Spatial;
using System;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class CallLocation : BizObject
    {
        #region PROPERTIES
        public long ReportCallId { get; set; }
        public Person.City City { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public GeographyPoint SpatialLocation { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallLocation() {}
        #endregion

        #region PRIVATE METHODS
        private static CallLocation GetCallLocationFromEntity(
            CallLocationEntity record)
        {
            if (record == null) return null;

            return new CallLocation()
            {
                ReportCallId = record.ReportCallId,
                City = Person.City.GetCity(record.CityCode),
                AddressLine1 = record.AddressLine1,
                AddressLine2 = record.AddressLine2,
                PostalCode = record.PostalCode,
                SpatialLocation = record.SpatialLocation,
                ModifiedDate = record.ModifiedDate
            };
        }
        #endregion

        #region STATIC METHODS
        public static CallLocation GetCallLocation(long reportCallId)
        {
            return GetCallLocationFromEntity(
                SiteProvider.Dispatch.GetCallLocation(reportCallId));
        }
        #endregion
    }
}
