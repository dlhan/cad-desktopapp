﻿//
// Cad.Data.BLL.Dispatch.CallNumberStatus
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class CallNumberStatus : BizObject
    {
        #region PROPERTIES
        public short Id { get; set; }
        public string Name { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallNumberStatus() {}
        #endregion

        #region PRIVATE METHODS
        private static CallNumberStatus GetCallNumberStatusFromEntity(
            CallNumberStatusEntity record)
        {
            if (record == null) return null;

            return new CallNumberStatus()
            {
                Id = record.Id,
                Name = record.Name,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<CallNumberStatus>
            GetCallNumberStatusesFromEntities(List<CallNumberStatusEntity> recordSet)
        {
            List<CallNumberStatus> result = new List<CallNumberStatus>();
            foreach (CallNumberStatusEntity record in recordSet)
                result.Add(GetCallNumberStatusFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static CallNumberStatus GetCallNumberStatus(short id)
        {
            return GetCallNumberStatusFromEntity(
                SiteProvider.Dispatch.GetCallNumberStatus(id));
        }

        public static List<CallNumberStatus> GetCallNumberStatuses()
        {
            return GetCallNumberStatusesFromEntities(
                SiteProvider.Dispatch.GetCallNumberStatuses());
        }
        #endregion
    }
}
