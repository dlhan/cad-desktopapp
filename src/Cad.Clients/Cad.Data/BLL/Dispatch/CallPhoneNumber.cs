﻿//
// Cad.Data.BLL.Dispatch.CallPhoneNumber
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class CallPhoneNumber : BizObject
    {
        #region PROPERTIES
        public long Id { get; set; }
        public string PhoneNumber { get; set; }
        public CallNumberStatus NumberStatus { get; set; }
        public short NumOfCalls { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallPhoneNumber() {}
        #endregion

        #region PRIVATE METHODS
        private static CallPhoneNumber GetCallPhoneNumberFromEntity(
            CallPhoneNumberEntity record)
        {
            if (record == null) return null;

            return new CallPhoneNumber()
            {
                Id = record.Id,
                PhoneNumber = record.PhoneNumber,
                NumberStatus = CallNumberStatus.GetCallNumberStatus(
                            record.CallNumberStatusId),
                NumOfCalls = record.NumOfCalls,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<CallPhoneNumber> GetCallPhoneNumbersFromEntities(
            List<CallPhoneNumberEntity> recordSet)
        {
            List<CallPhoneNumber> result = new List<CallPhoneNumber>();
            foreach (CallPhoneNumberEntity record in recordSet)
                result.Add(GetCallPhoneNumberFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static CallPhoneNumber GetCallPhoneNumber(long id)
        {
            return GetCallPhoneNumberFromEntity(
                SiteProvider.Dispatch.GetCallPhoneNumber(id));
        }
        #endregion
    }
}
