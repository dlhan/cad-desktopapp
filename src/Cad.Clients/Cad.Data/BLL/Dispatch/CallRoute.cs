﻿//
// Cad.Data.BLL.Dispatch.CallRoute
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class CallRoute : BizObject
    {
        #region PROPERTIES
        public int Id { get; set; }
        public string Name { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallRoute() {}
        #endregion

        #region PRIVATE METHODS
        private static CallRoute GetCallRouteFromEntity(
            CallRouteEntity record)
        {
            if (record == null) return null;

            return new CallRoute()
            {
                Id = record.Id,
                Name = record.Name,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<CallRoute> GetCallRoutesFromEntities(
            List<CallRouteEntity> recordSet)
        {
            List<CallRoute> result = new List<CallRoute>();
            foreach (CallRouteEntity record in recordSet)
                result.Add(GetCallRouteFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static CallRoute GetCallRoute(short id)
        {
            return GetCallRouteFromEntity(
                SiteProvider.Dispatch.GetCallRoute(id));
        }

        public static List<CallRoute> GetCallRoutes()
        {
            return GetCallRoutesFromEntities(
                SiteProvider.Dispatch.GetCallRoutes());
        }
        #endregion
    }
}
