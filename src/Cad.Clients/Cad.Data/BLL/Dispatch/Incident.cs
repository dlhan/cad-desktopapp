﻿//
// Cad.Data.BLL.Dispatch.Incident
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class Incident : BizObject
    {
        #region PROPERTIES
        public long Id { get; set; }
        public IncidentCategory Category { get; set; }
        public IncidentSubcategory Subcategory { get; set; }
        public IncidentScale Scale { get; set; }
        public string Synopsys { get; set; }
        public ReportCall Call { get; set; }
        public IncidentLocation Location { get; set; }
        public List<Squad> Squads { get; set; }
        public DateTime OpenedDate { get; set; }
        public DateTime ClosedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Incident() { }
        #endregion

        #region PRIVATE METHODS
        private static Incident GetIncidentFromEntity(IncidentEntity record)
        {
            if (record == null) return null;

            return new Incident()
            {
                Id = record.Id,
                Category = IncidentCategory
                    .GetIncidentCategoryBySubcategory(record.IncidentSubcategoryId),
                Subcategory = IncidentSubcategory
                    .GetIncidentSubcategory(record.IncidentSubcategoryId),
                Scale = IncidentScale.GetIncidentScale(record.IncidentScaleId),
                Synopsys = record.Synopsys,
                Call = ReportCall.GetReportCallByIncident(record.Id),
                Location = IncidentLocation.GetIncidentLocation(record.Id),
                Squads = Squad.GetSquadsByIncident(record.Id),
                OpenedDate = record.OpenedDate,
                ClosedDate = record.ClosedDate,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<Incident> 
            GetIncidentsFromEntities(List<IncidentEntity> recordSet)
        {
            List<Incident> result = new List<Incident>();
            foreach (IncidentEntity record in recordSet)
                result.Add(GetIncidentFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static Incident GetIncident(long id)
        {
            return GetIncidentFromEntity(
                SiteProvider.Dispatch.GetIncident(id));
        }

        public static List<Incident> GetOpenedIncidents()
        {
            return GetIncidentsFromEntities(
                SiteProvider.Dispatch.GetOpenedIncidents());
        }
        #endregion
    }
}
