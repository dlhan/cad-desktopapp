﻿//
// Cad.Data.BLL.Dispatch.IncidentCategory
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class IncidentCategory : BizObject
    {
        #region PROPERTIES
        public short Id { get; set; }
        public string Name { get; set; }
        public short ImageIndex { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentCategory() {}
        #endregion

        #region PRIVATE METHODS
        private static IncidentCategory GetIncidentCategoryFromEntity(
            IncidentCategoryEntity record)
        {
            if (record == null) return null;

            return new IncidentCategory()
            {
                Id = record.Id,
                Name = record.Name,
                ImageIndex = record.ImageIndex,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<IncidentCategory> GetIncidentCategoriesFromEntities(
            List<IncidentCategoryEntity> recordSet)
        {
            List<IncidentCategory> result = new List<IncidentCategory>();
            foreach (IncidentCategoryEntity record in recordSet)
                result.Add(GetIncidentCategoryFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static IncidentCategory
            GetIncidentCategoryBySubcategory(short subcategoryId)
        {
            return GetIncidentCategoryFromEntity(
                SiteProvider.Dispatch
                    .GetIncidentCategoryBySubcategory(subcategoryId));
        }

        public static IncidentCategory GetIncidentCategory(short id)
        {
            return GetIncidentCategoryFromEntity(
                SiteProvider.Dispatch.GetIncidentCategory(id));
        }

        public static List<IncidentCategory> GetIncidentCategories()
        {
            return GetIncidentCategoriesFromEntities(
                SiteProvider.Dispatch.GetIncidentCategories());
        }
        #endregion
    }
}
