﻿//
// Cad.Data.BLL.Dispatch.IncidentLocation
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using Microsoft.Spatial;
using System;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class IncidentLocation : BizObject
    {
        #region PROPERTIES
        public long IncidentId { get; set; }
        public Person.City City { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public GeographyPoint SpatialLocation { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentLocation() {}
        #endregion

        #region PRIVATE METHODS
        private static IncidentLocation GetIncidentLocationFromEntity(
            IncidentLocationEntity record)
        {
            if (record == null) return null;

            return new IncidentLocation()
            {
                IncidentId = record.IncidentId,
                City = Person.City.GetCity(record.CityCode),
                AddressLine1 = record.AddressLine1,
                AddressLine2 = record.AddressLine2,
                PostalCode = record.PostalCode,
                SpatialLocation = record.SpatialLocation,
                ModifiedDate = record.ModifiedDate
            };
        }
        #endregion

        #region STATIC METHODS
        public static IncidentLocation GetIncidentLocation(long incidentId)
        {
            return GetIncidentLocationFromEntity(
                SiteProvider.Dispatch.GetIncidentLocation(incidentId));
        }
        #endregion
    }
}
