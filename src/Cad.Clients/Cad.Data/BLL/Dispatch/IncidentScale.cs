﻿//
// Cad.Data.BLL.Dispatch.IncidentScale
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;


namespace Cad.Data.BLL.Dispatch
{
    public class IncidentScale : BizObject
    {
        #region PROPERTIES
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentScale() { }
        #endregion

        #region PRIVATE METHODS
        private static IncidentScale 
            GetIncidentScaleFromEntity(IncidentScaleEntity record)
        {
            if (record == null) return null;

            return new IncidentScale()
            {
                Id = record.Id,
                Name = record.Name,
                Description = record.Description,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<IncidentScale> 
            GetIncidentScalesFromEntities(List<IncidentScaleEntity> recordSet)
        {
            List<IncidentScale> result = new List<IncidentScale>();
            foreach (IncidentScaleEntity record in recordSet)
                result.Add(GetIncidentScaleFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static IncidentScale GetIncidentScale(short id)
        {
            return GetIncidentScaleFromEntity(
                SiteProvider.Dispatch.GetIncidentScale(id));
        }

        public static List<IncidentScale> GetIncidentScales()
        {
            return GetIncidentScalesFromEntities(
                SiteProvider.Dispatch.GetIncidentScales());
        }
        #endregion
    }
}
