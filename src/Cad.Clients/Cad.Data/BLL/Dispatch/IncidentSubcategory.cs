﻿//
// Cad.Data.BLL.Dispatch.IncidentSubcategory
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;


namespace Cad.Data.BLL.Dispatch
{
    public class IncidentSubcategory : BizObject
    {
        #region PROPERTIES
        public int Id { get; set; }
        public int IncidentCategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentSubcategory() { }
        #endregion

        #region PRIVATE METHODS
        private static IncidentSubcategory 
            GetIncidentSubcategoryFromEntity(
                IncidentSubcategoryEntity record)
        {
            if (record == null) return null;

            return new IncidentSubcategory()
            {
                Id = record.Id,
                IncidentCategoryID = record.IncidentCategoryID,
                Name = record.Name,
                Description = record.Description,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<IncidentSubcategory> 
            GetIncidentSubcategoriesFromEntities(
                List<IncidentSubcategoryEntity> recordSet)
        {
            List<IncidentSubcategory> result = new List<IncidentSubcategory>();
            foreach (IncidentSubcategoryEntity record in recordSet)
                result.Add(GetIncidentSubcategoryFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static IncidentSubcategory
            GetIncidentSubcategory(short id)
        {
            return GetIncidentSubcategoryFromEntity(
                SiteProvider.Dispatch.GetIncidentSubcategory(id));
        }

        public static List<IncidentSubcategory> 
            GetIncidentSubcategories(short id)
        {
            return GetIncidentSubcategoriesFromEntities(
                SiteProvider.Dispatch.GetIncidentSubcategories(id));
        }
        #endregion
    }
}
