﻿//
// Cad.Data.BLL.Dispatch.ReportCall
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.Linq;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class ReportCall : BizObject
    {
        #region PROPERTIES
        public long Id { get; set; }
        public long IncidentId { get; set; }
        public CallRoute Route { get; set; }
        public CallPhoneNumber CallNumber { get; set; }
        public string CallerName { get; set; }
        public CallDisposition Disposition { get; set; }
        public bool IsDuplicated { get; set; }
        public CallLocation Location { get; set; }
        public DateTime IncomingDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public ReportCall() {}
        #endregion

        #region PRIVATE METHODS
        private static ReportCall GetReportCallFromEntity(ReportCallEntity record)
        {
            if (record == null) return null;

            return new ReportCall()
            {
                Id = record.Id,
                IncidentId = record.IncidentId,
                Route = CallRoute.GetCallRoute(record.CallRouteId),
                CallNumber = CallPhoneNumber.GetCallPhoneNumber(
                            record.CallPhoneNumberId),
                CallerName = record.CallerName,
                Disposition = CallDisposition.GetCallDisposition(record.CallDispositionId),
                IsDuplicated = record.IsDuplicated,
                Location = CallLocation.GetCallLocation(record.Id),
                IncomingDate = record.IncomingDate,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<ReportCall> 
            GetReportCallsFromEitities(List<ReportCallEntity> recordSet)
        {
            List<ReportCall> result = new List<ReportCall>();
            foreach (ReportCallEntity record in recordSet)
                result.Add(GetReportCallFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static ReportCall GetReportCallByIncident(long incidentId)
        {
            //foreach (ReportCall call in GetReportCallsByIncident(incidentId))
            //    if (!call.IsDuplicated) return call;
            //return null;
            
            return GetReportCallsByIncident(incidentId)
                .Where(call => call.IncidentId == incidentId)
                .SingleOrDefault();
        }

        public static List<ReportCall> GetReportCallsByIncident(long incidentId, 
            bool isDuplicated = false)
        {
            return GetReportCallsFromEitities(
                SiteProvider.Dispatch.GetReportCallsByIncident(
                    incidentId, isDuplicated));
        }
        #endregion
    }
}
