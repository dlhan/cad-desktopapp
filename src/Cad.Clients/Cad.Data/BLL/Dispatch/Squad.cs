﻿//
// Cad.Data.BLL.Dispatch.Squad
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class Squad : BizObject
    {
        #region PROPERTIES
        public long Id { get; set; }
        public long IncidentId { get; set; }
        public short BuildGuideId { get; set; }
        public List<SquadUnit> Units { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Squad() {}
        #endregion

        #region PRIVATE METHODS
        private static Squad GetSquadFromEntity(
            SquadEntity record)
        {
            if (record == null) return null;

            return new Squad()
            {
                Id = record.Id,
                IncidentId = record.IncidentId,
                BuildGuideId = record.BuildGuideId,
                Units = SquadUnit.GetSquadUnitsBySquad(record.Id),
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<Squad> GetSquadsFromEntities(
            List<SquadEntity> recordSet)
        {
            List<Squad> result = new List<Squad>();
            foreach (SquadEntity record in recordSet)
                result.Add(GetSquadFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static Squad GetSquad(long id)
        {
            return GetSquadFromEntity(
                SiteProvider.Dispatch.GetSquad(id));
        }

        public static List<Squad> GetSquadsByIncident(long incidentId)
        {
            return GetSquadsFromEntities(
                SiteProvider.Dispatch.GetSquadsByIncident(incidentId));
        }
        #endregion
    }
}
