﻿//
// Cad.Data.BLL.Dispatch.SquadUnit
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class SquadUnit : BizObject
    {
        #region PROPERTIES
        public long SquadId { get; set; }
        public long VehicleId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Equipment.Vehicle Vehicle 
        {
            get { return Equipment.Vehicle.GetVehicle(VehicleId); }
        }
        private UnitStatus dispatchStatus;
        public UnitStatus DispatchStatus 
        {
            get 
            {
                dispatchStatus = UnitStatus.GetUnitStatus(SquadId, VehicleId, 1);
                return dispatchStatus;
            }
            set
            {
                dispatchStatus = value;
                UnitStatus.InsertUnitStatus(dispatchStatus);
            }
        }
        private UnitStatus enrouteStatus;
        public UnitStatus EnrouteStatus
        {
            get
            {
                enrouteStatus = UnitStatus.GetUnitStatus(SquadId, Vehicle.Id, 2);
                return enrouteStatus;
            }
            set
            {
                enrouteStatus = value;
                UnitStatus.InsertUnitStatus(enrouteStatus);
            }
        }
        private UnitStatus arrivedStatus;
        public UnitStatus ArrivedStatus
        {
            get
            {
                arrivedStatus = UnitStatus.GetUnitStatus(SquadId, Vehicle.Id, 3);
                return arrivedStatus;
            }
            set
            {
                arrivedStatus = value;
                UnitStatus.InsertUnitStatus(arrivedStatus);
            }
        }
        private UnitStatus clearedUnitStatus;
        public UnitStatus ClearedStatus
        {
            get
            {
                clearedUnitStatus = UnitStatus.GetUnitStatus(SquadId, Vehicle.Id, 4);
                return clearedUnitStatus;
            }
            set
            {
                clearedUnitStatus = value;
                UnitStatus.InsertUnitStatus(clearedUnitStatus);
            }
        }
        #endregion

        #region CONSTRUCTORS
        public SquadUnit() {}
        #endregion

        #region PRIVATE METHODS
        private static SquadUnit GetSquadUnitFromEntity(
            SquadUnitEntity record)
        {
            if (record == null) return null;

            SquadUnit result = new SquadUnit()
            {
                SquadId = record.SquadId,
                VehicleId = record.VehicleId,
                ModifiedDate = record.ModifiedDate
            };

            return result;
        }

        private static List<SquadUnit> GetSquadUnitsFromEntities(
            List<SquadUnitEntity> recordSet)
        {
            List<SquadUnit> result = new List<SquadUnit>();
            foreach (SquadUnitEntity record in recordSet)
                result.Add(GetSquadUnitFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static SquadUnit GetSquadUnit(long squadId, long vehicleId)
        {
            return GetSquadUnitFromEntity(
                SiteProvider.Dispatch.GetSquadUnit(squadId, vehicleId));
        }

        public static List<SquadUnit> GetSquadUnitsBySquad(long squadId)
        {
            return GetSquadUnitsFromEntities(
                SiteProvider.Dispatch.GetSquadUnitsBySquad(squadId));
        }

        public static bool UpdateSquadUnitStatus(long squadId, 
            long vehicleId, short statusId, DateTime changedDate)
        {
            //bool result = SquadUnitStatus.UpdateSquadUnitStatus(squadId, 
            //    vehicleId, statusId, changedDate);

            //return result;
            return true;
        }
        #endregion
    }
}

