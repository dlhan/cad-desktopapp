﻿//
// Cad.Data.BLL.Dispatch.UnitStatus
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class UnitStatus : BizObject
    {
        #region PROPERTIES
        public long SquadId { get; set; }
        public long VehicleId { get; set; }
        public short UnitStatusTypeId { get; set; }
        public DateTime ChangedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public UnitStatusType Status 
        {
            get 
            { 
                return UnitStatusType.GetUnitStatusType(UnitStatusTypeId); 
            }
        }
        #endregion

        #region CONSTRUCTORS
        public UnitStatus() {}
        #endregion

        #region PRIVATE METHODS
        private static UnitStatus GetUnitStatusFromEntity(
            UnitStatusEntity record)
        {
            if (record == null) return null;

            return new UnitStatus()
            {
                SquadId = record.SquadId,
                VehicleId = record.VehicleId,
                UnitStatusTypeId = record.UnitStatusTypeId,
                ChangedDate = record.ChangedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<UnitStatus> GetUnitStatusesFromEntities(
            List<UnitStatusEntity> recordSet)
        {
            List<UnitStatus> result = new List<UnitStatus>();
            foreach (UnitStatusEntity record in recordSet)
                result.Add(GetUnitStatusFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static UnitStatus GetUnitStatus(
            long squadId, long vehicleId, short unitStatusId)
        {
            return GetUnitStatusFromEntity(
                SiteProvider.Dispatch.GetUnitStatus(
                    squadId, vehicleId, unitStatusId));
        }

        public static List<UnitStatus> GetUnitStatusesByVehicle(
            long squadId, long vehicleId)
        {
            return GetUnitStatusesFromEntities(
                SiteProvider.Dispatch.GetUnitStatusesByVehicle(
                    squadId, vehicleId));
        }

        public static bool InsertUnitStatus(UnitStatus status)
        {
            return InsertUnitStatus(status.SquadId, 
                status.VehicleId, 
                status.UnitStatusTypeId, 
                status.ChangedDate);
        }

        public static bool InsertUnitStatus(long squadId, 
            long vehicleId, short statusTypeId, DateTime changedDate)
        {
            UnitStatusEntity record = new UnitStatusEntity()
            {
                SquadId = squadId,
                VehicleId = vehicleId,
                UnitStatusTypeId = statusTypeId,
                ChangedDate = changedDate,
            };

            return SiteProvider.Dispatch.InsertUnitStatus(record);
        }

        public static bool UpdateUnitStatus(UnitStatus status)
        {
            return UpdateUnitStatus(status.SquadId,
                status.VehicleId,
                status.UnitStatusTypeId,
                status.ChangedDate);
        }

        public static bool UpdateUnitStatus(long squadId, 
            long vehicleId, short statusId, DateTime changedDate)
        {
            UnitStatusEntity record = new UnitStatusEntity()
            {
                SquadId = squadId,
                VehicleId = vehicleId,
                UnitStatusTypeId = statusId,
                ChangedDate = changedDate,
            };

            return SiteProvider.Dispatch.UpdateUnitStatus(record);
        }
        #endregion
    }
}
