﻿//
// Cad.Data.BLL.Dispatch.UnitStatusType
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Dispatch
{
    public class UnitStatusType : BizObject
    {
        #region PROPERTIES
        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public UnitStatusType() {}
        #endregion

        #region PRIVATE METHODS
        private static UnitStatusType GetUnitStatusTypeFromEntity(
            UnitStatusTypeEntity record)
        {
            if (record == null) return null;

            return new UnitStatusType()
            {
                Id = record.Id,
                Name = record.Name,
                Description = record.Description,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<UnitStatusType> GetUnitStatusTypesFromEntities(
            List<UnitStatusTypeEntity> recordSet)
        {
            List<UnitStatusType> result = new List<UnitStatusType>();
            foreach (UnitStatusTypeEntity record in recordSet)
                result.Add(GetUnitStatusTypeFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static UnitStatusType GetUnitStatusType(short id)
        {
            return GetUnitStatusTypeFromEntity(
                SiteProvider.Dispatch.GetUnitStatusType(id));
        }

        public static List<UnitStatusType> GetUnitStatusTypes()
        {
            return GetUnitStatusTypesFromEntities(
                SiteProvider.Dispatch.GetUnitStatusTypes());
        }
        #endregion
    }
}
