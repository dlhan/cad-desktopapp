﻿//
// Cad.Data.BLL.Equipment.Vehicle
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Equipment
{
    public class Vehicle : BizObject
    {
        #region PROPERTIES
        public long Id { get; set; }
        public HumanResources.Department Department { get; set; }
        public VehicleType Type { get; set; }
        public string PlateNumber { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public short Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Vehicle() {}
        #endregion

        #region PRIVATE METHODS
        private static Vehicle GetVehicleFromEntity(
            VehicleEntity record)
        {
            if (record == null) return null;

            return new Vehicle()
            {
                Id = record.Id,
                Department = HumanResources.Department.GetDepartment(
                                record.DepartmentId),
                Type = VehicleType.GetVehicleType(record.VehicleTypeId),
                PlateNumber = record.PlateNumber,
                Maker = record.Maker,
                Model = record.Model,
                Status = record.Status,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<Vehicle> GetVehiclesFromEntities(
            List<VehicleEntity> recordSet)
        {
            List<Vehicle> result = new List<Vehicle>();
            foreach (VehicleEntity record in recordSet)
                result.Add(GetVehicleFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static Vehicle GetVehicle(long id)
        {
            return GetVehicleFromEntity(
                SiteProvider.Equipment.GetVehicle(id));
        }
        #endregion
    }
}
