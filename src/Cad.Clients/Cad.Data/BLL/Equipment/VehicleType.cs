﻿//
// Cad.Data.BLL.Equipment.VehicleType
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Equipment
{
    public class VehicleType : BizObject
    {
        #region PROPERTIES
        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public VehicleType() {}
        #endregion

        #region PRIVATE METHODS
        private static VehicleType GetVehicleTypeFromEntity(
            VehicleTypeEntity record)
        {
            if (record == null) return null;

            return new VehicleType()
            {
                Id = record.Id,
                Name = record.Name,
                Description = record.Description,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<VehicleType> GetVehicleTypesFromEntities(
            List<VehicleTypeEntity> recordSet)
        {
            List<VehicleType> result = new List<VehicleType>();
            foreach (VehicleTypeEntity record in recordSet)
                result.Add(GetVehicleTypeFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static VehicleType GetVehicleType(short id)
        {
            return GetVehicleTypeFromEntity(
                SiteProvider.Equipment.GetVehicleType(id));
        }
        #endregion
    }
}
