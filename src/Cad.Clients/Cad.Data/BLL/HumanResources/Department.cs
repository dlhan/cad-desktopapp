﻿//
// Cad.Data.BLL.HumanResources.Department
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.HumanResources
{
    public class Department : BizObject
    {
        #region PROPERTIES
        public long Id { get; set; }
        public string Name { get; set; }
        public string Acronym { get; set; }
        public bool CurrentFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Department() {}
        #endregion

        #region PRIVATE METHODS
        private static Department GetDepartmentFromEntity(
            DepartmentEntity record)
        {
            if (record == null) return null;

            return new Department()
            {
                Id = record.Id,
                Name = record.Name,
                Acronym = record.Acronym,
                CurrentFlag = record.CurrentFlag,
                CreatedDate = record.CreatedDate,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<Department> GetDepartmentsFromEntities(
            List<DepartmentEntity> recordSet)
        {
            List<Department> result = new List<Department>();
            foreach (DepartmentEntity record in recordSet)
                result.Add(GetDepartmentFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static Department GetDepartment(long id)
        {
            return GetDepartmentFromEntity(
                SiteProvider.HumanResources.GetDepartment(id));
        }
        #endregion
    }
}
