﻿//
// Cad.Data.BLL.Person.City
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Person
{
    public class City : BizObject
    {
        #region PROPERTIES
        public string CityCode { get; set; }
        public StateProvince StateProvince { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public City() { }
        #endregion

        #region PRIVATE METHODS
        private static City GetCityFromEntity(CityEntity record)
        {
            if (record == null) return null;

            return new City()
            {
                CityCode = record.CityCode,
                StateProvince = StateProvince.GetStateProvince(
                        record.StateProvinceCode),
                Name = record.Name,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<City> 
            GetCitiesFromEntities(List<CityEntity> recordSet)
        {
            List<City> result = new List<City>();
            foreach (CityEntity record in recordSet)
                result.Add(GetCityFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static City GetCity(string cityCode)
        {
            return GetCityFromEntity(SiteProvider.Person.GetCity(cityCode));
        }

        public static List<City> GetCities(string stateProvinceCode)
        {
            return GetCitiesFromEntities(
                SiteProvider.Person.GetCities(stateProvinceCode));
        }
        #endregion
    }
}
