﻿//
// Cad.Data.BLL.Person.StateProvince
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using Cad.Data.DAL;
using Cad.Data.Domain.Entities;

namespace Cad.Data.BLL.Person
{
    public class StateProvince : BizObject
    {
        #region PROPERTIES
        public string StateProvinceCode { get; set; }
        public string CountryRegionCode { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public StateProvince() { }
        #endregion

        #region PRIVATE METHODS
        private static StateProvince 
            GetStateProvinceFromEntity(StateProvinceEntity record)
        {
            if (record == null) return null;

            return new StateProvince()
            {
                StateProvinceCode = record.StateProvinceCode,
                CountryRegionCode = record.CountryRegionCode,
                Name = record.Name,
                ModifiedDate = record.ModifiedDate
            };
        }

        private static List<StateProvince> 
            GetStateProvincesFromEntities(List<StateProvinceEntity> recordSet)
        {
            List<StateProvince> result = new List<StateProvince>();
            foreach (StateProvinceEntity record in recordSet)
                result.Add(GetStateProvinceFromEntity(record));
            return result;
        }
        #endregion

        #region PUBLIC METHODS
        public static StateProvince GetStateProvince(string stateProvinceCode)
        {
            return GetStateProvinceFromEntity(
                SiteProvider.Person.GetStateProvince(stateProvinceCode));
        }

        public static List<StateProvince> 
            GetStateProvinces(string countryRegionCode)
        {
            return GetStateProvincesFromEntities(
                SiteProvider.Person.GetStateProvinces(countryRegionCode));
        }
        #endregion
    }
}
