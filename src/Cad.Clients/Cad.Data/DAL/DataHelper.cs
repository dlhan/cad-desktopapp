﻿//
// Cad.Data.DAL.DataHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using Microsoft.Spatial;
using System;
using System.Data;
using System.Data.Spatial;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace Cad.Data.DAL
{
    public static class DataHelper
    {
        public static byte NullToByte(IDataReader reader, string col)
        {
            if (!byte.TryParse(reader[col].ToString(), out byte result))
                result = byte.MinValue;
            return result;
        }

        public static DateTime NullToDateTime(IDataReader reader, string col)
        {
            if (!DateTime.TryParse(reader[col].ToString(), out DateTime result))
                result = DateTime.MinValue;
            return result;
        }

        public static GeographyPoint NullToGeography(IDataReader reader, string col)
        {
            GeographyPoint result = null;

            byte[] location = (byte[])reader[col];
            if (location != null)
            {
                MySqlGeometry geometry = new MySqlGeometry(MySqlDbType.Byte, location);
                result = GeographyPoint.Create((double)geometry.XCoordinate,
                    (double)geometry.YCoordinate);
            }

            return result;
        }

        public static string NullToString(IDataReader reader, string col)
        {
            return reader[col] == null 
                ? string.Empty 
                : reader[col].ToString();
        }
    }
}
