﻿//
// Cad.Data.DAL.DispatchProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.Data;
using Cad.Clients.Configuration;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL
{
    public abstract class DispatchProvider : DataAccess
    {
        #region PROPERTIES
        private static DispatchProvider _instance = null;
        public static DispatchProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (DispatchProvider)Activator.CreateInstance(
                        Type.GetType(ConfigurationContext.Settings.Dispatch.ProviderType));
                }
                return _instance;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public DispatchProvider()
        {
            ConnectionString = ConfigurationContext
                .Settings
                .Dispatch
                .ConnectionString;
        }
        #endregion

        #region ABSTRACT METHODS
        public abstract CallDispositionEntity GetCallDisposition(short id);
        public abstract List<CallDispositionEntity> GetCallDispositions();

        public abstract CallLocationEntity GetCallLocation(long reportCallId);

        public abstract CallNumberStatusEntity GetCallNumberStatus(short id);
        public abstract List<CallNumberStatusEntity> GetCallNumberStatuses();

        public abstract CallPhoneNumberEntity GetCallPhoneNumber(long id);

        public abstract CallRouteEntity GetCallRoute(short id);
        public abstract List<CallRouteEntity> GetCallRoutes();

        public abstract IncidentEntity GetIncident(long id);
        public abstract List<IncidentEntity> GetOpenedIncidents();

        public abstract IncidentCategoryEntity GetIncidentCategoryBySubcategory(
            short subcategoryId);
        public abstract IncidentCategoryEntity GetIncidentCategory(
            short id);
        public abstract List<IncidentCategoryEntity> GetIncidentCategories();

        public abstract IncidentLocationEntity GetIncidentLocation(
            long incidentId);

        public abstract IncidentScaleEntity GetIncidentScale(short id);
        public abstract List<IncidentScaleEntity> GetIncidentScales();
        
        public abstract IncidentSubcategoryEntity GetIncidentSubcategory(
            short id);
        public abstract List<IncidentSubcategoryEntity> GetIncidentSubcategories(
            short categoryId);

        public abstract ReportCallEntity GetReportCall(long id);
        public abstract List<ReportCallEntity> GetReportCallsByIncident(
            long incidentId, bool isDuplicated = false);

        public abstract SquadEntity GetSquad(long id);
        public abstract List<SquadEntity> GetSquadsByIncident(long incidentId);

        public abstract SquadUnitEntity GetSquadUnit(long squadId, long vehicleId);
        public abstract List<SquadUnitEntity> GetSquadUnitsBySquad(long squadId);

        public abstract UnitStatusEntity GetUnitStatus(
            long squadId, long vehicleId, short unitStatusId);
        public abstract List<UnitStatusEntity> GetUnitStatusesByVehicle(
            long squadId, long vehicleId);
        public abstract bool InsertUnitStatus(UnitStatusEntity squadUnitStatus);
        public abstract bool UpdateUnitStatus(UnitStatusEntity squadUnitStatus);

        public abstract UnitStatusTypeEntity GetUnitStatusType(short id);
        public abstract List<UnitStatusTypeEntity> GetUnitStatusTypes();
        #endregion

        #region VIRTUAL METHODS
        protected virtual CallDispositionEntity GetCallDispositionFromReader(
            IDataReader reader)
        {
            return new CallDispositionEntity()
            {
                Id = (short)reader["id"],
                Name = reader["name"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<CallDispositionEntity> GetCallDispositionCollectionFromReader(
            IDataReader reader)
        {
            List<CallDispositionEntity> result = new List<CallDispositionEntity>();
            while (reader.Read())
                result.Add(GetCallDispositionFromReader(reader));
            return result;
        }


        protected virtual CallLocationEntity GetCallLocationFromReader(
            IDataReader reader)
        {
            return new CallLocationEntity()
            {
                ReportCallId = (long)reader["report_call_id"],
                CityCode = reader["city_code"].ToString(),
                AddressLine1 = reader["address_line1"].ToString(),
                AddressLine2 = DataHelper.NullToString(reader, "address_line2"),
                PostalCode = DataHelper.NullToString(reader, "postal_code"),
                SpatialLocation = DataHelper.NullToGeography(reader, "spatial_location"),
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }


        protected virtual CallPhoneNumberEntity GetCallPhoneNumberFromReader(
            IDataReader reader)
        {
            return new CallPhoneNumberEntity()
            {
                Id = (long)reader["id"],
                PhoneNumber = reader["phone_number"].ToString(),
                CallNumberStatusId = (short)reader["call_number_status_id"],
                NumOfCalls = (short)reader["num_of_calls"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }


        protected virtual CallNumberStatusEntity GetCallNumberStatusFromReader(
            IDataReader reader)
        {
            return new CallNumberStatusEntity()
            {
                Id = (short)reader["id"],
                Name = reader["name"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<CallNumberStatusEntity> GetCallNumberStatusCollectionFromReader(
            IDataReader reader)
        {
            List<CallNumberStatusEntity> result = new List<CallNumberStatusEntity>();
            while (reader.Read())
                result.Add(GetCallNumberStatusFromReader(reader));
            return result;
        }


        protected virtual CallRouteEntity GetCallRouteFromReader(
            IDataReader reader)
        {
            return new CallRouteEntity()
            {
                Id = (short)reader["id"],
                Name = reader["name"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<CallRouteEntity> GetCallRouteCollectionFromReader(
            IDataReader reader)
        {
            List<CallRouteEntity> result = new List<CallRouteEntity>();
            while (reader.Read())
                result.Add(GetCallRouteFromReader(reader));
            return result;
        }


        protected virtual IncidentEntity GetIncidentFromReader(
            IDataReader reader)
        {
            return new IncidentEntity()
            {
                Id = (long)reader["id"],
                IncidentSubcategoryId = (short)reader["incident_subcategory_id"],
                IncidentScaleId = (short)reader["incident_scale_id"],
                Synopsys = DataHelper.NullToString(reader, "synopsys"),
                OpenedDate = (DateTime)reader["opened_date"],
                ClosedDate = DataHelper.NullToDateTime(reader, "closed_date"),
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"],
            };
        }

        protected virtual List<IncidentEntity> GetIncidentCollectionFromReader(
            IDataReader reader)
        {
            List<IncidentEntity> result = new List<IncidentEntity>();
            while (reader.Read()) result.Add(GetIncidentFromReader(reader));
            return result;
        }


        protected virtual IncidentCategoryEntity GetIncidentCategoryFromReader(
            IDataReader reader)
        {
            return new IncidentCategoryEntity()
            {
                Id = (short)reader["id"],
                Name = reader["name"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<IncidentCategoryEntity> GetIncidentCategoryCollectionFromReader(
            IDataReader reader)
        {
            List<IncidentCategoryEntity> result =
                new List<IncidentCategoryEntity>();
            while (reader.Read())
                result.Add(GetIncidentCategoryFromReader(reader));
            return result;
        }


        protected virtual IncidentLocationEntity GetIncidentLocationFromReader(
            IDataReader reader)
        {
            return new IncidentLocationEntity()
            {
                IncidentId = (long)reader["incident_id"],
                CityCode = reader["city_code"].ToString(),
                AddressLine1 = reader["address_line1"].ToString(),
                AddressLine2 = DataHelper.NullToString(reader, "address_line2"),
                PostalCode = DataHelper.NullToString(reader, "postal_code"),
                SpatialLocation = DataHelper.NullToGeography(reader, "spatial_location"),
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }


        protected virtual IncidentScaleEntity GetIncidentScaleFromReader(
            IDataReader reader)
        {
            return new IncidentScaleEntity()
            {
                Id = (short)reader["id"],
                Name = reader["name"].ToString(),
                Description = reader["description"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<IncidentScaleEntity> GetIncidentScaleCollectionFromReader(
            IDataReader reader)
        {
            List<IncidentScaleEntity> result = new List<IncidentScaleEntity>();
            while (reader.Read()) result.Add(GetIncidentScaleFromReader(reader));
            return result;
        }


        protected virtual IncidentSubcategoryEntity GetIncidentSubcategoryFromReader(
            IDataReader reader)
        {
            return new IncidentSubcategoryEntity()
            {
                Id = (short)reader["id"],
                IncidentCategoryID = (short)reader["incident_category_id"],
                Name = reader["name"].ToString(),
                Description = reader["description"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<IncidentSubcategoryEntity> GetIncidentSubcategoryCollectionFromReader(
            IDataReader reader)
        {
            List<IncidentSubcategoryEntity> result =
                new List<IncidentSubcategoryEntity>();
            while (reader.Read())
                result.Add(GetIncidentSubcategoryFromReader(reader));
            return result;
        }


        protected virtual ReportCallEntity GetReportCallFromReader(
            IDataReader reader)
        {
            return new ReportCallEntity()
            {
                Id = (long)reader["id"],
                IncidentId = (long)reader["incident_id"],
                CallRouteId = (short)reader["call_route_id"],
                CallPhoneNumberId = (long)reader["call_phone_number_id"],
                CallerName = reader["caller_name"].ToString(),
                CallDispositionId = (short)reader["call_disposition_id"],
                IsDuplicated = (bool)reader["is_duplicated"],
                IncomingDate = (DateTime)reader["incoming_date"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<ReportCallEntity> GetReportCallCollectionFromReader(
            IDataReader reader)
        {
            List<ReportCallEntity> result = new List<ReportCallEntity>();
            while (reader.Read())
                result.Add(GetReportCallFromReader(reader));
            return result;
        }


        protected virtual SquadEntity GetSquadFromReader(IDataReader reader)
        {
            return new SquadEntity()
            {
                Id = (long)reader["id"],
                IncidentId = (long)reader["incident_id"],
                BuildGuideId = (short)reader["build_guide_id"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<SquadEntity> GetSquadCollectionFromReader(
            IDataReader reader)
        {
            List<SquadEntity> result = new List<SquadEntity>();
            while (reader.Read())
                result.Add(GetSquadFromReader(reader));
            return result;
        }


        protected virtual SquadUnitEntity GetSquadUnitFromReader(
            IDataReader reader)
        {
            return new SquadUnitEntity()
            {
                SquadId = (long)reader["squad_id"],
                VehicleId = (long)reader["vehicle_id"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<SquadUnitEntity> GetSquadUnitCollectionFromReader(
            IDataReader reader)
        {
            List<SquadUnitEntity> result = new List<SquadUnitEntity>();
            while (reader.Read())
                result.Add(GetSquadUnitFromReader(reader));
            return result;
        }


        protected virtual UnitStatusEntity GetUnitStatusFromReader(
            IDataReader reader)
        {
            return new UnitStatusEntity()
            {
                SquadId = (long)reader["squad_id"],
                VehicleId = (long)reader["vehicle_id"],
                UnitStatusTypeId = (short)reader["unit_status_type_id"],
                ChangedDate = (DateTime)reader["changed_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<UnitStatusEntity> GetUnitStatusCollectionFromReader(
            IDataReader reader)
        {
            List<UnitStatusEntity> result = new List<UnitStatusEntity>();
            while (reader.Read())
                result.Add(GetUnitStatusFromReader(reader));
            return result;
        }


        protected virtual UnitStatusTypeEntity GetUnitStatusTypeFromReader(
            IDataReader reader)
        {
            return new UnitStatusTypeEntity()
            {
                Id = (short)reader["id"],
                Name = reader["name"].ToString(),
                Description = reader["description"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<UnitStatusTypeEntity> GetUnitStatusTypeCollectionFromReader(
            IDataReader reader)
        {
            List<UnitStatusTypeEntity> result = new List<UnitStatusTypeEntity>();
            while (reader.Read())
                result.Add(GetUnitStatusTypeFromReader(reader));
            return result;
        }
        #endregion
    }
}
