﻿//
// Cad.Data.DAL.EquipmentProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.Data;
using Cad.Clients.Configuration;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL
{
    public abstract class EquipmentProvider : DataAccess
    {
        #region PROPERTIES
        private static EquipmentProvider _instance = null;
        public static EquipmentProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (EquipmentProvider)Activator.CreateInstance(
                        Type.GetType(ConfigurationContext.Settings.Equipment.ProviderType));
                }
                return _instance;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public EquipmentProvider()
        {
            ConnectionString = ConfigurationContext
                .Settings
                .Dispatch
                .ConnectionString;
        }
        #endregion

        #region ABSTRACT METHODS
        public abstract VehicleEntity GetVehicle(long id);
        public abstract VehicleTypeEntity GetVehicleType(short id);
        #endregion

        #region VIRTUAL METHODS
        protected virtual VehicleEntity 
            GetVehicleFromReader(IDataReader reader)
        {
            return new VehicleEntity()
            {
                Id = (long)reader["id"],
                DepartmentId = (long)reader["department_id"],
                VehicleTypeId = (short)reader["vehicle_type_id"],
                PlateNumber = reader["plate_number"].ToString(),
                Maker = reader["maker"].ToString(),
                Model = reader["model"].ToString(),
                Status = (short)reader["status"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<VehicleEntity> 
            GetVehicleCollectionFromReader(IDataReader reader)
        {
            List<VehicleEntity> result = new List<VehicleEntity>();
            while (reader.Read())
                result.Add(GetVehicleFromReader(reader));
            return result;
        }

        protected virtual VehicleTypeEntity
            GetVehicleTypeFromReader(IDataReader reader)
        {
            return new VehicleTypeEntity()
            {
                Id = (short)reader["id"],
                Name = reader["name"].ToString(),
                Description = reader["description"].ToString(),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<VehicleTypeEntity>
            GetVehicleTypeCollectionFromReader(IDataReader reader)
        {
            List<VehicleTypeEntity> result = new List<VehicleTypeEntity>();
            while (reader.Read())
                result.Add(GetVehicleTypeFromReader(reader));
            return result;
        }
        #endregion
    }
}
