﻿//
// Cad.Data.DAL.HumanResourcesProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.Data;
using Cad.Clients.Configuration;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL
{
    public abstract class HumanResourcesProvider : DataAccess
    {
        #region PROPERTIES
        private static HumanResourcesProvider _instance = null;
        public static HumanResourcesProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (HumanResourcesProvider)Activator.CreateInstance(
                        Type.GetType(ConfigurationContext.Settings.HumanResources.ProviderType));
                }
                return _instance;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public HumanResourcesProvider()
        {
            ConnectionString = ConfigurationContext
                .Settings
                .Dispatch
                .ConnectionString;
        }
        #endregion

        #region ABSTRACT METHODS
        public abstract DepartmentEntity GetDepartment(long id);
        #endregion

        #region VIRTUAL METHODS
        protected virtual DepartmentEntity 
            GetDepartmentFromReader(IDataReader reader)
        {
            return new DepartmentEntity()
            {
                Id = (long)reader["id"],
                Name = reader["name"].ToString(),
                Acronym = DataHelper.NullToString(reader, "acronym"),
                CurrentFlag = (bool)reader["current_flag"],
                CreatedDate = (DateTime)reader["created_date"],
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<DepartmentEntity> 
            getDepartmentCollectionFromReader(IDataReader reader)
        {
            List<DepartmentEntity> result = new List<DepartmentEntity>();
            while (reader.Read())
                result.Add(GetDepartmentFromReader(reader));
            return result;
        }
        #endregion
    }
}
