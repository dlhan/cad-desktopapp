﻿//
// Cad.Data.DAL.MySqlClient.MySqlDispatchProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL.MySqlClient
{
    public class MySqlDispatchProvider : DispatchProvider
    {
        public override CallDispositionEntity GetCallDisposition(short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_disposition", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCallDispositionFromReader(reader);

                return null;
            }
        }

        public override List<CallDispositionEntity> GetCallDispositions()
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_dispositions", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetCallDispositionCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override CallLocationEntity GetCallLocation(long reportCallId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_location", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_report_call_id", MySqlDbType.Int64).Value = reportCallId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCallLocationFromReader(reader);

                return null;
            }
        }


        public override CallNumberStatusEntity GetCallNumberStatus(short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_number_status", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCallNumberStatusFromReader(reader);

                return null;
            }
        }

        public override List<CallNumberStatusEntity> GetCallNumberStatuses()
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_number_statuses", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetCallNumberStatusCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override CallPhoneNumberEntity GetCallPhoneNumber(long id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_phone_number", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int64).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCallPhoneNumberFromReader(reader);

                return null;
            }
        }


        public override CallRouteEntity GetCallRoute(short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_route", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCallRouteFromReader(reader);

                return null;
            }
        }

        public override List<CallRouteEntity> GetCallRoutes()
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_call_routes", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetCallRouteCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override IncidentEntity GetIncident(long id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_incident", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int64).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetIncidentFromReader(reader);

                return null;
            }
        }

        public override List<IncidentEntity> GetOpenedIncidents()
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_opened_incidents", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetIncidentCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override IncidentCategoryEntity GetIncidentCategoryBySubcategory(
            short subcategoryId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand(
                    "get_incident_category_by_subcategory", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_subcategory_id", MySqlDbType.Int16).Value = subcategoryId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetIncidentCategoryFromReader(reader);

                return null;
            }
        }

        public override IncidentCategoryEntity GetIncidentCategory(short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_incident_category", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetIncidentCategoryFromReader(reader);

                return null;
            }
        }

        public override List<IncidentCategoryEntity> GetIncidentCategories()
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_incident_categories", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetIncidentCategoryCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override IncidentLocationEntity GetIncidentLocation(
            long incidentId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_incident_location", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_incident_id", MySqlDbType.Int64).Value = incidentId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetIncidentLocationFromReader(reader);

                return null;
            }
        }


        public override IncidentScaleEntity GetIncidentScale(short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_incident_scale", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetIncidentScaleFromReader(reader);

                return null;
            }
        }

        public override List<IncidentScaleEntity> GetIncidentScales()
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_incident_scales", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetIncidentScaleCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override IncidentSubcategoryEntity GetIncidentSubcategory(
            short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_incident_subcategory", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetIncidentSubcategoryFromReader(reader);

                return null;
            }
        }

        public override List<IncidentSubcategoryEntity> GetIncidentSubcategories(
            short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand(
                    "get_incident_subcategories_by_category", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_category_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                return GetIncidentSubcategoryCollectionFromReader(ExecuteReader(cmd));
            }
        }
        

        public override ReportCallEntity GetReportCall(long id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_report_call", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int64).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetReportCallFromReader(reader);

                return null;
            }
        }

        public override List<ReportCallEntity> GetReportCallsByIncident(
            long incidentId, bool isDuplicated = false)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_report_calls_by_incident", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_incident_id", MySqlDbType.Int64).Value = incidentId;
                cmd.Parameters.Add("p_is_duplicated", MySqlDbType.Bit).Value = isDuplicated;
                cn.Open();
                return GetReportCallCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override SquadEntity GetSquad(long id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_squad", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int64).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetSquadFromReader(reader);

                return null;
            }
        }

        public override List<SquadEntity> GetSquadsByIncident(long incidentId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_squads_by_incident", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_incident_id", MySqlDbType.Int64).Value = incidentId;
                cn.Open();
                return GetSquadCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override SquadUnitEntity GetSquadUnit(long squadId, long vehicleId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_squad_unit", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_squad_id", MySqlDbType.Int64).Value = squadId;
                cmd.Parameters.Add("p_vehicle_id", MySqlDbType.Int64).Value = vehicleId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetSquadUnitFromReader(reader);

                return null;
            }
        }

        public override List<SquadUnitEntity> GetSquadUnitsBySquad(long squadId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_squad_units_by_squad", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_squad_id", MySqlDbType.Int64).Value = squadId;
                cn.Open();
                return GetSquadUnitCollectionFromReader(ExecuteReader(cmd));
            }
        }
        

        public override UnitStatusEntity GetUnitStatus(
            long squadId, long vehicleId, short unitStatusTypeId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_unit_status", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_squad_id", MySqlDbType.Int64).Value = squadId;
                cmd.Parameters.Add("p_vehicle_id", MySqlDbType.Int64).Value = vehicleId;
                cmd.Parameters.Add("p_unit_status_type_id", MySqlDbType.Int16).Value = unitStatusTypeId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetUnitStatusFromReader(reader);

                return null;
            }
        }

        public override List<UnitStatusEntity> GetUnitStatusesByVehicle(
            long squadId, long vehicleId)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                //MySqlCommand cmd = new MySqlCommand("get_squad_unit_statuses_by_vehicle", cn);
                MySqlCommand cmd = new MySqlCommand()
                {
                    CommandText = "get_unit_statuses_by_vehicle",
                    Connection = cn
                };
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_squad_id", MySqlDbType.Int64).Value = squadId;
                cmd.Parameters.Add("p_vehicle_id", MySqlDbType.Int64).Value = vehicleId;
                cn.Open();
                return GetUnitStatusCollectionFromReader(ExecuteReader(cmd));
            }
        }

        public override bool InsertUnitStatus(UnitStatusEntity unitStatus)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand()
                {
                    CommandText = "insert_unit_status",
                    Connection = cn
                };
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_squad_id", MySqlDbType.Int64).Value 
                    = unitStatus.SquadId;
                cmd.Parameters.Add("p_vehicle_id", MySqlDbType.Int64).Value 
                    = unitStatus.VehicleId;
                cmd.Parameters.Add("p_unit_status_type_id", MySqlDbType.Int16).Value 
                    = unitStatus.UnitStatusTypeId;
                cmd.Parameters.Add("p_changed_date", MySqlDbType.DateTime).Value 
                    = unitStatus.ChangedDate;
                cn.Open();
                return (ExecuteNonQuery(cmd) == 1);
            }
        }

        public override bool UpdateUnitStatus(UnitStatusEntity unitStatus)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand()
                {
                    CommandText = "update_unit_status",
                    Connection = cn
                };
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_squad_id", MySqlDbType.Int64).Value 
                    = unitStatus.SquadId;
                cmd.Parameters.Add("p_vehicle_id", MySqlDbType.Int64).Value 
                    = unitStatus.VehicleId;
                cmd.Parameters.Add("p_unit_status_type_id", MySqlDbType.Int16).Value 
                    = unitStatus.UnitStatusTypeId;
                cmd.Parameters.Add("p_changed_date", MySqlDbType.DateTime).Value 
                    = unitStatus.ChangedDate;
                cn.Open();
                return (ExecuteNonQuery(cmd) == 1);
            }
        }


        public override UnitStatusTypeEntity GetUnitStatusType(short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_unit_status_type", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetUnitStatusTypeFromReader(reader);

                return null;
            }
        }

        public override List<UnitStatusTypeEntity> GetUnitStatusTypes()
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand()
                {
                    CommandText = "get_unit_status_types",
                    Connection = cn
                };
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return GetUnitStatusTypeCollectionFromReader(ExecuteReader(cmd));
            }
        }
    }
}
