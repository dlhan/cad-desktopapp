﻿//
// Cad.Data.DAL.MySqlClient.MySqlEquipmentProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL.MySqlClient
{
    public class MySqlEquipmentProvider : EquipmentProvider
    {
        public override VehicleEntity GetVehicle(long id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_vehicle", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int64).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetVehicleFromReader(reader);

                return null;
            }
        }

        public override VehicleTypeEntity GetVehicleType(short id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_vehicle_type", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int16).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetVehicleTypeFromReader(reader);

                return null;
            }
        }
    }
}
