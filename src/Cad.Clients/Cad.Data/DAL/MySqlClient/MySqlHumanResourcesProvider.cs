﻿//
// Cad.Data.DAL.MySqlClient.MySqlHumanResourcesProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL.MySqlClient
{
    public class MySqlHumanResourcesProvider : HumanResourcesProvider
    {
        public override DepartmentEntity GetDepartment(long id)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_department", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_id", MySqlDbType.Int64).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetDepartmentFromReader(reader);

                return null;
            }
        }
    }
}
