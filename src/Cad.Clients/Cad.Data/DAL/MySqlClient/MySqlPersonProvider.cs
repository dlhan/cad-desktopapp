﻿//
// Cad.Data.DAL.MySqlClient.MySqlPersonProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL.MySqlClient
{
    public class MySqlPersonProvider : PersonProvider
    {
        public override CityEntity GetCity(string cityCode)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_city", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_city_code", MySqlDbType.VarChar, 5)
                    .Value = cityCode;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCityFromReader(reader);

                return null;
            }
        }

        public override List<CityEntity> GetCities(string stateProvinceCode)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_cities_by_province", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_stateprovince_code", MySqlDbType.VarChar, 4)
                    .Value = stateProvinceCode;
                cn.Open();
                return GetCityCollectionFromReader(ExecuteReader(cmd));
            }
        }


        public override StateProvinceEntity GetStateProvince(string stateProvinceCode)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_state_province", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_stateprovince_code", MySqlDbType.VarChar, 4)
                    .Value = stateProvinceCode;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetStateProvinceFromReader(reader);

                return null;
            }
        }

        public override List<StateProvinceEntity> GetStateProvinces(string countryRegionCode)
        {
            using (MySqlConnection cn = new MySqlConnection(this.ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand("get_state_provinces_by_countryregion", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_countryregion_code", MySqlDbType.VarChar, 3)
                    .Value = countryRegionCode;
                cn.Open();
                return GetStateProvinceCollectionFromReader(ExecuteReader(cmd));
            }
        }
    }
}
