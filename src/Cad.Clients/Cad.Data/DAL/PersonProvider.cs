﻿//
// Cad.Data.DAL.PersonProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;
using System.Collections.Generic;
using System.Data;
using Cad.Clients.Configuration;
using Cad.Data.Domain.Entities;

namespace Cad.Data.DAL
{
    public abstract class PersonProvider : DataAccess
    {
        #region PROPERTIES
        private static PersonProvider _instance = null;
        public static PersonProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (PersonProvider)Activator.CreateInstance(
                        Type.GetType(ConfigurationContext.Settings.Person.ProviderType));
                }
                return _instance;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public PersonProvider()
        {
            ConnectionString = ConfigurationContext
                .Settings
                .Dispatch
                .ConnectionString;
        }
        #endregion

        #region ABSTRACT METHODS
        public abstract CityEntity GetCity(string cityCode);
        public abstract List<CityEntity>
            GetCities(string stateProvinceCode);

        public abstract StateProvinceEntity 
            GetStateProvince(string stateProvinceCode);
        public abstract List<StateProvinceEntity> 
            GetStateProvinces(string countryRegionCode);
        #endregion

        #region VIRTUAL METHODS
        protected virtual CityEntity
            GetCityFromReader(IDataReader reader)
        {
            return new CityEntity()
            {
                CityCode = reader["city_code"].ToString(),
                StateProvinceCode = reader["state_province_code"].ToString(),
                Name = reader["name"].ToString(),
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<CityEntity>
            GetCityCollectionFromReader(IDataReader reader)
        {
            List<CityEntity> cities = new List<CityEntity>();
            while (reader.Read()) cities.Add(GetCityFromReader(reader));
            return cities;
        }


        protected virtual StateProvinceEntity 
            GetStateProvinceFromReader(IDataReader reader)
        {
            return new StateProvinceEntity()
            {
                StateProvinceCode = reader["state_province_code"].ToString(),
                Name = reader["name"].ToString(),
                ModifiedDate = (DateTime)reader["modified_date"]
            };
        }

        protected virtual List<StateProvinceEntity>
            GetStateProvinceCollectionFromReader(IDataReader reader)
        {
            List<StateProvinceEntity> provinces = new List<StateProvinceEntity>();
            while (reader.Read()) provinces.Add(GetStateProvinceFromReader(reader));
            return provinces;
        }
        #endregion
    }
}
