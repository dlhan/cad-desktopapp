﻿//
// Cad.Data.DAL.SiteProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.DAL
{
    public static class SiteProvider
    {
        public static DispatchProvider Dispatch
        {
            get { return DispatchProvider.Instance; }
        }

        public static EquipmentProvider Equipment
        {
            get { return EquipmentProvider.Instance; }
        }

        public static HumanResourcesProvider HumanResources
        {
            get { return HumanResourcesProvider.Instance; }
        }

        public static PersonProvider Person
        {
            get { return PersonProvider.Instance; }
        }
    }
}
