﻿//
// Cad.Data.Domain.Entities.CallLocation
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using Microsoft.Spatial;

namespace Cad.Data.Domain.Entities
{
    public class CallLocationEntity : EntityBase
    {
        #region PROPERTIES
        public long ReportCallId { get; set; }
        public string CityCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public GeographyPoint SpatialLocation { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallLocationEntity() { }
        #endregion
    }
}
