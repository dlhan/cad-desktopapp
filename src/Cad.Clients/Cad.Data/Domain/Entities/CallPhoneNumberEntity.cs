﻿//
// Cad.Data.Domain.Entities.CallPhoneNumberEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class CallPhoneNumberEntity : EntityBase
    {
        #region PROPERTIES
        public long Id { get; set; }
        public string PhoneNumber { get; set; }
        public short CallNumberStatusId { get; set; }
        public short NumOfCalls { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallPhoneNumberEntity() { }
        #endregion
    }
}
