﻿//
// Cad.Data.Domain.Entities.CallRouteEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class CallRouteEntity : EntityBase
    {
        #region PROPERTIES
        public short Id { get; set; }
        public string Name { get; set; }
        public bool CurrentFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CallRouteEntity() { }
        #endregion
    }
}
