﻿//
// Cad.Data.Domain.Entities.CityEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class CityEntity : EntityBase
    {
        #region PROPERTIES
        public string CityCode { get; set; }
        public string StateProvinceCode { get; set; }
        public string Name { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CityEntity() { }
        #endregion
    }
}
