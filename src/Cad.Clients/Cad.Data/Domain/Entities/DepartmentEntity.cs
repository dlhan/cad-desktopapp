﻿//
// Cad.Data.Domain.Entities.DepartmentEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class DepartmentEntity : EntityBase
    {
        #region PROPERTIES
        public long Id { get; set; }
        public string Name { get; set; }
        public string Acronym { get; set; }
        public bool CurrentFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DepartmentEntity() { }
        #endregion
    }
}
