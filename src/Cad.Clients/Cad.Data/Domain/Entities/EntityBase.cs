﻿//
// Cad.Data.Domain.Entities.EntityBase
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class EntityBase
    {
        public System.DateTime ModifiedDate { get; set; }
    }
}
