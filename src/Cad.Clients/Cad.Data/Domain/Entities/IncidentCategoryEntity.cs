﻿//
// Cad.Data.Domain.Entities.IncidentCategoryEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class IncidentCategoryEntity : EntityBase
    {
        #region PROPERTIES
        public short Id { get; set; }
        public string Name { get; set; }
        public short ImageIndex { get; set; }
        public bool CurrentFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentCategoryEntity() { }
        #endregion
    }
}
