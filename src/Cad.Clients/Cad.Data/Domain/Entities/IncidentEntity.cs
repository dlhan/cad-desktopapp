﻿//
// Cad.Data.Domain.Entities.IncidentEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class IncidentEntity : EntityBase
    {
        #region PROPERTIES
        public long Id { get; set; }
        public short IncidentSubcategoryId { get; set; }
        public short IncidentScaleId { get; set; }
        public string Synopsys { get; set; }
        public System.DateTime OpenedDate { get; set; }
        public System.DateTime ClosedDate { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentEntity() { }
        #endregion
    }
}
