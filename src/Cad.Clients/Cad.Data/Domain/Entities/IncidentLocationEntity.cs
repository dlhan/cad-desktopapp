﻿//
// Cad.Data.Domain.Entities.IncidentLocation
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using Microsoft.Spatial;

namespace Cad.Data.Domain.Entities
{
    public class IncidentLocationEntity : EntityBase
    {
        #region PROPERTIES
        public long IncidentId { get; set; }
        public string CityCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public GeographyPoint SpatialLocation { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentLocationEntity() { }
        #endregion
    }
}
