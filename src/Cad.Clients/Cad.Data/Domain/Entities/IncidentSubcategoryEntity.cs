﻿//
// Cad.Data.Domain.Entities.IncidentSubcategoryEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class IncidentSubcategoryEntity : EntityBase
    {
        #region PROPERTIES
        public short Id { get; set; }
        public short IncidentCategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool CurrentFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public IncidentSubcategoryEntity() { }
        #endregion
    }
}
