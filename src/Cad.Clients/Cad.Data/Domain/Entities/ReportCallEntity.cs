﻿//
// Cad.Data.Domain.Entities.ReportCallEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class ReportCallEntity : EntityBase
    {
        #region PROPERTIES
        public long Id { get; set; }
        public long IncidentId { get; set; }
        public short CallRouteId { get; set; }
        public long CallPhoneNumberId { get; set; }
        public string CallerName { get; set; }
        public short CallDispositionId { get; set; }
        public bool IsDuplicated { get; set; }
        public System.DateTime IncomingDate { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public ReportCallEntity() { }
        #endregion
    }
}
