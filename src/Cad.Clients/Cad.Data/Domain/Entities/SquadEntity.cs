﻿//
// Cad.Data.Domain.Entities.SquadEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class SquadEntity : EntityBase
    {
        #region PROPERTIES
        public long Id { get; set; }
        public long IncidentId { get; set; }
        public short BuildGuideId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public SquadEntity() { }
        #endregion
    }
}
