﻿//
// Cad.Data.Domain.Entities.SquadUnitEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class SquadUnitEntity : EntityBase
    {
        #region PROPERTIES
        public long SquadId { get; set; }
        public long VehicleId { get; set; }
        #endregion

        #region CONSTRUCTORS
        public SquadUnitEntity() { }
        #endregion
    }
}
