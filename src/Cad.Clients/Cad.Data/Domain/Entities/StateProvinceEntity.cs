﻿//
// Cad.Data.Domain.Entities.StateProvinceEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class StateProvinceEntity : EntityBase
    {
        #region PROPERTIES
        public string StateProvinceCode { get; set; }
        public string CountryRegionCode { get; set; }
        public string Name { get; set; }
        #endregion

        #region CONSTRUCTORS
        public StateProvinceEntity() { }
        #endregion
    }
}
