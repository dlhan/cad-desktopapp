﻿//
// Cad.Data.Domain.Entities.UnitStatusEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class UnitStatusEntity : EntityBase
    {
        #region PROPERTIES
        public long SquadId { get; set; }
        public long VehicleId { get; set; }
        public short UnitStatusTypeId { get; set; }
        public System.DateTime ChangedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public UnitStatusEntity() { }
        #endregion
    }
}
