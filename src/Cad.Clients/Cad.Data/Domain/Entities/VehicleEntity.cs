﻿//
// Cad.Data.Domain.Entities.VehicleEntity
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Data.Domain.Entities
{
    public class VehicleEntity : EntityBase
    {
        #region PROPERTIES
        public long Id { get; set; }
        public long DepartmentId { get; set; }
        public short VehicleTypeId { get; set; }
        public string PlateNumber { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public short Status { get; set; }
        public System.DateTime CreatedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public VehicleEntity() { }
        #endregion
    }
}
