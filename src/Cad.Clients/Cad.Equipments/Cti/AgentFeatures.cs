﻿//
// Cad.Equipments.Cti.AgentFeatures
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Equipments.Cti
{
    public enum AgentFeatures
    {
        Enable = 1,
        Disable = 2,
        Connect = 3,
        Disconnect = 4,
        Login = 5,
        Logout = 6
    }
}
