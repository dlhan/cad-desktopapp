﻿//
// Cad.Equipments.Cti.AgentState
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;

namespace Cad.Equipments.Cti
{
    [Flags]
    public enum AgentState
    {
        Connected = 0,
        //Disconnected = 1,
        //LoggedIn = 2,
        //LoggedOut = 4,
        //Enabled = 8,
        //Disabled = 16,
        Disconnected = 1 << 0,
        LoggedIn = 1 << 1,
        LoggedOut = 1 << 2,
        Enabled = 1 << 3,
        Disabled = 1 << 4
    }
}