﻿//
// Cat.Equipments.Cti.CallFeatures
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Equipments.Cti
{
    // Scenario                  Feature             State               Event(Vendor)
    //-------------------------------------------------------------------------
    // Call arrived                                 Ringing             IncomingCall
    // Drop the call            Drop                None                Cleared
    // Answer the call          Answer              Accepted            Established
    // Conversation                                 Busy
    // Hangup                   CompleteCall        None                Cleared
    // Dial up to someone       Dial                Dialing             
    // Hold a call 
    // Start conference         SetupConf           Conferenced         
    // Add someone to           AddToConf           Conferenced
    //  conference    
    // Remove someone form      RemoveFromConf      Conferenced
    //  conference
    // Start transfer           SetupTransfer       Transferred
    // Assing someone to        BindTransfer        Transferred
    //  Transfer
    public enum CallFeatures
    {
        /// <summary>
        /// Answer a call
        /// </summary>
        Answer = 1,
        Dial = 2,
        /// <summary>
        /// Drop a call
        /// </summary>
        Drop = 3,
        /// <summary>
        /// Holde a call
        /// </summary>
        Hold = 4,
        /// <summary>
        /// Unhold a call
        /// </summary>
        Unhold = 5,
        /// <summary>
        /// Redirect a call
        /// </summary>
        //Redirect = 6,
        AddToConf = 7,
        RemoveFromConf = 8,
        SetupConf = 9,
        BindTransfer = 10,
        SetupTransfer = 11
    }
}
