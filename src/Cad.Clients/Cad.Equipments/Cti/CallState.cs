﻿//
// Cat.Equipments.Cti.CallState
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

namespace Cad.Equipments.Cti
{
    public enum CallState
    {
        None = 0,
        Accepted = 1,
        Busy = 2,
        Conferenced = 3,
        Dialing = 4,
        Hold = 5,
        Ringing = 6,
        Closed = 7,
        Unknown = 19
    }
}
