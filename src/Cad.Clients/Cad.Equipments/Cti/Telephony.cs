﻿//
// Cad.Equipment.Cti.Telephony
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System;

namespace Cad.Equipments.Cti
{
    public static class Telephony
    {
        #region VARIABLES
        private static bool _initialized = false;
        private static bool _initializedDefaultProvider = false;
        private static Exception _initializedException = null;
        private static object _lock = new object();
        #endregion

        #region PROPERTIES
        private static AgentState _agentState;
        public static AgentState AgentState 
        {
            get
            {
                Initialze();
                return _agentState;
            }
        }

        private static CallState _callState;
        public static CallState CallState 
        {
            get
            {
                Initialze();
                return _callState;
            }
        }
        #endregion

        #region PRIVATE METHODS
        private static void Initialze()
        {
            if (_initialized && _initializedDefaultProvider)
                return;
            if (_initializedException != null)
                throw _initializedException;

            lock (_lock)
            {
                if (_initialized && _initializedDefaultProvider)
                    return;
                if (_initializedException != null)
                    throw _initializedException;

                bool initializeGeneralSettings = !_initialized;
                // the default provider can be initialized once the pre start
                // init has happened (i.e. when compilation has begun)
                // or if this is not even a hosted scenario
                bool initializedDefaultProvider = !_initializedDefaultProvider;

                if (!initializedDefaultProvider && !initializeGeneralSettings)
                    return;

                bool generalSettingsInitialized;
                bool defaultProviderInitialized = false;
                try
                {
                    //TelephonySection settings = (TelephonySection)ConfigurationManager
                    //    .GetSection("telephony");
                    //generalSettingsInitialized = InitializeSettings(
                    //    initializeGeneralSettings, settings);
                    //defaultProviderInitialized = InitializeDefaultProvider(
                    //    initializedDefaultProvider, settings);
                    generalSettingsInitialized = InitializeSettings();
                    defaultProviderInitialized = InitializeDefaultProvider();

                    _agentState = AgentState.Disconnected 
                        | AgentState.LoggedOut 
                        | AgentState.Disabled;
                    _callState = CallState.None;
                }
                catch (Exception e)
                {
                    _initializedException = e;
                    throw;
                }

                // update this state only after the whole method completes
                // to preserve the behavior where the system is uninitialized
                // if any exceptions were thrown.
                if (generalSettingsInitialized)
                    _initialized = true;

                if (defaultProviderInitialized)
                    _initializedDefaultProvider = true;
            }
        }

        private static bool InitializeSettings()
        {
            return true;
        }

        private static bool InitializeDefaultProvider()
        {
            return true;
        }
        #endregion

        #region PUBLIC METHODS
        public static bool ExecuteAgentFeature(AgentFeatures feature)
        {
            //return Provider.ExecuteAgentFeature(feature);

            // USE ONLY TEST: START
            switch (feature)
            {
                case AgentFeatures.Connect:
                    if (_agentState.HasFlag(AgentState.Disconnected))
                    {
                        _agentState &= ~AgentState.Connected;
                        _agentState |= AgentState.Disconnected;
                        return true;
                    }
                    return false;

                case AgentFeatures.Disconnect:
                    if (_agentState.HasFlag(AgentState.Disconnected))
                    {
                        _agentState &= ~AgentState.Connected;
                        _agentState |= AgentState.Disconnected;
                        return true;
                    }
                    return false;

                case AgentFeatures.Login:
                    if (_agentState.HasFlag(AgentState.LoggedOut))
                    {
                        _agentState &= ~AgentState.LoggedOut;
                        _agentState |= AgentState.LoggedIn;
                        return true;
                    }
                    return false;

                case AgentFeatures.Logout:
                    if (_agentState.HasFlag(AgentState.LoggedIn))
                    {
                        _agentState &= ~AgentState.LoggedOut;
                        _agentState |= AgentState.LoggedIn;
                        return true;
                    }
                    return false;

                case AgentFeatures.Enable:
                    if (_agentState.HasFlag(AgentState.Disconnected))
                        if (!ExecuteAgentFeature(AgentFeatures.Connect))
                            return false;
                    if (_agentState.HasFlag(AgentState.LoggedOut))
                        if (!ExecuteAgentFeature(AgentFeatures.Login))
                            return false;
                    if (_agentState.HasFlag(AgentState.Disabled))
                    {
                        _agentState &= ~AgentState.Disabled;
                        _agentState |= AgentState.Enabled;
                        return true;
                    }
                    return false;

                case AgentFeatures.Disable:
                    if (_agentState.HasFlag(AgentState.Enabled))
                    {
                        _agentState &= ~AgentState.Enabled;
                        _agentState |= AgentState.Disabled;
                        return true;
                    }
                    return false;

                default: return false;
            }
            // USE ONLY TEST: END
        }

        public static bool ExecuteCallFeatures(CallFeatures feature,
            string phoneNumber = "")
        {
            //return Provider.ExecuteCallFeature(feature, phoneNumber);
            return true;
        }
        #endregion

        #region EVENTS
        #endregion
    }
}
