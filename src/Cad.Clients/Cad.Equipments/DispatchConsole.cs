﻿//
// Cad.Equipments.DispatchConsole
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2021
//

using System.Text;

namespace Cad.Equipments
{
    public class DispatchConsole
    {
        #region PROPERTIES
        public int Id { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DispatchConsole()
        {
        }
        #endregion

        #region OVERRIDE METHODS
        #endregion
    }
}
